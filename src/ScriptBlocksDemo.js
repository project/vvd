goog.provide('sb.ScriptBlocksDemo');

goog.require('sb.ScriptBlocks');
goog.require('sb.ScriptBlocksXML');
goog.require('sb.ScriptBlocksJSON');
goog.require('sb.Block');
goog.require('sb.BlockSpec');
goog.require('sb.Drawer');
goog.require('sb.Page');
goog.require('sb.Workspace');
goog.require('sb.Logger');

goog.require('goog.dom');
goog.require('goog.ui.Button');
goog.require('goog.ui.Component.EventType');
goog.require('goog.events');


sb.ScriptBlocksDemo.init = function() {
	
	sb.ScriptBlocks.init();
	
	var wsElement = goog.dom.getElement('blockWS');
	sb.ScriptBlocksDemo.workspace = sb.ScriptBlocks.createWorkspace( wsElement, true );
	sb.ScriptBlocksJSON.init(sb.ScriptBlocksDemo.workspace);
	
	// init demo UI
	var EVENTS = goog.object.getValues(goog.ui.Component.EventType); 
	
	var clearBtn = new goog.ui.Button('Clear');
	clearBtn.decorate(goog.dom.getElement('clearBtn'));
	goog.events.listen(clearBtn, EVENTS, sb.ScriptBlocksDemo.clearWorkspace);
	
	var structureBtn = new goog.ui.Button('Show Structure');
	structureBtn.decorate(goog.dom.getElement('structureBtn'));
	goog.events.listen(structureBtn, EVENTS, sb.ScriptBlocksDemo.showStructure);

	var loadXMLBtn = new goog.ui.Button('Load XML');
	loadXMLBtn.decorate(goog.dom.getElement('loadXMLBtn'));
	goog.events.listen(loadXMLBtn, EVENTS, sb.ScriptBlocksXML.loadXML);

	var loadJSONBtn = new goog.ui.Button('Load JSON');
	loadJSONBtn.decorate(goog.dom.getElement('loadJSONBtn'));
	goog.events.listen(loadJSONBtn, EVENTS, sb.ScriptBlocksJSON.loadJSON);

	var saveXMLBtn = new goog.ui.Button('Save XML');
	saveXMLBtn.decorate(goog.dom.getElement('saveXMLBtn'));
	goog.events.listen(saveXMLBtn, EVENTS, sb.ScriptBlocksXML.saveXML);
	
	var saveJSONBtn = new goog.ui.Button('Save JSON');
	saveJSONBtn.decorate(goog.dom.getElement('saveJSONBtn'));
	goog.events.listen(saveJSONBtn, EVENTS, sb.ScriptBlocksJSON.saveJSON);

	var drawerBtn = new goog.ui.Button('Next Drawer');
	drawerBtn.decorate(goog.dom.getElement('drawerBtn'));
	goog.events.listen(drawerBtn, EVENTS, sb.ScriptBlocks.nextDrawer);
	
//	var runBtn = new goog.ui.Button('Run Code');
//	runBtn.decorate(goog.dom.getElement('runBtn'));
//	goog.events.listen(runBtn, EVENTS, sb.ScriptBlocksDemo.runCode);
}

sb.ScriptBlocksDemo.run = function() {
	
	sb.ScriptBlocksDemo.testPage = new sb.Page();
	sb.ScriptBlocksDemo.workspace.addPage(sb.ScriptBlocksDemo.testPage);
	
	sb.Logger.log("ScriptBlocksDemo running", goog.debug.Logger.Level.INFO);
	
}

sb.ScriptBlocksDemo.addBlock = function() {
	var block = new sb.Block( sb.BlockSpec.RETURN );
	sb.ScriptBlocksDemo.testPage.addBlock( block );
}

sb.ScriptBlocksDemo.addCBlock = function() {
	var block = new sb.Block( sb.BlockSpec.IF_THEN );
	sb.ScriptBlocksDemo.testPage.addBlock( block );
}

sb.ScriptBlocksDemo.clearWorkspace = function() {
	var pages = sb.ScriptBlocksDemo.workspace.getPages();
	
	for(var i = 0; i < pages.length; i += 1) {
		var page = pages[i];
		page.removeAllBlocks();
	}
}

sb.ScriptBlocksDemo.showStructure = function(){
	var pages = sb.ScriptBlocksDemo.workspace.getPages();
	
	for (var i = 0; i < pages.length; i += 1) {
		var page = pages[i];
		
		sb.Logger.log(sb.ScriptBlocks.getPageText(page));
		
		var pageBlocks = page.getChildBlocks();
		
		for (var j = 0; j < pageBlocks.length; j += 1) {
			var block = pageBlocks[j];
			sb.Logger.log(block.getID());
		}
		
	}
}

sb.ScriptBlocksDemo.runCode = function() {
	var app = getFlexApp("WebLogo");
	if(goog.isDefAndNotNull(app)) {
		app.startCompilation();
	}
}

function getTextFromFlash(str) {   
	sb.Logger.log(str);
}

function getBlocksFromJS() {
	var page = sb.ScriptBlocksDemo.workspace.getPages()[0];
	var pageBlocks = page.getChildBlocks();
	
	var blocks = [];
	
	for (var j = 0; j < pageBlocks.length; j += 1) {
			var block = pageBlocks[j];
			var childArgs = [];
			var nestedBlockIDs = [];
			
			blocks.push(block.getName());
		}
		
	return blocks;
}

function getIDsFromJS() {
	var page = sb.ScriptBlocksDemo.workspace.getPages()[0];
	var pageBlocks = page.getChildBlocks();
	
	var blocks = [];
	
	for (var j = 0; j < pageBlocks.length; j += 1) {
		var block = pageBlocks[j];
		var childArgs = [];
		var nestedBlockIDs = [];
		
		blocks.push(block.getID());
	}
		
	return blocks;
}

//Finds the socketed block of a given block in a given socket 
//at a given position in that socket
function getSB(blockID, socket, socketID) {
	var page = sb.ScriptBlocksDemo.workspace.getPages()[0];
	var pageBlocks = page.getAllBlocks();
	
	for (var j = 0; j < pageBlocks.length; j += 1) {
		var block = pageBlocks[j];
		if (block.id_ == ("block" + blockID)) {
			var socketBlock = block.getChildBlocks()[socket];
			for (var k = 0; k < socketID; k += 1) {
				var newBlock = socketBlock.getAfterBlock();
				socketBlock = newBlock;
			}
			
			var packet = [];
			packet.push(socketBlock.getName());
			packet.push(socketBlock.getID());
			return(packet);
		}
		
	}
}

//Finds the block after a given block
function getAfterBlock(blockID) {
	var page = sb.ScriptBlocksDemo.workspace.getPages()[0];
	var pageBlocks = page.getAllBlocks();
	
	for (var j = 0; j < pageBlocks.length; j += 1) {
		var block = pageBlocks[j];
		if (block.id_ == ("block" + blockID)) {
			var afterBlock = block.getAfterBlock();
			
			var packet = [];
			packet.push(afterBlock.getName());
			packet.push(afterBlock.getID());
			return(packet);
		}
		
	}
}

function addDrawerBlock(label, isCommand, sockets, drawer) {
	sb.ScriptBlocksXML.generateNewBlock(label, isCommand, sockets, drawer);
}

/**
 * @interface
 */
sb.ScriptBlocksDemo.IFlexApp = function() {};
sb.ScriptBlocksDemo.IFlexApp.prototype.sendBlocks = function(){};
sb.ScriptBlocksDemo.IFlexApp.prototype.sendNewBlocks = function(){};
sb.ScriptBlocksDemo.IFlexApp.prototype.startCompilation = function(){};

sb.ScriptBlocksDemo.addDrawerBlocks = function() {
	var app = /** @type {sb.ScriptBlocksDemo.IFlexApp} */(getFlexApp("WebLogo"));
	if(goog.isDefAndNotNull(app)) {
		app.sendBlocks();
	}
}



/**
 * @param {string} appName
 * @return {sb.ScriptBlocksDemo.IFlexApp}
 */
function getFlexApp(appName)
{
  if (navigator.appName.indexOf ("Microsoft") !=-1)
  {
    return window[appName];
  } 
  else 
  {
    return document[appName];
  }
}

function sendNewBlocks() {
	var app = getFlexApp("WebLogo");
	if(goog.isDefAndNotNull(app)) {
		app.sendNewBlocks();
	}
}
