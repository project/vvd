/**
 *	@fileoverview ScriptBlocks contains a set of API calls for the ScriptBlocks library.
 *	@author clgreenhill@gmail.com (Colin Greenhill)
 */

goog.provide('sb.ScriptBlocks');

goog.require('sb.Workspace');
goog.require('sb.ObjectManager');
goog.require('sb.ViewManager');
goog.require('sb.BlockDragDropManager');
goog.require('sb.BlockEvent');
goog.require('sb.BlockParentEvent');
goog.require('sb.WorkspaceEvent');
goog.require('sb.SBObjectEvent');
goog.require('sb.Logger');

goog.require('goog.events.EventHandler');
goog.require('goog.events.KeyHandler');
goog.require('goog.debug.ErrorHandler');


/**
 * Testing.
 * @constructor
 */
sb.ScriptBlocks = function(){};

/**
 * Initializes listeners for view creation and drag and drop interaction.
 */
sb.ScriptBlocks.init = function() {
	
	// init logger
	sb.Logger.init();
	
	/**
	 * @private
	 * @type {Array.<sb.Block>}
	 */
	sb.ScriptBlocks.clipboard_ = [];
	
	// init managers
	/** @private */
	sb.ScriptBlocks.objMgr_ = sb.ObjectManager.getInstance();
	/** @private */
	sb.ScriptBlocks.viewMgr_ = sb.ViewManager.getInstance();
	/** @private */
	sb.ScriptBlocks.keyMgr_ = new goog.events.KeyHandler(document);
	
	
	// set listeners
	goog.events.listen(sb.ScriptBlocks.keyMgr_, goog.events.KeyHandler.EventType.KEY, sb.ScriptBlocks.onKey_);
	goog.events.listen(sb.ScriptBlocks.objMgr_, sb.SBObjectEvent.CREATED, sb.ScriptBlocks.onObjCreated_);
}

/**
 * @return {?sb.Workspace}
 */
sb.ScriptBlocks.getWorkspace = function() {
	
	return sb.ScriptBlocks.workspace;
}

/**
 * @param {Element} element
 * @param {boolean=} createSubElements
 * 
 * @return {sb.Workspace}
 */
sb.ScriptBlocks.createWorkspace = function( element, createSubElements ) {
	
	sb.ScriptBlocks.workspace = new sb.Workspace();
	
	sb.ScriptBlocks.viewMgr_.setWorkspaceElement( element );
	sb.ScriptBlocks.viewMgr_.addWorkspaceListeners( sb.ScriptBlocks.workspace );
	
	if(createSubElements) {
		sb.ScriptBlocks.viewMgr_.buildWorkspace();
	}
	
	goog.events.listen( sb.ScriptBlocks.workspace, sb.SBObjectEvent.NULL_PARENT, sb.ScriptBlocks.onNullParent_ );
	
	return sb.ScriptBlocks.workspace;
}

/**
 * Creates a new page and adds it to the workspace.
 * 
 * @return {sb.Page}
 */
sb.ScriptBlocks.createPage = function() {

	var page = new sb.Page();
	sb.ScriptBlocks.workspace.addPage(page);
	
	return page;
}

/**
 * Creates a new drawer and adds it to the workspace.
 * 
 * @param {string=} name
 * 
 * @return {sb.Drawer}
 */
sb.ScriptBlocks.createDrawer = function(name) {

	var drawer = new sb.Drawer(name);
	sb.ScriptBlocks.workspace.addDrawer(drawer);
	
	return drawer;
}

/**
 * @param {sb.SBObjectEvent} event
 */
sb.ScriptBlocks.onObjCreated_ = function( event ) {
	var obj = event.object;
	if(obj.getType() === sb.Block.TYPE) {
		obj.setParentEventTarget(sb.ScriptBlocks.workspace);
	}
}

sb.ScriptBlocks.onNullParent_ = function( event ) {
	var obj = event.currentTarget;
	obj.setParentEventTarget(sb.ScriptBlocks.workspace);
}

/**
 * @param {sb.Page} page
 */
sb.ScriptBlocks.getPageText = function( page ) {
	
	var pageString = page.getID() + ": \n";
	
	var topBlocks = page.getChildBlocks();
	
	for( var i = 0; i < topBlocks.length; i += 1 ) {
		var block = topBlocks[i];
		pageString += "\n" + sb.ScriptBlocks.getBlockText( block ) + "\n";
	}
	
	return pageString;
}

/**
 * @param {sb.Block} block
 * @param {string=} strPrefix
 * 
 * @return {string}
 */
sb.ScriptBlocks.getBlockText = function( block, strPrefix ) {
	var blockText = block.getID() + '\n';
	
	if( strPrefix === undefined ) {
		strPrefix = '';
	}
	
	var args = block.getArguments();
	var i;
	for(i=0; i < args.length; i += 1) {
		var arg = args[i];
		var argValue = arg.getValue();
		var argString = strPrefix + ' | - ' + arg.getName() + ":";
		
		if( goog.isDefAndNotNull( argValue ) && argValue instanceof sb.Block ) {
			blockText += strPrefix + ' | - ' + arg.getName() + ":" + sb.ScriptBlocks.getBlockText( argValue, strPrefix + ' |   ' );
		}
		else {
			blockText += strPrefix + ' | - ' + arg.getName() + ":" + arg.getValue() + "\n";
		}
	}
	
	var afterBlock = block.getAfter();
	if( afterBlock !== null ) {
		blockText += strPrefix + ' | \n' + strPrefix + sb.ScriptBlocks.getBlockText( afterBlock, strPrefix );
	}
	return blockText;
}

sb.ScriptBlocks.nextDrawer = function() {
	
	sb.ScriptBlocks.workspace.nextDrawer();
	
}

sb.ScriptBlocks.prevDrawer = function() {
	
	sb.ScriptBlocks.workspace.prevDrawer();
	
}

/**
 * Returns the DOM element that represents the given object.
 * 
 * @param {sb.SBObject} object
 * 
 * @return {Element}
 */
sb.ScriptBlocks.getElement = function( object ) {
	
	return sb.ScriptBlocks.viewMgr_.getElement( object );
}

/**
 * @param {sb.SBObject} object
 * @param {Element} element
 */
sb.ScriptBlocks.setElement = function( object, element ) {
	
	sb.ScriptBlocks.viewMgr_.setElement( object, element );
	
	if(object.getType() === sb.Page.TYPE) {
		sb.ScriptBlocks.workspace.addPage(/** @type {!sb.Page} */(object));
	}
	
}

/**
 * @param {goog.events.KeyEvent} event
 */
sb.ScriptBlocks.onKey_ = function( event ) {
	
	// delete key
	if(event.keyCode == 46) {
		sb.ScriptBlocks.deleteSelection();
	}
	
	if( event.platformModifierKey ) {
	
		switch( event.keyCode ) {
			
			// c
			case 67:
				sb.ScriptBlocks.copySelection();
				break;
			
			// v
			case 86:
				sb.ScriptBlocks.pasteSelection();
				break;
			
			//x
			case 88: 
				sb.ScriptBlocks.cutSelection();
				break;
				
			default:
				break;
		}
	}
}

/**
 * Deletes a block, removing it from its parent, 
 * and deleting any child blocks, and connecting 
 * the before and after blocks if both exist.
 * 
 * @param {sb.Block} block
 */
sb.ScriptBlocks.deleteBlock = function( block ) {
	
	var parent = block.getParent();
	var parentArg = null;
	
	if( goog.isDefAndNotNull(parent) ) {
	
		if( parent instanceof sb.Block ) {
			// get the argument of the parent block that this block was assigned to
		
			var parentArgs = parent.getArguments();
			for(var i = 0; i < parentArgs.length; i += 1) {
				var tempArg = parentArgs[i];
				if(tempArg.getValue() === block) {
					parentArg = tempArg
					break;
				}
			}
		}
		
		parent.removeBlock( block );
	}
	
	var before = block.getBeforeBlock();
	if( goog.isDefAndNotNull( before ) ) {
		before.disconnectAfterBlock();
	}
	
	var after = block.getAfterBlock();
	
	if( goog.isDefAndNotNull( after ) ) {
		if(goog.isDefAndNotNull( before )) {
			before.connectAfterBlock( after );
		}
		else if(block.getReturnType() === sb.DataType.COMMAND 
				&& goog.isDefAndNotNull( parent ) 
				&& goog.isDefAndNotNull( parentArg )) {
			parent.connectChildBlock( after, parentArg.getName() );
		}
		else {
			sb.ScriptBlocks.deleteBlock( after );
		}
	}
	
	var children = block.getChildBlocks();
	for(var i = 0; i < children.length; i += 1) {
		var childBlock = children[i];
		sb.ScriptBlocks.deleteBlock( childBlock );
	}
	
	sb.ScriptBlocks.workspace.removeFromSelection( [block] );
	
	block.dispose();
	
	sb.ScriptBlocks.viewMgr_.deleteBlockView( block );
	sb.ScriptBlocks.objMgr_.deleteObject( block );
	
	sb.ScriptBlocks.workspace.setDirty();
}

sb.ScriptBlocks.deleteSelection = function() {
	
	var sBlocks = sb.ScriptBlocks.workspace.getSelection();
	
	for(var i = 0; i < sBlocks.length; i += 1) {
		var block = sBlocks[i];
		if(goog.isDefAndNotNull(block)) {
			sb.ScriptBlocks.deleteBlock( block );
		}
	}
}

sb.ScriptBlocks.copySelection = function() {
	var sBlocks = sb.ScriptBlocks.workspace.getSelection();
	
	sb.ScriptBlocks.clearClipboard();
	var origBlocks = [];
	var block = null;
	var newBlock = null;
	
	
	sb.ScriptBlocks.objMgr_.suppressEvents();
	for(var i = 0; i < sBlocks.length; i += 1) {
		
		block = sBlocks[i];
		newBlock = block.clone();
		origBlocks.push(block);
		sb.ScriptBlocks.clipboard_.push(newBlock);
	}
	
	for(i = 0; i < sb.ScriptBlocks.clipboard_.length; i += 1) {
		block = origBlocks[i];
		newBlock = sb.ScriptBlocks.clipboard_[i];
		
		// connect child blocks
		var args = block.getArguments();
		for(var j = 0; j < args.length; j += 1) {
			var arg = args[j];
			var val = arg.getValue();
			if( goog.isDefAndNotNull(val) && val instanceof sb.Block 
					&& goog.array.contains(origBlocks, val) ) {
				var childIndex = origBlocks.indexOf(val);
				var newChildBlock = /** @type {sb.Block} */sb.ScriptBlocks.clipboard_[childIndex];
				newBlock.connectChildBlock( newChildBlock, arg.getName() );
			}
		}
		
		// connect after block
		var afterBlock = block.getAfterBlock();
		if( goog.isDefAndNotNull(afterBlock) 
				&& goog.array.contains(origBlocks, afterBlock) ) {
			var afterIndex = origBlocks.indexOf(afterBlock);
			newBlock.connectAfterBlock( sb.ScriptBlocks.clipboard_[afterIndex] );
		}
	}
	sb.ScriptBlocks.objMgr_.resumeEvents();
	
}	
	
sb.ScriptBlocks.pasteSelection = function() {
	var currentPage = sb.ScriptBlocks.workspace.getCurrentPage();
	
	// create copies of the selected blocks
	var newBlocks = [];
	var block, newBlock;
	var i;
	for(i = 0; i < sb.ScriptBlocks.clipboard_.length; i += 1) {
		block = sb.ScriptBlocks.clipboard_[i];
		newBlock = block.clone();
		newBlocks[i] = newBlock;
	}
	
	
	// assign child/after connections
	var topBlocks = [];
	for(i = 0; i < sb.ScriptBlocks.clipboard_.length; i += 1) {
		block = sb.ScriptBlocks.clipboard_[i];
		newBlock = /** @type {sb.Block} */newBlocks[i];
		
		// if this is a top level block, add to array
		/** @type {sb.IBlockParent} */
		var parent = block.getParent();
		if(goog.isDefAndNotNull(parent) && 
				( parent instanceof sb.Page || !goog.array.contains(sb.ScriptBlocks.clipboard_, parent) )
				|| 
				!goog.isDefAndNotNull(parent) &&
				!goog.array.contains(sb.ScriptBlocks.clipboard_, block.getBeforeBlock()) ) {
			
			topBlocks.push(newBlock);
		}
		
		// connect child blocks
		var args = block.getArguments();
		for(var j = 0; j < args.length; j += 1) {
			var arg = args[j];
			var val = arg.getValue();
			if( goog.isDefAndNotNull(val) && val instanceof sb.Block 
					&& goog.array.contains(sb.ScriptBlocks.clipboard_, val) ) {
				var childIndex = sb.ScriptBlocks.clipboard_.indexOf(val);
				var newChildBlock = /** @type {sb.Block} */newBlocks[childIndex];
				newBlock.connectChildBlock( newChildBlock, arg.getName() );
			}
		}
		
		// connect after block
		var afterBlock = block.getAfterBlock();
		if( goog.isDefAndNotNull(afterBlock) 
				&& goog.array.contains(sb.ScriptBlocks.clipboard_, afterBlock) ) {
			var afterIndex = sb.ScriptBlocks.clipboard_.indexOf(afterBlock);
			newBlock.connectAfterBlock( newBlocks[afterIndex] );
		}
	}
	
	// add top blocks to the current page
	for(i = 0; i < topBlocks.length; i += 1) {
		var topBlock = topBlocks[i];
		currentPage.addBlock(topBlock);
	}
}

sb.ScriptBlocks.cutSelection = function() {
	sb.ScriptBlocks.copySelection();
	sb.ScriptBlocks.deleteSelection();
}

sb.ScriptBlocks.clearClipboard = function() {
	sb.ScriptBlocks.objMgr_.suppressEvents();
	for(var i = 0; i < sb.ScriptBlocks.clipboard_.length; i += 1) {
		var block = sb.ScriptBlocks.clipboard_[i];
		sb.ScriptBlocks.deleteBlock(block);
	}
	sb.ScriptBlocks.clipboard_ = [];
	sb.ScriptBlocks.objMgr_.resumeEvents();
}

sb.ScriptBlocks.workspace = null;

goog.exportSymbol('sb.ScriptBlocks.init', sb.ScriptBlocks.init);
goog.exportSymbol('sb.ScriptBlocks.createWorkspace', sb.ScriptBlocks.createWorkspace);
goog.exportSymbol('sb.ScriptBlocks.getWorkspace', sb.ScriptBlocks.getWorkspace);
goog.exportSymbol('sb.ScriptBlocks.createPage', sb.ScriptBlocks.createPage);
goog.exportSymbol('sb.ScriptBlocks.createDrawer', sb.ScriptBlocks.createDrawer);