goog.provide('sb.ViewManager');

goog.require('sb.Block');
goog.require('sb.Page');
goog.require('sb.Drawer');
goog.require('sb.SocketType');

goog.require('sb.BlockView');
goog.require('sb.PageView');
goog.require('sb.DrawerView');
goog.require('sb.BlockViewEvent');
goog.require('sb.SocketShapes');

goog.require('sb.Logger');

goog.require('goog.dom');
goog.require('goog.math.Box');
goog.require('goog.math.Coordinate');
goog.require('goog.events');
goog.require('goog.string');
goog.require('goog.ui.TabBar');

/**
 * @extends {goog.events.EventTarget}
 * @constructor
 */
sb.ViewManager = function() {
	
	/**
	 * @type {Object.<sb.BlockView>}
	 * @private
	 */
	this.blockViews_ = {};
	
	/**
	 * @type {Object.<sb.PageView>}
	 * @private
	 */
	this.pageViews_ = {};
	
	/**
	 * @type {Object.<sb.DrawerView>}
	 * @private
	 */
	this.drawerViews_ = {};
	
	/**
	 * @type {Object.<sb.DrawerView>}
	 * @private
	 */
	this.curDrawerView_ = null;
	
	/**
	 * @type {Object.<sb.PageView>}
	 * @private
	 */
	this.curPageView_ = null;
	
	/**
	 * @type {number}
	 * @private
	 */
	this.snapTolerance_ = 40;
	
	this.workspaceElement_ = null;
	this.pageHolder_ = null;
	this.drawerHolder_ = null;
	
	this.drawerControls_ = null;
	this.pageTabIDs_ = {};
	this.tabPageIDs_ = {};
	
	this.defaultPageSize_ = null;
	
	this.hilightEl_ = null;
	
	this.selectionEl_ = null;
	this.selectedViews_ = [];
	
	// set default socket shapes
	sb.SocketShapes.initDefaultMap();
	
	sb.BlockView.installStyles();
	
	/** @private */
	this.objMgr_ = sb.ObjectManager.getInstance();
	
	goog.events.listen(this.objMgr_, sb.SBObjectEvent.CREATED, this.onObjectCreated_, false, this);
	goog.events.listen(this.objMgr_, sb.SBObjectEvent.DELETED, this.onObjectDeleted_, false, this);

	goog.events.listen(this, sb.BlockViewEvent.INPUT_CHANGED, this.onBlockInputChange_, false, this);
	
	
	/** @private */
	this.ddMgr_ = new sb.BlockDragDropManager();

	goog.events.listen(this.ddMgr_, sb.BlockDragEvent.DRAG_START, this.onDragStart_, false, this);
	goog.events.listen(this.ddMgr_, sb.BlockDragEvent.DRAG_END, this.onDragEnd_, false, this);
}
goog.inherits(sb.ViewManager, goog.events.EventTarget);

goog.addSingletonGetter(sb.ViewManager);


/**
 * @param {Element} workspaceEl
 */
sb.ViewManager.prototype.setWorkspaceElement = function( workspaceEl ) {
	
	this.workspaceElement_ = workspaceEl;
	if(!goog.isDefAndNotNull(this.drawerHolder_)) {
		this.drawerHolder_ = workspaceEl;
	}
	
	// create selection element	
	this.selectionEl_ = document.createElement("div");
	goog.dom.classes.add( this.selectionEl_, "sbSelection" );
	this.selectionEl_.style.display = "none";
	goog.dom.appendChild( this.workspaceElement_, this.selectionEl_ );
	
	this.selectionEl_.onmousedown = function(){return false};
	//goog.events.listen(this.selectionEl_, goog.events.EventType.MOUSEDOWN, function(event){ event.preventDefault(); });
	
	// create hilight element
	this.hilightEl_ = document.createElement( "div" );
	goog.dom.classes.add( this.hilightEl_, "sbHilight" );
	goog.dom.appendChild( this.workspaceElement_, this.hilightEl_ );
	
	
	this.ddMgr_.setWorkspaceElement( workspaceEl );
}

/**
 * @return {Element}
 */
sb.ViewManager.prototype.getWorkspaceElement = function() {
	
	return this.workspaceElement_;
}

/**
 * @return {Element}
 */
sb.ViewManager.prototype.getPageHolder = function( workspaceEl ) {
	
	return this.workspaceElement_;
}

/**
 * @param {goog.events.EventTarget} workspace
 */
sb.ViewManager.prototype.addWorkspaceListeners = function( workspace ) {
	
	goog.events.listen(workspace, 
			[sb.BlockEvent.MOVED, 
			 sb.BlockEvent.LABEL_CHANGED, 
			 sb.BlockEvent.CHILD_CONNECTED, 
			 sb.BlockEvent.CHILD_DISCONNECTED,
			 sb.BlockEvent.AFTER_CONNECTED,
			 sb.BlockEvent.AFTER_DISCONNECTED, 
			 sb.BlockEvent.ARGUMENT_ADDED, 
			 sb.BlockEvent.ARGUMENT_REMOVED,
			 sb.BlockEvent.COLOR_CHANGED],
			 this.onBlockChange_, false, this);
	
	goog.events.listen(workspace, 
			[sb.ArgumentEvent.VALUE_CHANGED, 
			 sb.ArgumentEvent.OPTIONS_CHANGED], 
			 this.onArgumentChange_, false, this);
	
	goog.events.listen(workspace, 
			[sb.BlockParentEvent.BLOCK_ADDED, 
			 sb.BlockParentEvent.BLOCK_REMOVED, 
			 sb.BlockParentEvent.ALL_BLOCKS_REMOVED],
			 this.onBlockParentChange_, false, this);
	
	goog.events.listen(workspace, sb.SBObjectEvent.NULL_PARENT, this.onNullParent_, false, this);
		
	goog.events.listen(workspace, 
			[sb.WorkspaceEvent.PAGE_ADDED, 
			 sb.WorkspaceEvent.DRAWER_ADDED, 
			 sb.WorkspaceEvent.PAGE_CHANGED, 
			 sb.WorkspaceEvent.DRAWER_CHANGED, 
			 sb.WorkspaceEvent.SELECTION_CHANGED],
			 this.onWorkspaceChange_, false, this);
	
}

/**
 * @param {goog.events.EventTarget} workspace
 */
sb.ViewManager.prototype.clearWorkspaceListeners = function( workspace ) {
	
	goog.events.unlisten(workspace, 
			[sb.BlockEvent.MOVED, 
			 sb.BlockEvent.LABEL_CHANGED, 
			 sb.BlockEvent.CHILD_CONNECTED, 
			 sb.BlockEvent.CHILD_DISCONNECTED,
			 sb.BlockEvent.AFTER_CONNECTED,
			 sb.BlockEvent.AFTER_DISCONNECTED, 
			 sb.BlockEvent.ARGUMENT_ADDED, 
			 sb.BlockEvent.ARGUMENT_REMOVED,
			 sb.BlockEvent.COLOR_CHANGED],
			 this.onBlockChange_, false, this);
	
	goog.events.unlisten(workspace, 
			[sb.ArgumentEvent.VALUE_CHANGED, 
			 sb.ArgumentEvent.OPTIONS_CHANGED], 
			 this.onArgumentChange_, false, this);
	
	goog.events.unlisten(workspace, 
			[sb.BlockParentEvent.BLOCK_ADDED, 
			 sb.BlockParentEvent.BLOCK_REMOVED, 
			 sb.BlockParentEvent.ALL_BLOCKS_REMOVED],
			 this.onBlockParentChange_, false, this);
	
	goog.events.unlisten(workspace, sb.SBObjectEvent.NULL_PARENT, this.onNullParent_, false, this);
		
	goog.events.unlisten(workspace, 
			[sb.WorkspaceEvent.PAGE_ADDED, 
			 sb.WorkspaceEvent.DRAWER_ADDED, 
			 sb.WorkspaceEvent.PAGE_CHANGED, 
			 sb.WorkspaceEvent.DRAWER_CHANGED, 
			 sb.WorkspaceEvent.SELECTION_CHANGED],
			 this.onWorkspaceChange_, false, this);
	
}

sb.ViewManager.prototype.buildWorkspace = function() {
	
	/**
	 * Disabled to work around bug in Chrome:
	 * http://team.imaginationtoolbox.org/issues/2823
	 */
	//this.workspaceElement_.style.overflow = "hidden";
	
	// TODO: move to(/create) sb.WorkspaceView?
	this.drawerHolder_ = document.createElement("div");
	this.drawerHolder_.className = "sbDrawerHolder";
	
	// TODO: clean up layout of controls - separate class?
	this.drawerControls_ = document.createElement("select");
	this.drawerControls_.style.width = "100%";
	this.drawerControls_.className = "sbDrawerLabel";
	
	goog.events.listen(this.drawerControls_, goog.events.EventType.CHANGE, this.onDrawerChange_, false, this);
	
	goog.dom.appendChild(this.workspaceElement_, this.drawerHolder_);
	goog.dom.appendChild(this.drawerHolder_, this.drawerControls_);
	

	this.pageTabBar = new goog.ui.TabBar();
	this.pageTabBar.render( this.workspaceElement_ );
	
	this.pageHolder_ = document.createElement("div");
	goog.dom.appendChild(this.workspaceElement_, this.pageHolder_);
	
	
	this.hiddenEl_ = document.createElement("div");
	goog.style.setWidth(this.hiddenEl_, 0);
	goog.style.setHeight(this.hiddenEl_, 0);
	this.hiddenEl_.style.position = "absolute";
	this.hiddenEl_.style.overflow = "hidden";
	goog.dom.getElementsByTagNameAndClass('body', null)[0].appendChild(this.hiddenEl_);
	
	goog.events.listen( this.pageTabBar, goog.ui.Component.EventType.SELECT, this.onPageTabSelect_, false, this);
	
	goog.dom.classes.add(this.pageHolder_, "sbPageHolder");
	
	this.resizePageHolder();
	
	goog.events.listen(window, goog.events.EventType.RESIZE, this.onWindowResize_, true, this);
	
	this.trashBin_ = document.createElement("div");
	goog.dom.classes.add(this.trashBin_, "sbTrashBin");
	this.trashBin_.innerHTML = "Trash";
	goog.dom.appendChild(this.workspaceElement_, this.trashBin_);
	
	this.resizePageHolder();
	
}

/**
 * @return {goog.math.Size}
 */
sb.ViewManager.prototype.getDefaultPageSize = function() {
	return this.defaultPageSize_;
}


sb.ViewManager.prototype.updateLayout = function() {
	this.resizePageHolder();
	this.resizeDrawer();
}

sb.ViewManager.prototype.resizePageHolder = function() {
	
	var wsBB = goog.style.getBorderBox(this.workspaceElement_);
	var phBB = goog.style.getBorderBox( this.pageHolder_ );
	
	var wsSize = goog.style.getSize(this.workspaceElement_);
	
	var pageTabSize;
	var tab = this.pageTabBar.getSelectedTab();
	if(goog.isDefAndNotNull( tab )) {
		pageTabSize = goog.style.getSize( this.pageTabBar.getElement() );
	}
	else {
		pageTabSize = new goog.math.Size(0, 0);
	}
		
	var newWidth = wsSize.width 
		- goog.style.getBorderBoxSize(this.drawerHolder_).width 
		- phBB.left - phBB.right - wsBB.left - wsBB.right;
	
	var newHeight = wsSize.height - pageTabSize.height;
		
	goog.style.setWidth(this.pageHolder_, newWidth);
	goog.style.setHeight(this.pageHolder_, newHeight)
	
	var phSize = goog.style.getSize( this.pageHolder_ );
	this.defaultPageSize_ = new goog.math.Size( 
			phSize.width - phBB.left - phBB.right, 
			phSize.height - phBB.top - phBB.bottom);
	
	if(goog.isDefAndNotNull(this.curPageView_)) {
		this.curPageView_.autoSize( this.defaultPageSize_ );
	}
}

sb.ViewManager.prototype.resizeDrawer = function() {
	var dhBB = goog.style.getBorderBox(this.drawerHolder_);
	var dhHeight = goog.style.getSize(this.drawerHolder_).height;
	var dCtrlHeight = goog.style.getSize(this.drawerControls_).height
	
	if(goog.isDefAndNotNull(this.curDrawerView_)) {
		goog.style.setHeight(this.curDrawerView_.getElement(), dhHeight - dhBB.top - dhBB.bottom - dCtrlHeight);
	}
}

/**
 * @param {sb.Drawer} drawer
 * 
 * @return {sb.DrawerView}
 */
sb.ViewManager.prototype.createDrawerView = function( drawer ) {
	
	var drawerView = new sb.DrawerView( drawer, null, drawer.getName() );
	this.drawerViews_[drawer.getID()] = drawerView;
	
	drawerView.setParentEventTarget(this);
	
	return drawerView;
}

/**
 * @param {sb.Page} page
 * 
 * @return {sb.PageView}
 */
sb.ViewManager.prototype.createPageView = function( page ) {
	
	var pageView = new sb.PageView( page, null, page.getName() );
	this.pageViews_[page.getID()] = pageView;
	
	pageView.setParentEventTarget(this);
	
	// add listener for drawing a selection box
	goog.events.listen(pageView.getElement(), goog.events.EventType.MOUSEDOWN, this.onWSMouseDown_, false, this);
	
	return pageView;
}

/**
 * @param {sb.Block} block
 * 
 * @return {sb.BlockView}
 */
sb.ViewManager.prototype.createBlockView = function( block ) {
	
	var blockView = new sb.BlockView( block.getID(), block, 160, 40);
	this.blockViews_[block.getID()] = blockView;
	
	goog.events.listen( blockView.getElement(), goog.events.EventType.CLICK, this.onBlockClick_, false, this );
	
	var blockEl = blockView.getElement();
	goog.dom.appendChild( this.hiddenEl_, blockEl );
	blockView.layout();
	blockView.redraw();
	
	return blockView;
}

/**
 * @param {sb.Block} block
 */
sb.ViewManager.prototype.deleteBlockView = function( block ) {
	
	var blockView = this.blockViews_[ block.getID() ];
	if(!goog.isDefAndNotNull(blockView)) {
		return;
	}
	var blockEl = blockView.getElement();
	
	var blockParent = block.getParent();
	if(goog.isDefAndNotNull(blockParent)) {
		var parentView = this.getPageView( blockParent.getID() );
		if(!goog.isDefAndNotNull(blockParent)) {
			parentView = this.getDrawerView( blockParent.getID() );
		}
		
		parentView.removeBlockView(blockView);
	}
	
	this.ddMgr_.removeBlockListeners( blockView.getElement() );
	
	goog.events.unlisten( blockEl, goog.events.EventType.CLICK, this.onBlockClick_, false, this );
	
	
	goog.object.remove( this.blockViews_, block.getID() );
	goog.array.remove(this.selectedViews_, blockView );
	
	blockView.dispose();
}

/**
 * @param {string} blockID
 * 
 * @return {sb.BlockView}
 */
sb.ViewManager.prototype.getBlockView = function( blockID ) {
	
	var blockView = this.blockViews_[blockID];
	
	return blockView;
}


/**
 * @param {string} drawerID
 * 
 * @return {sb.DrawerView}
 */
sb.ViewManager.prototype.getDrawerView = function( drawerID ) {
	
	var drawerView = this.drawerViews_[drawerID];
	
	return drawerView;
}


/**
 * @param {string} pageID
 * 
 * @return {sb.PageView}
 */
sb.ViewManager.prototype.getPageView = function( pageID ) {
	
	var pageView = this.pageViews_[pageID];
	
	return pageView;
}

/**
 * @param {Element} element
 * 
 * @return {sb.BlockView}
 */
sb.ViewManager.prototype.getElementView_ = function( element ) {
	
	var objID = element.id;
	
	var blockView = this.blockViews_[objID];
	
	return blockView;
}

/**
 * @param {sb.SBObject} object
 * 
 * @return {Element}
 */
sb.ViewManager.prototype.getElement = function( object ) {
	
	var objType = object.getType();
	var objID = object.getID();
	
	var element;
	
	switch(objType) {
	
	case sb.Block.TYPE:
		element = this.blockViews_[objID].getElement();
		break;
		
	case sb.Page.TYPE:
		element = this.pageViews_[objID].getElement();
		break;
	
	case sb.Drawer.TYPE:
		element = this.drawerViews_[objID].getElement();
		break;
	}
	
	return element ? element : null; 
}

/**
 * @param {sb.SBObject} object
 * @param {Element} element
 */
sb.ViewManager.prototype.setElement = function( object, element ) {
	
	var objID = object.getID();
	
	switch(object.getType()) {
	
	
	case "page":
		var pageView = this.pageViews_[objID];
		pageView.setElement(element);
		break;
	
	case "drawer":
		var drawerView = this.drawerViews_[objID];
		drawerView.setElement(element);
		break;
		
	default:
		sb.Logger.log("invalid object for setElement");
		break;
	}
}

/**
 * @param {goog.math.Rect} rect
 */
sb.ViewManager.prototype.hilightArea = function( rect ) {
	
	this.hilightEl_.style.display = "inline-block";
	this.hilightEl_.style.position = "absolute";
	
	goog.style.setPosition(this.hilightEl_, rect.left, rect.top);
	goog.style.setSize(this.hilightEl_, rect.width, rect.height);
	
	goog.dom.insertSiblingAfter( this.hilightEl_, this.pageHolder_ );
}

sb.ViewManager.prototype.removeHilight = function() {
	
	this.hilightEl_.style.display = "none";
}

/**
 * @param {string} pageID
 */
sb.ViewManager.prototype.addPageToWorkspace = function(pageID) {
	var pageView = this.getPageView(pageID);
	var pageEl = pageView.getElement();
	
	if(!goog.dom.contains(this.workspaceElement_, pageEl)) {
		if(goog.isDefAndNotNull( this.pageHolder_ )) {
			
			var pageTitle = pageView.getName();
			if(!goog.isDefAndNotNull( pageTitle )) {
				pageTitle = pageView.getID();
			}
				
			var tab = new goog.ui.Tab( pageTitle );
			this.pageTabBar.addChild( tab, true );
			
			this.tabPageIDs_[tab.getId()] = pageID;
			this.pageTabIDs_[pageID] = tab.getId();
			
			goog.dom.appendChild(this.pageHolder_, pageEl);
		}
		else {
			goog.dom.appendChild(this.workspaceElement_, pageEl);
		}
	}
	
	pageView.setParentEventTarget(this);
	
	pageView.hide();
	
	this.resizePageHolder();
}

/**
 * @param {string} drawerID
 */
sb.ViewManager.prototype.addDrawerToWorkspace = function(drawerID) {
	var drawerView = this.getDrawerView( drawerID );
	
	// account for IE and Firefox differences
	try {
		this.drawerControls_.add( new Option(drawerView.getName(), drawerView.getID(), false, false), null );
	}
	catch(e) {
		this.drawerControls_.add( new Option(drawerView.getName(), drawerView.getID(), false, false) );
	}
	
	if(!goog.isDefAndNotNull(this.curDrawerView_)) {
		this.showDrawer_(drawerView);
	}
}

/**
 * @param {sb.PageView} pageView
 */
sb.ViewManager.prototype.showPage_ = function(pageView) {
	
	if(goog.isDefAndNotNull( this.curPageView_ )) {
		this.curPageView_.hide();
	}
	
	this.curPageView_ = pageView;
	pageView.show();
	
	var pageEl = pageView.getElement();
	var blockEls = pageEl.children;
	for(var i = 0; i < blockEls.length; i += 1) {
		var blockView = this.getBlockView(blockEls[i].id);
		if( goog.isDefAndNotNull(blockView) ) {
			blockView.redraw();
		}
	}
	
	pageView.autoSize( this.defaultPageSize_ );
	

	var tab = this.pageTabBar.getChild( this.pageTabIDs_[pageView.getID()] );
	if(tab !== this.pageTabBar.getSelectedTab()) {
		this.pageTabBar.setSelectedTab( tab );
	}
}

/**
 * @param {sb.DrawerView} drawerView
 */
sb.ViewManager.prototype.showDrawer_ = function(drawerView) {
	
	var drawerEl = drawerView.getElement();

	if( !goog.isDefAndNotNull(this.curDrawerView_) ) {
		goog.dom.appendChild(this.drawerHolder_, drawerEl);
	}
	else {
		goog.dom.replaceNode(drawerEl, this.curDrawerView_.getElement());
	}
	
	this.curDrawerView_ = drawerView;
	
	var blockEls = drawerEl.children;
	var i;
	for(i = 0; i < blockEls.length; i += 1) {
		var blockView = this.getBlockView(blockEls[i].id);
		blockView.redraw();
	}
	
	this.resizeDrawer();
	
	// update the dropdown list
	for(i = 0; i < this.drawerControls_.options.length; i += 1) {
		var option = this.drawerControls_.options[i];
		if(option.value == drawerView.getID()) {
			this.drawerControls_.selectedIndex = i;
			break;
		}
	}
}

/**
 * @param {sb.Block} block
 */
sb.ViewManager.prototype.connectChildBlockViews = function( block ) {
	
	var blockView = this.getBlockView( block.getID() );
	
	var args = block.getArguments();
	for(var i = 0; i < args.length; i += 1) {
		var arg = args[i];
		var val = arg.getValue();
		
		if(val instanceof sb.Block) {
			var childView = this.getBlockView( val.getID() );
			if( childView.getParent() !== blockView ) {
				blockView.addChild( childView, arg.getName() );
			}
			
			this.connectChildBlockViews( val );
			this.connectAfterBlockViews( val );
		}
	}
}

/**
 * @param {sb.Block} block
 */
sb.ViewManager.prototype.connectAfterBlockViews = function( block ) {
	
	var blockView = this.getBlockView( block.getID() );
	
	var afterBlock = block.getAfter();
	if(goog.isDefAndNotNull( afterBlock )) {
		
		var afterView = this.getBlockView( afterBlock.getID() );
		if( afterView.getBeforeView() !== blockView ) {
			blockView.connectAfterView( afterView );
		}
		
		this.connectChildBlockViews( afterBlock );
		this.connectAfterBlockViews( afterBlock );
	}
}

/**
 * @param {goog.math.Coordinate} position
 * @param {boolean=}	useClosest		If the position is not inside any page, return the closest visible PageView.
 * @param {number=}		maxDistance		The maximum distance allowed between the position and PageView.
 * 
 * @return {sb.PageView}
 */
sb.ViewManager.prototype.getPageViewAtPosition = function( position, useClosest, maxDistance ) {
	
	var pageView = null;
	
	
	// used if the position is not inside any page
	var closestDist;
	if(useClosest) {
		closestDist = goog.isDefAndNotNull(maxDistance) ? maxDistance : Infinity;
	}
	
	for(var pageID in this.pageViews_) 
	{
		var tempPageView = this.pageViews_[pageID];
		var pageEl = tempPageView.getElement();
		if(pageEl.style.display === "none") {
			continue;
		}
		var pageSize = goog.style.getSize(pageEl);
		var pagePos = goog.style.getRelativePosition(pageEl, this.workspaceElement_);
		
		var pageRect = new goog.math.Rect(pagePos.x, pagePos.y, pageSize.width, pageSize.height);
		if( pageRect.contains(position) ) {
			pageView = tempPageView;
			break;
		}
		else if(useClosest){
			
			// find distance between pos and closest point on page
			var closestPos = new goog.math.Coordinate();
			
			if(position.x >= pagePos.x && position.x <= pagePos.x + pageSize.width) {
				closestPos.x = position.x;
			} 
			else {
				if(position.x < pagePos.x) {
					closestPos.x = pagePos.x;
				}
				else {
					closestPos.x = pagePos.x + pageSize.width;
				}
			}
			
			if(position.y >= pagePos.y && position.y <= pagePos.y + pageSize.height) {
				closestPos.y = position.y;
			} 
			else {
				if(position.y < pagePos.y) {
					closestPos.y = pagePos.y;
				}
				else {
					closestPos.y = pagePos.y + pageSize.height;
				}
			}
			
			var pageDist = goog.math.Coordinate.distance(position, closestPos);
			if(pageDist < closestDist) {
				closestDist = pageDist;
				pageView = tempPageView;
			}
			
		}
	}
	
	
	return pageView;
}

/**
 * @param {Array} sockets
 * @param {!goog.math.Coordinate} position
 * @param {string=} dataType
 * @param {boolean=} includeMidStack
 * 
 * @return {Object}
 */
sb.ViewManager.prototype.getClosestSocket = function( sockets, position, dataType, includeMidStack ) {
	
	var closestSocketID = null;
	var closestBlockID = null;
	var closestSqDist = 10000;
	
	for(var i = 0; i < sockets.length; i += 1) {
		
		var block = sockets[i].block;
		var socketID = sockets[i].socketID;
		var socketPos = sockets[i].socketPos;
		
		var sqDist = goog.math.Coordinate.squaredDistance(position, socketPos);
		
		if(sqDist < this.snapTolerance_ * this.snapTolerance_) {
			
			if(closestSocketID === null || sqDist < closestSqDist) {
				closestBlockID = block.getID();
				closestSocketID = socketID;
				closestSqDist = sqDist;
			}
		}
	}
	
	var result = {
		blockID: closestBlockID,
		socketName: closestSocketID
	};
	
	return result;
};

/**
 * @return {goog.math.Box}
 */
sb.ViewManager.prototype.getTrashBox = function() {
	var pos = goog.style.getRelativePosition(this.trashBin_, this.workspaceElement_);
	var size = goog.style.getSize(this.trashBin_);
	var box = new goog.math.Box(pos.y, pos.x + size.width, pos.y + size.height, pos.x);
	
	return box;
}

sb.ViewManager.prototype.openTrash = function() {
	goog.dom.classes.add(this.trashBin_, "sbTrashBinOpen");
}

sb.ViewManager.prototype.closeTrash = function() {
	goog.dom.classes.remove(this.trashBin_, "sbTrashBinOpen");
}


sb.ViewManager.prototype.clearSelection = function() {
	for(var i = 0; i < this.selectedViews_.length; i += 1) {
		this.selectedViews_[i].setSelected(false);
	}
	
	this.selectedViews_ = [];
}

/**
 * Handler for changing the displayed page when a page tab is clicked.
 * @param {Event} event
 */
sb.ViewManager.prototype.onPageTabSelect_ = function(event) {
	
	var tab = this.pageTabBar.getSelectedTab();
	var pageID = this.tabPageIDs_[tab.getId()];
	var page = this.objMgr_.getObject(pageID, "page");
	
	if(page !== sb.ScriptBlocks.workspace.getCurrentPage()) {
		sb.ScriptBlocks.workspace.setCurrentPage(page);
	}
}

/**
 * Handler for starting a selection box.
 * 
 * @param {Event} event
 */
sb.ViewManager.prototype.onWSMouseDown_ = function(event) {
	var tarEl = /** @type {Element} */(event.target);
	if( sb.ViewManager.isBlockElement( tarEl ) ) {
		return;
	}
	
	goog.style.setSize( this.selectionEl_, 0, 0 );
	
	this.selectStartPos_ = goog.style.getRelativePosition( event, this.workspaceElement_ );
	goog.style.setPosition( this.selectionEl_, this.selectStartPos_ );
	
	this.selectionEl_.style.display = "block";
	goog.dom.appendChild( this.workspaceElement_, this.selectionEl_ );
	
	goog.events.listen( this.workspaceElement_, goog.events.EventType.MOUSEMOVE, this.onSelectionDraw_, false, this );
	goog.events.listen( this.workspaceElement_, goog.events.EventType.MOUSEUP, this.onSelectionEnd_, false, this );
}

/**
 * @param {Event} event
 */
sb.ViewManager.prototype.onWindowResize_ = function(event) {
	
	this.updateLayout();
}

/**
 * @param {Event} event
 */
sb.ViewManager.prototype.onBlockClick_ = function(event) {
	
	// don't select the block if an input element was clicked
	// - otherwise, cut & paste commands would affect the block instead of the input text
	if( event.target.tagName === 'INPUT' ) {
		return;
	}
	
	var blockEl = event.currentTarget;
	var block = /** @type {sb.Block} */(this.objMgr_.getObject( blockEl.id ));
	
	if( !event.shiftKey && !event.ctrlKey ) {
		sb.ScriptBlocks.getWorkspace().setSelection( null );
	}
	
	if( event.ctrlKey && goog.array.contains( sb.ScriptBlocks.getWorkspace().getSelection(), block ) ) {
		sb.ScriptBlocks.getWorkspace().removeFromSelection( [block] );
	}
	else {
		sb.ScriptBlocks.getWorkspace().addToSelection( [block] );
	}
	
	event.stopPropagation();
}


/**
 * @param {sb.BlockViewEvent} event
 */
sb.ViewManager.prototype.onBlockInputChange_ = function(event) {
	var blockView = event.target;
	var block = this.objMgr_.getObject(blockView.getID());
	var arg = block.getArgument(event.argName);
	arg.setValue(event.value);
}

/**
 * Handler for drawing a selection box as the mouse moves.
 * 
 * @param {Event} event
 */
sb.ViewManager.prototype.onSelectionDraw_ = function(event) {
	
	var mousePos = goog.style.getRelativePosition( event, this.workspaceElement_ );
	
	var top = this.selectStartPos_.y > mousePos.y ? mousePos.y : this.selectStartPos_.y;
	var left = this.selectStartPos_.x > mousePos.x ? mousePos.x : this.selectStartPos_.x;
	var bottom = this.selectStartPos_.y < mousePos.y ? mousePos.y : this.selectStartPos_.y;
	var right = this.selectStartPos_.x < mousePos.x ? mousePos.x : this.selectStartPos_.x;
	
	goog.style.setPosition( this.selectionEl_, left, top );
	goog.style.setSize( this.selectionEl_, right - left, bottom - top );
}

/**
 * Handler for finishing a selection box.
 * 
 * @param {Event} event
 */
sb.ViewManager.prototype.onSelectionEnd_ = function(event) {
	// remove selection drawing listeners
	goog.events.unlisten( this.workspaceElement_, goog.events.EventType.MOUSEMOVE, this.onSelectionDraw_, false, this );
	goog.events.unlisten( this.workspaceElement_, goog.events.EventType.MOUSEUP, this.onSelectionEnd_, false, this );
	
	var i;
	// clear previous selected blocks
	for(i = 0; i < this.selectedViews_.length; i += 1) {
		this.selectedViews_[i].setSelected(false);
	}
	sb.ScriptBlocks.getWorkspace().clearSelection();
	
	// TODO: determine which page selection was made on?
	// find block elements within selection
	var selectionRect = goog.style.getBounds( this.selectionEl_ );
	
	var blockEl;
	this.selectedViews_ = [];
	for( var blockID in this.blockViews_) {
		var blockView = this.blockViews_[blockID];
		blockEl = blockView.getLayoutElement();
		var blockRect = goog.style.getBounds( blockEl );
		
		if( selectionRect.contains( blockRect ) ) {
			var block = /** @type {sb.Block} */(this.objMgr_.getObject(blockID));
			sb.ScriptBlocks.getWorkspace().addToSelection( [block] );
			this.selectedViews_.push( blockView );
			blockView.setSelected(true);
		}
	}	

	// hide selection box
	this.selectionEl_.style.display = "none";
}

// Begin drag/drop event handling
/**
 * @param {sb.BlockDragEvent} event
 * @private
 */
sb.ViewManager.prototype.onDragStart_ = function( event ) {
	
	var block = /** @type {sb.Block} */this.objMgr_.getObject( event.blockID );
	
	var blockParent = block.getParent();
	if( goog.isDefAndNotNull( blockParent ) ) {
		blockParent.removeBlock( block );
	}
	else {
		var beforeBlock = block.getBefore();
		if( beforeBlock instanceof sb.Block ) {
			beforeBlock.disconnectAfter();
		}
	}
	
	this.updateOpenSockets(block, true);
	
	goog.events.listen(this.ddMgr_, sb.BlockDragEvent.DRAG_MOVE, this.onDragMove_, false, this);
}

//TODO: consolidate code shared with onDragEnd_ (finding closest block/socket)
/**
 * @param {sb.BlockDragEvent} event
 * @private
 */
sb.ViewManager.prototype.onDragMove_ = function( event ) {
	// check if trash should be open or closed
	var trashBox = this.getTrashBox();
	if(trashBox.contains(event.position)) {
		this.openTrash();
	} 
	else {
		this.closeTrash();
	}
	
	// TODO: move to onDragStart_, save list of possible sockets
	// - check against list here
	
	// find where the block would connect if it was dropped here
	var blockID = event.blockID;
	var blockView = this.getBlockView( event.blockID );
	
	// find the page/view the block will be dropped on
	var pageView = this.getPageViewAtPosition( event.position );
	
	if(pageView == null) {
		return;
	}
	
	var block = /** @type {sb.Block} */ (this.objMgr_.getObject( blockID ));
	var page = /** @type {sb.Page} */ (this.objMgr_.getObject( pageView.getID() ));
	
	var blockPos = /** @type {!goog.math.Coordinate} */(event.position);
	var pagePos = goog.style.getRelativePosition(pageView.getElement(), this.getWorkspaceElement());
	var pageDropPos = goog.math.Coordinate.difference(blockPos, pagePos);
	
	
	var closestSocketName;
	var closestBlockID;
	if(goog.isDefAndNotNull( block.getReturnType() ) && 
			(block.getReturnType() !== sb.DataType.COMMAND || 
					goog.array.indexOf( block.getConnections(), sb.SocketType.BEFORE) != -1)) {
		
		// if there's an open block socket in range of the drop position
		// TODO: get all sockets in range, choose closest open one?
		var closestIDs = this.getClosestSocket( this.openSockets_, pageDropPos, block.getReturnType(), true);
		closestSocketName = closestIDs.socketName;
		closestBlockID = closestIDs.blockID;
	}	
	
	if(goog.isDefAndNotNull( closestSocketName )) {
		var targetParentBlock = sb.ObjectManager.getInstance().getObject( closestBlockID );
		var tarBlockView = this.getBlockView( closestBlockID );
		
		// only set the highlight if the selected socket is not yet highlighted
		if(!goog.isDefAndNotNull( this.targetDropBlockView_ ) ||
				this.targetDropBlockView_ !== tarBlockView ||
				this.targetDropSocketName_ !== closestSocketName ) {	
						
			this.targetDropBlockView_ = tarBlockView;
			this.targetDropSocketName_ = closestSocketName;
			
			var socketRect = tarBlockView.getSocketRect( closestSocketName );
			
			var tarBlockPos = goog.style.getRelativePosition( tarBlockView.getElement(), this.workspaceElement_ );
			
			var hilightRect = new goog.math.Rect( 
					tarBlockPos.x + socketRect.left, tarBlockPos.y + socketRect.top, 
					socketRect.width, socketRect.height );
			
			this.hilightArea( hilightRect );
		}
	}
	else {
		this.removeHilight();
		this.targetDropBlockView_ = null;
		this.targetDropSocketName_ = null;
	}
}

/**
 * @param {sb.Block} targetBlock
 * @param {boolean} includeMidStack
 */
sb.ViewManager.prototype.updateOpenSockets = function( targetBlock, includeMidStack ) {
	var i, j, k;
	var dataType = targetBlock.getReturnType();
	
	this.openSockets_ = [];
	var allPages = this.objMgr_.getObjects(sb.Page.TYPE);
	var allBlocks = [];
	for(i = 0; i < allPages.length; i += 1) {
		var page = allPages[i];
		var pageView = this.pageViews_[page.getID()];
		if(pageView.getVisible()) {
			allBlocks = page.getAllBlocks();
		
			for(j = 0; j < allBlocks.length; j += 1) {
				var block = allBlocks[j];
				if(block === targetBlock) {
					continue;
				}
				var blockView = this.blockViews_[block.getID()];
				var blockPos = goog.style.getRelativePosition(blockView.getElement(), pageView.getElement());
				var blockArgs = block.getArguments();
				
				var socketPos = null;
				for(k = 0; k < blockArgs.length; k += 1) {
					
					var arg = blockArgs[k];
					if(arg.getDataType() !== dataType && arg.getDataType() !== sb.DataType.POLY 
							|| arg.getDataType() === sb.DataType.POLY && dataType === sb.DataType.COMMAND
							|| arg.getSocketType() === sb.SocketType.NONE ) {
						continue;
					}
					
					var argVal = arg.getValue();
					if( !goog.isDefAndNotNull(argVal) || !(argVal instanceof sb.Block) 
							|| arg.getDataType() === sb.DataType.COMMAND && includeMidStack ) {
						// save block/arg combo to list
						socketPos = goog.math.Coordinate.sum(blockPos, /**@type{!goog.math.Coordinate}*/(blockView.getSocketPosition(arg.getName())));
						var blockSocket = {block: block, socketID: arg.getName(), socketPos: socketPos};
						
						this.openSockets_.push(blockSocket);
					}
					
				}
				
				if( goog.array.contains( block.getConnections(), sb.SocketType.AFTER ) 
						&& dataType === sb.DataType.COMMAND) {
					
					if( includeMidStack || !goog.isDefAndNotNull( block.getAfter() )) {
						socketPos = goog.math.Coordinate.sum(blockPos, /**@type{!goog.math.Coordinate}*/(blockView.getSocketPosition(sb.SocketType.AFTER)));
						this.openSockets_.push({block: block, socketID: sb.SocketType.AFTER, socketPos: socketPos});
					}
				}
				
				if( !(goog.isDefAndNotNull(targetBlock.getAfter()))
						&& goog.array.contains( block.getConnections(), sb.SocketType.BEFORE ) 
						&& dataType === sb.DataType.COMMAND) {
					
					if( includeMidStack && !goog.isDefAndNotNull( block.getBefore() ) ) {
						socketPos = goog.math.Coordinate.sum(blockPos, /**@type{!goog.math.Coordinate}*/(blockView.getSocketPosition(sb.SocketType.BEFORE)));
						this.openSockets_.push({block: block, socketID: sb.SocketType.BEFORE, socketPos: socketPos});
					}
				}
				
			}
		}
	}
}

/**
 * @param {sb.BlockDragEvent} event
 * @private
 */
sb.ViewManager.prototype.onDragEnd_ = function( event ) {

	this.closeTrash();
	goog.events.unlisten(this.ddMgr_, sb.BlockDragEvent.DRAG_MOVE, this.onDragMove_, false, this);
	
	var blockID = event.blockID;
	var block;
	
	var dragEl = goog.dom.getElement(blockID);
	
	var deleteBox = false;
	
	var trashBox = this.getTrashBox();
	if(trashBox.contains(event.position)) {
		deleteBox = true;
	}
	
	
	block = /** @type {sb.Block} */ (this.objMgr_.getObject( blockID ));
	
	if(deleteBox) {
		goog.dom.removeNode(dragEl);
		sb.ScriptBlocks.deleteBlock(block);
		return;
	}
	
	var blockView = this.getBlockView( blockID );
	
	
	if(goog.isDefAndNotNull( this.targetDropSocketName_ )) {
		var targetParentBlock = /**@type {sb.Block}*/(this.objMgr_.getObject( this.targetDropBlockView_.getID() ));
		
		if(this.targetDropSocketName_ === sb.SocketType.BEFORE) {
			var page = targetParentBlock.getParent();
			var blockHeight = blockView.getHeight();
			var targetBlockPos = targetParentBlock.getPosition();
			var pos = new goog.math.Coordinate(targetBlockPos.x, targetBlockPos.y - blockHeight + sb.BlockView.hNotchHeight + sb.BlockView.linePadding * 2);
			block.setPosition(pos);
			page.addBlock(block);
			block.connectAfterBlock(targetParentBlock);
		}
		else if(this.targetDropSocketName_ === sb.SocketType.AFTER) {
			var afterBlock = targetParentBlock.getAfter() ;
			
			if(goog.isDefAndNotNull( afterBlock )) {
				targetParentBlock.disconnectAfterBlock();
			}
			
			targetParentBlock.connectAfterBlock( block );
			
			if(goog.isDefAndNotNull( afterBlock )) {
				var lastBlock = block;
				while( goog.isDefAndNotNull( lastBlock.getAfter() )) {
					lastBlock = lastBlock.getAfter();
				}
				lastBlock.connectAfterBlock( afterBlock );
			}
		}
		else {
			if( block.getReturnType() === sb.DataType.COMMAND ) {
				var childBlock = targetParentBlock.getArgument(this.targetDropSocketName_).getValue();
				
				if(goog.isDefAndNotNull( childBlock ) && childBlock instanceof sb.Block) {
					targetParentBlock.removeBlock( childBlock );
				}
				
				targetParentBlock.connectChildBlock( block, this.targetDropSocketName_ );
				
				if(goog.isDefAndNotNull( childBlock )) {
					
					var lastBlock = block;
					while( goog.isDefAndNotNull( lastBlock.getAfter() )) {
						lastBlock = lastBlock.getAfter();
					}
					lastBlock.connectAfterBlock( childBlock );
				}
				
			}
			else {
				targetParentBlock.connectChildBlock( block, this.targetDropSocketName_ );
			}
		}
		
		this.targetDropSocketName_ = null;
		this.targetDropBlockView_ = null;
		this.openSockets_ = [];
	}
	else {
		// find the page/view the block will be dropped on
		var pageView = this.getPageViewAtPosition( event.position, true, 100 );
		
		if(pageView == null) {
			
			goog.dom.removeNode(dragEl);
			this.objMgr_.removeFromMap(block);
			return;
		}

		var page = /** @type {sb.Page} */(this.objMgr_.getObject( pageView.getID() ));
		var blockPos = /** @type {!goog.math.Coordinate} */(event.position);
		var pagePos = goog.style.getRelativePosition(pageView.getElement(), this.getWorkspaceElement());
		var pageDropPos = goog.math.Coordinate.difference(blockPos, pagePos);
		
		pageDropPos.x = Math.max(pageDropPos.x, 0);
		pageDropPos.y = Math.max(pageDropPos.y, 0);
		
		block.setPosition( pageDropPos );
		page.addBlock( block );

		pageView.autoSize( this.getDefaultPageSize() );
	}

	this.removeHilight();
	
	sb.ScriptBlocks.workspace.setDirty();
}
// End drag/drop event handling

/**
 * @param {sb.SBObjectEvent} event
 * @private
 */
sb.ViewManager.prototype.onObjectCreated_ = function( event ) {
	
	switch(event.object.getType()) {
	case "block":
		var block = /**@type {sb.Block}*/(event.object);
		var blockView = this.createBlockView( block );
		break;
		
	case "page":
		var pageView = this.createPageView( /**@type {sb.Page}*/(event.object) );
		this.ddMgr_.addPageListeners( pageView.getElement() );
		break;
	
	case "drawer":
		var drawerView = this.createDrawerView( /**@type {sb.Drawer}*/(event.object) );
		break;
		
	case "workspace":
		break;
		
	default:
		//sb.Logger.log("no view created for: " + event.object.getID());
		break;
	}
	
	if( goog.isDefAndNotNull(sb.ScriptBlocks.workspace) ) {
		sb.ScriptBlocks.workspace.setDirty();
	}
		
}

// TODO: move view deletion code here
/**
 * @param {sb.SBObjectEvent} event
 */
sb.ViewManager.prototype.onObjectDeleted_ = function( event ) {
	
	var id = event.objectID;
	
	switch(event.objectType) {
	case "block":
		var blockView = this.blockViews_[id];
		
		break;
		
	case "page":
		
		break;
	
	case "drawer":
		
		break;
		
	case "workspace":
		break;
		
	default:
		//sb.Logger.log("no view created for: " + event.object.getID());
		break;
	}
}

/**
 * @param {sb.BlockEvent} event
 * @private
 */
sb.ViewManager.prototype.onBlockChange_ = function( event ) {
	
	var block = /** @type {sb.Block}*/event.target;
	var blockView = this.getBlockView( block.getID() );

	switch( event.type ) {
	
	case sb.BlockEvent.MOVED:
		var blockPos = block.getPosition();
		if(goog.isDefAndNotNull(blockPos)) {
			blockView.moveTo( block.getPosition() );
		}
		break;
		
	case sb.BlockEvent.LABEL_CHANGED:
		blockView.layout();
		blockView.redraw();
		
		this.redrawParents( block );
		break;
	
	case sb.BlockEvent.CHILD_CONNECTED:
		
		var childBlock = event.childBlock;
		sb.Logger.log("connecting " + childBlock.getID() + " to " + block.getID());
		
		var childView = this.getBlockView( childBlock.getID() );
		this.ddMgr_.addBlockListeners( childView.getElement() );
		
		blockView.addChild( childView, event.argName );
		blockView.updateSocketPositions();
		blockView.redraw();
		childView.redraw();
		
		this.redrawParents( block );
		
		break;
	
	case sb.BlockEvent.AFTER_CONNECTED:
		
		var afterBlock = event.afterBlock;
		var afterView = this.getBlockView( afterBlock.getID() );
		
		this.ddMgr_.addBlockListeners( afterView.getElement() );
		
		blockView.connectAfterView( afterView );
		
		blockView.redraw();
		this.redrawParents( block );
		break;
	
	case sb.BlockEvent.CHILD_DISCONNECTED:
		var childBlock = event.childBlock;
		var childView = this.getBlockView( childBlock.getID() );
		blockView.removeChild( childView );
		blockView.computeWidth();
		blockView.updateSocketPositions();
		blockView.redraw();
		this.redrawParents( block );
		break;
		
	case sb.BlockEvent.AFTER_DISCONNECTED:
		blockView.removeAfter();
		this.redrawParents( block );
		break;
	
	case sb.BlockEvent.ARGUMENT_REMOVED:
		blockView.removeSocket( event.argument.getName() );
	case sb.BlockEvent.ARGUMENT_ADDED:
		blockView.layout();
		blockView.redraw();
		this.redrawParents( block );
		break;
		
	case sb.BlockEvent.COLOR_CHANGED:
		blockView.drawShape();
		break;
	
	default:	
		
		blockView.redraw();
		break;
		
	}
	
	sb.ScriptBlocks.workspace.setDirty();
}

/**
 * @param {sb.BlockParentEvent} event
 * @private
 */
sb.ViewManager.prototype.onBlockParentChange_ = function( event ) {
	
	var target = /**@type {sb.SBObject} */event.target;
	
	switch( target.getType() ) {
		
	case "page":
		
		var pageView = this.getPageView( target.getID() );
		var block, blockView;
		if( event.type === sb.BlockParentEvent.BLOCK_ADDED ) {
			
			var views = [];
			for( var i = 0; i < event.children.length; i += 1 ) {
				block = event.children[i];
				
				blockView = this.getBlockView( block.getID() );
				var blockPos = block.getPosition();
				if(goog.isDefAndNotNull(blockPos)) {
					blockView.moveTo( blockPos );
				}
				
				this.ddMgr_.addBlockListeners( blockView.getElement() );
				this.addChildListeners_(block);
				
				views.push(blockView);
			}
			pageView.addBlockView( views[0] );
		}
		else if( event.type == sb.BlockParentEvent.BLOCK_REMOVED ) {
			
			if(!this.ddMgr_.isDragging() ) {
				block = event.children[0];
				blockView = this.getBlockView( block.getID() );
				
				pageView.removeBlockView( blockView );

				goog.dom.appendChild( this.hiddenEl_, blockView.getElement() );
			}
		}
		else if( event.type == sb.BlockParentEvent.ALL_BLOCKS_REMOVED ) {
			
			pageView.clear();
		}
		
		if( !this.ddMgr_.isDragging() ) {
			pageView.autoSize( this.getDefaultPageSize() );
		}
		break;
		
	case "drawer":
		
		var drawerView = this.getDrawerView( target.getID() );
		
		if(event.type === sb.BlockParentEvent.BLOCK_ADDED) {
			
			var views = [];
			for(var i = 0; i < event.children.length; i += 1) {
				var block = event.children[i];
				block.isFactory = true;
				var blockView = this.getBlockView( block.getID() );
				var blockEl = blockView.getElement();
				
				goog.events.unlisten( blockEl, goog.events.EventType.CLICK, this.onBlockClick_, false, this );
				
				this.ddMgr_.addBlockListeners( blockEl, true );
				
				views.push( blockView );
			}
			drawerView.addBlockView( views[0] );
		}
		else if( event.type == sb.BlockParentEvent.BLOCK_REMOVED ) {
			
			if(!this.ddMgr_.isDragging() ) {
				block = event.children[0];
				blockView = this.getBlockView( block.getID() );
				
				drawerView.removeBlockView( blockView );
			}
		}
		else if( event.type == sb.BlockParentEvent.ALL_BLOCKS_REMOVED ) {
			
			drawerView.clear();
		}
		break;
		
	}
	
	sb.ScriptBlocks.workspace.setDirty();
		
}


/**
 * @param {Event} event
 */
sb.ViewManager.prototype.onDrawerChange_ = function(event) {
	
	var drawerID = this.drawerControls_.options[ this.drawerControls_.selectedIndex ].value;
	var drawer = this.objMgr_.getObject(drawerID, "drawer");
	
	sb.ScriptBlocks.workspace.setCurrentDrawer(drawer);
}

/**
 * @param {sb.SBObjectEvent} event
 * @private
 */
sb.ViewManager.prototype.onNullParent_ = function(event) {
	var obj = event.target;
	if( obj instanceof sb.Block ) {
		var view = this.getBlockView( obj.getID() );
		var blockEl = view.getElement();
		if( blockEl !== this.ddMgr_.getCurrentDragElement() ) {
			goog.dom.appendChild( this.hiddenEl_, view.getElement() );
		}
	}
}


/**
 * @param {sb.WorkspaceEvent} event
 * @private
 */
sb.ViewManager.prototype.onWorkspaceChange_ = function(event) {
	
	switch( event.type ) {
	
	case sb.WorkspaceEvent.PAGE_ADDED:
		var page = event.page;
		this.addPageToWorkspace(page.getID());
		break;
		
	case sb.WorkspaceEvent.DRAWER_ADDED:
		var drawer = event.drawer;
		this.addDrawerToWorkspace(drawer.getID());
		break;

	case sb.WorkspaceEvent.PAGE_CHANGED:
		var page = event.page;
		var pageView = this.pageViews_[page.getID()];
		this.showPage_(pageView);
		break;
		
	case sb.WorkspaceEvent.DRAWER_CHANGED:
		var drawer = event.drawer;
		var drawerView = this.drawerViews_[drawer.getID()];
		this.showDrawer_(drawerView);
		break;
		
	case sb.WorkspaceEvent.SELECTION_CHANGED:
		var sel = sb.ScriptBlocks.workspace.getSelection();
		var oldSelViews = goog.array.clone( this.selectedViews_ );
		var i, block, blockView;
		for(i = 0; i < oldSelViews.length; i += 1) {
			blockView = oldSelViews[i];
			block = blockView.getID();
			if( !goog.array.contains( sel, block ) ) {
				blockView.setSelected( false );
				goog.array.remove( this.selectedViews_, blockView );
			}
		}
		for(i = 0; i < sel.length; i += 1) {
			block = sel[i];
			blockView = this.getBlockView( block.getID() );
			
			if( !goog.array.contains( this.selectedViews_, blockView ) ) {
				blockView.setSelected( true );
				this.selectedViews_.push( blockView );
			}
		}
		
		break;
	}
	
	sb.ScriptBlocks.workspace.setDirty();
}

/**
 * @param {goog.events.Event} event
 * @private
 */
sb.ViewManager.prototype.onArgumentChange_ = function( event ) {
	
	var arg = /**@type {sb.Argument} */event.target;
	
	// find the Block this argument belongs to
	var allBlocks = /**@type {Array.<sb.Block>}*/(this.objMgr_.getObjects(sb.Block.TYPE));
	var block;
	for(var i = 0; i < allBlocks.length; i += 1) {
		var tempBlock = allBlocks[i];
		if(goog.array.contains( tempBlock.getArguments(), arg )) {
			block = tempBlock;
			break;
		}
	}
	// find and update the BlockView
	var blockView = this.getBlockView( block.getID() );
	
	switch(event.type) {
	
	case sb.ArgumentEvent.VALUE_CHANGED: 
	
		var value = arg.getValue();
		if(!(value instanceof sb.Block)) {
			blockView.updateArgValue(arg.getName(), value);
		}
		break;
		
	case sb.ArgumentEvent.OPTIONS_CHANGED:
		// test if block is not displayed
		blockView.updateArgOptions(arg.getName());
		break;
	}
	
	blockView.redraw();
	
	this.redrawParents(block);
	
	sb.ScriptBlocks.workspace.setDirty();
}

/**
 * @param {sb.Block} block
 */
sb.ViewManager.prototype.addChildListeners_ = function(block) {
	
	var childBlocks = block.getChildBlocks();
	for(var i = 0; i < childBlocks.length; i += 1) {
		var childView = this.getBlockView( childBlocks[i].getID() );
		this.ddMgr_.addBlockListeners( childView.getElement() );
		
		this.addChildListeners_( childBlocks[i] );
	}
	var afterBlock = block.getAfter();
	if(afterBlock !== null) {
		var afterView = this.getBlockView( afterBlock.getID() );
		this.ddMgr_.addBlockListeners( afterView.getElement() );
		this.addChildListeners_( afterBlock );
	}
	
}


/**
 * @param {sb.Block} block
 * @private
 */
sb.ViewManager.prototype.redrawParents = function( block ) {
	
	var blockParent = block.getParent();
	
	while( !goog.isDefAndNotNull( blockParent ) && goog.isDefAndNotNull(block.getBefore()) ) {
		block = block.getBefore();
		var blockView = this.getBlockView( block.getID() );
		blockView.computeHeight();
		blockParent = block.getParent();
	}
	
	if( blockParent instanceof sb.Block ) {
		var blockParentView = this.getBlockView( blockParent.getID() );
		
		var arg;
		var parentArgs = blockParent.getArguments();
		for(var i = 0; i < parentArgs.length; i += 1) {
			if(parentArgs[i].getValue() === block) {
				arg = parentArgs[i];
			}
		}
		blockParentView.updateSocketPositions();
		blockParentView.redraw();
		this.redrawParents( blockParent );
	}
	else if( blockParent instanceof sb.Page ) {
		var pageView = this.getPageView( blockParent.getID() );
		pageView.autoSize( this.getDefaultPageSize() );
	}
}

/**
 * @param {Element} element
 */
sb.ViewManager.isBlockElement = function( element ) {
	if(element.id.indexOf('block') !== -1) {
		return true;
	}
	else {
		return false;
	}
}