goog.provide('sb.BlockViewEvent');

goog.require('goog.events.Event');

/**
 * An event caused by user interaction with a BlockView.
 * 
 * @param {string} type The type of event
 * @extends {goog.events.Event}
 * @constructor 
 */
sb.BlockViewEvent = function( type ) {
	this.type = type;
}
goog.inherits(sb.BlockViewEvent, goog.events.Event);


/** @define {string} */
sb.BlockViewEvent.INPUT_CHANGED = "blockViewInputChanged"; 

/** @define {string} */
sb.BlockViewEvent.LABEL_CHANGED = "blockViewLabelChanged"; 