goog.provide('sb.PageView');


goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.style');
goog.require('goog.math.Size');
goog.require('goog.math.Coordinate');
goog.require('goog.object');

/**
 * @param {sb.AbstractBlockParent} model
 * @param {Element=} element
 * @param {string=} name
 * @param {boolean=} showGrid
 * @param {goog.math.Size=} size 
 * 
 * @extends {goog.events.EventTarget}
 * @constructor
 */
sb.PageView = function( model, element, name, showGrid, size ) {
	
	this.padding_ = 20;
	
	/**
	 * @type {sb.AbstractBlockParent}
	 * @private
	 */
	this.page_ = model;
	
	// TODO: figure out how to handle assigned/unassigned elements
	// size - always retrieved from the element, or set as a property?
	if(!goog.isDefAndNotNull(element)) {
		element = document.createElement('div');
	} 
	else {
		if(!goog.isDefAndNotNull( size )) {
			size = new goog.math.Size( 500, 500 );
		}
		goog.style.setSize( element, size );
	}
	
	this.element_ = element;
	
	this.width = element.style.width;
	this.height = element.style.height;
	element.className="sbPage";
	element.id = this.page_.getID();
	
	this.name_ = name;
	
	this.showGrid_ = showGrid;
	if(showGrid) {
		this.drawGrid(sb.PageView.defaultGridSize_);
	}
}
goog.inherits(sb.PageView, goog.events.EventTarget);

/**
 * @type {number}
 * @private
 */
sb.PageView.defaultGridSize_ = 20;

/**
 * @return {string}
 */
sb.PageView.prototype.getID = function() {
	return this.page_.getID();
}

sb.PageView.prototype.getName = function() {
	return this.name_;
}

/**
 * Creates an HTML element for this page view and assigns it the id and current width and height.
 */
sb.PageView.prototype.createElement = function() {
	
	this.element_ = document.createElement( 'div' );
	goog.style.setSize( this.element_, this.width, this.height);
	this.element_.id = this.page_.getID();
}

/**
 * @param {Element} newElement
 */
sb.PageView.prototype.setElement = function( newElement ) {
	
	if(goog.isDefAndNotNull( this.element_ )) {
		goog.dom.removeNode( this.element_ );
	}
	this.element_ = newElement;
	var elSize = goog.style.getSize( newElement );
	this.width = elSize.width;
	this.height = elSize.height;
	
	if(this.showGrid_) {
		this.drawGrid(sb.PageView.defaultGridSize_);
	}
}

/**
 * @return {Element}
 */
sb.PageView.prototype.getElement = function() {
	
	return this.element_;
}

/**
 * @param {sb.BlockView} blockView
 * @param {goog.math.Coordinate=} position
 */
sb.PageView.prototype.addBlockView = function(blockView, position) {
	
	if(goog.isDefAndNotNull( position )) {
		
		blockView.moveTo( position );
		
	}
	else {
		// find the right edge of the rightmost block
		var nextBlockPos = this.padding_;
		var i = 0;
		while(i < this.element_.children.length) {
			
			var childEl = this.element_.children[i];
			var blockRight = goog.style.getPosition( childEl ).x 
				+ goog.style.getSize( childEl ).width + this.padding_;
			
			if( blockRight > nextBlockPos ) {
				nextBlockPos = blockRight;
			}
			
			i += 1;
		}
		
		var blockEl = blockView.getElement();
		if( goog.style.getStyle( blockEl, "left" ) == "" ) {
			goog.style.setPosition(blockEl, nextBlockPos, this.padding_);
		}
	}
	
	blockView.getElement().style.position = "absolute";
	if( !goog.isNull( this.element_ ) ) {
		goog.dom.append( this.element_, blockView.getElement() );
	}
	
	blockView.setParent(this);
}

/**
 * @param {Array.<sb.BlockView>} blockViews
 */
sb.PageView.prototype.addBlockViews = function(blockViews) {
	
	if( goog.isNull( this.element_ ) ) {
		return;
	}
	
	if( blockViews.length === 1 ) {
		this.addBlockView(blockViews[0]);
	}
	else {
		
		var elClone,
			i;
		
		elClone = goog.dom.htmlToDocumentFragment( 
			this.element_.outerHTML
		)
		
		for( i = 0; i < blockViews.length; i += 1) {
			goog.dom.append( elClone, blockViews[i].getElement() );
			blockViews[i].setParent(this);
		}
		
		goog.dom.replaceNode( elClone, this.element_);
	}
}

/**
 * @param {sb.BlockView} blockView
 */
sb.PageView.prototype.removeBlockView = function( blockView ) {
	goog.dom.removeNode( blockView.getElement() );
	
}

/**
 * Removes all block views from this page.
 */
sb.PageView.prototype.clear = function() {
	
	goog.dom.removeChildren( this.element_ );
	
	if(this.showGrid_) {
		this.drawGrid(sb.PageView.defaultGridSize_);
	}
}

/**
 * Returns the rectangle describing the (client) position and size of this PageView.
 * 
 * @return {goog.math.Rect}
 */
sb.PageView.prototype.getRectangle = function() {
	
	var pageRect = goog.style.getBounds( this.element_ );
	
	return pageRect;
}

/**
 * @return {goog.math.Coordinate}
 */
sb.PageView.prototype.getPosition = function() {
	return goog.style.getPageOffset(this.element_);
}

/**
 * @param {number} cellSize
 * @private
 */
sb.PageView.prototype.drawGrid = function( cellSize ) {
	
	var canvas = document.createElement("canvas");
	canvas.width = goog.style.getSize(this.element_).width;
	canvas.height = goog.style.getSize(this.element_).height;
	
	goog.dom.appendChild(this.element_, canvas);
	
	var ctx = canvas.getContext("2d");
	
	ctx.strokeStyle = "#CCCCCC";
	
	for(var r = cellSize - 0.5; r < canvas.height - 1; r += cellSize) {
		
		ctx.moveTo(0.5, r);
		ctx.lineTo(canvas.width - 0.5, r);
	}
	
	for(var c = cellSize - 0.5; c < canvas.width - 1; c += cellSize) {
		ctx.moveTo(c, 0.5);
		ctx.lineTo(c, canvas.height - 0.5);
	}
	ctx.stroke();
	
	this.canvas_ = canvas;
}

sb.PageView.prototype.hide = function() {
	
	this.element_.style.display = "none";
}

sb.PageView.prototype.show = function() {
	
	this.element_.style.display = "block";
}

sb.PageView.prototype.getVisible = function() {
	return (this.element_.style.display !== "none");
}

/**
 * Adjust the size of the page element to fit all blocks.
 * 
 * @param {goog.math.Size|number=} 	minSize 	Minimum size or width.
 * @param {number=} 				minHeight 	Minimum height.
 */
sb.PageView.prototype.autoSize = function( minSize, minHeight ) {
	
	var scrollTop = this.element_.scrollTop;
	var scrollLeft = this.element_.scrollLeft;
	
	var maxSize = new goog.math.Coordinate(0, 0);
	
	var childEls = this.element_.children;
	
	for(var i = 0; i < childEls.length; i += 1) {
		var blockEl = childEls[i];
		var childPos = goog.style.getPosition( blockEl );
		var childSize = goog.style.getSize( blockEl );
		
		var btmRightCorner = new goog.math.Coordinate(
				childPos.x + childSize.width, childPos.y + childSize.height);
		if(btmRightCorner.x > maxSize.x) {
			maxSize.x = btmRightCorner.x;
		}
		if(btmRightCorner.y > maxSize.y) {
			maxSize.y = btmRightCorner.y;
		}
	}
	
	var newWidth = maxSize.x + 10;
	var newHeight = maxSize.y + 10;
	
	var minWidth;
	if(minSize instanceof goog.math.Size) {
		minWidth = minSize.width;
		minHeight = minSize.height;
	}
	else {
		minWidth = minSize;
	}
	
	if(goog.isDefAndNotNull(minWidth) && newWidth < minWidth) {
		newWidth = "100%";
	}
	if(goog.isDefAndNotNull(minHeight) && newHeight < minHeight) {
		newHeight = "100%";
	}
	goog.style.setWidth(this.element_, newWidth);
	goog.style.setHeight(this.element_, newHeight);
	//goog.style.setSize(this.element_, newSize);
	
	// scroll position is reset when the element is resized
	// - put it back at the previous scroll position.
	//this.element_.scrollTop = scrollTop;
	//this.element_.scrollLeft = scrollLeft;
}
