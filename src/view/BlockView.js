goog.provide('sb.BlockView');

goog.require('sb.Block');
goog.require('sb.DataType');
goog.require('sb.SocketType');
goog.require('sb.SocketShapes');
goog.require('sb.BlockViewEvent');
goog.require('sb.Utils');

goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.graphics');
goog.require('goog.style');
goog.require('goog.array');
goog.require('goog.string');
goog.require('goog.math.Coordinate');
goog.require('goog.events.EventTarget');
goog.require('goog.json');

goog.require('goog.ui.AnimatedZippy');


/**
 * @param {string} id
 * @param {sb.Block} block
 * @param {number} width
 * @param {number} height
 * @extends {goog.events.EventTarget}
 * @constructor
 */
sb.BlockView = function( id, block, width, height ) {
	
	//private properties
	this.element_ = null;
	
	this.childViews_ = {};
	
	this.parentView_ = null;
	this.beforeView_ = null;
	this.afterView_ = null;
	this.canvas_ = null;
	
	this.emptyHeight_ = height;
	
	this.id_ = id;
	this.block_ = block;
	
	this.listeners_ = [];
	
	this.selected_ = false;
	
	// key is argument name
	this.sockets_ = {};
	/**
	 * @type {Object.<goog.math.Coordinate>}
	 * @private
	 */
	this.socketPositions_ = {};
	
	this.lines_ = [];
	
	// create the DOM elements
	this.element_ = document.createElement( 'div' );

	this.element_.style.position = 'absolute';
	this.element_.id = this.id_;
	
	goog.style.setSize(this.element_, width, height);
	
	/**
	 * @type {goog.graphics.AbstractGraphics}
	 * @private
	 */
	this.graphics_;// = goog.graphics.createGraphics(width, height);
	if(sb.Utils.supportsCanvas()) {
		this.graphics_ = new goog.graphics.CanvasGraphics(width, height);
	}
	else {
		this.graphics_ = new goog.graphics.VmlGraphics(width, height);
	}
	
	goog.events.EventTarget.call( this ); 
	this.registerDisposable(this.graphics_);
	
	this.graphics_.createDom();
	this.graphics_.render();

	this.canvas_ = this.graphics_.getElement();
	this.canvas_.style.position = 'absolute';
	
	goog.dom.appendChild(this.element_, this.canvas_);

	this.layoutElement_ = document.createElement( 'div' );
	goog.dom.classes.add( this.layoutElement_, "sbBlock" );
	
	this.layoutParentEl_ = document.createElement('div');
	this.layoutParentEl_.style.position = "absolute";
	
	goog.dom.appendChild(this.layoutParentEl_, this.layoutElement_);
	goog.dom.appendChild(this.element_, this.layoutParentEl_);
	
	//this.redraw();
	sb.Logger.log("created " + this.id_);
}
goog.inherits(sb.BlockView, goog.events.EventTarget);

/**
 * @define {string}
 */
sb.BlockView.DEFAULT_COLOR = "#9999FF";

/**
 * @return {string}
 */
sb.BlockView.prototype.getID = function() {
	return this.id_;
}

/**
 * @return {number}
 */
sb.BlockView.prototype.getWidth = function() {
	var width = goog.style.getSize(this.layoutElement_).width;
	return width;
}

/**
 * @return {number}
 */
sb.BlockView.prototype.getHeight = function() {
	var height = goog.style.getSize(this.layoutElement_).height;
	if(this.hasAfterSocket()) {
		height += sb.BlockView.hNotchHeight;
	}
	return height;
}

/**
 * @return {Element}
 */
sb.BlockView.prototype.getElement = function() {
	
	return this.element_;
}

/**
 * @return {Element}
 */
sb.BlockView.prototype.getLayoutElement = function() {
	
	return this.layoutElement_;
}

/**
 * @param {string} socketName
 * 
 * @return {Element}
 */
sb.BlockView.prototype.getSocketElement = function( socketName ) {
	
	return this.sockets_[socketName];
}

/**
 * @return {goog.math.Coordinate}
 */
sb.BlockView.prototype.getPosition = function() {
	return goog.style.getPosition(this.element_);
}

/**
 * @param {string} argName
 * 
 * @return {goog.math.Coordinate}
 */
sb.BlockView.prototype.getSocketPosition = function(argName) {
	return this.socketPositions_[argName];
}

/**
 * @return {Object}
 */
sb.BlockView.prototype.updateSocketPositions = function() {
	
	this.socketPositions_ = {};
	for (var argName in this.sockets_) {
		var arg = this.block_.getArgument(argName);
		if(arg.getSocketType() !== sb.SocketType.NONE) {
			var pos = goog.style.getRelativePosition(this.sockets_[argName], this.layoutElement_);
			this.socketPositions_[argName] = goog.style.getRelativePosition(this.sockets_[argName], this.layoutElement_);
		}
	}
	
	if(goog.isDefAndNotNull(this.afterSocketEl_)) {
		var x = 0;
		var y = goog.style.getSize(this.layoutElement_).height;
		this.socketPositions_[sb.SocketType.AFTER] = new goog.math.Coordinate(x, y);
	}
	
	if(goog.array.contains(this.block_.getConnections(), sb.SocketType.BEFORE)) {
		this.socketPositions_[sb.SocketType.BEFORE] = new goog.math.Coordinate(0, 0);
	}
	
	return goog.object.clone(this.socketPositions_);
}

/**
 * @param {sb.BlockView|sb.PageView|sb.DrawerView} parentView
 */
sb.BlockView.prototype.setParent = function(parentView) {
	this.parentView_ = parentView;
	this.setParentEventTarget(parentView);
}

/**
 * @return {!sb.BlockView|sb.PageView|sb.DrawerView}
 */
sb.BlockView.prototype.getParent = function() {
	return this.parentView_;
}

/**
 * Add the childView to the list of child views,
 * append its element to this view's element
 * and move it to the correct socket position.
 * 
 * @param {sb.BlockView} childView
 * @param {string} argName
 */
sb.BlockView.prototype.addChild = function( childView, argName ) {
	
	var childEl = childView.getElement();
	
	var socketEl = this.sockets_[argName];
	goog.dom.appendChild( socketEl, childEl );
	goog.style.setPosition( childEl, 0, 0 );
	childEl.style.position = "relative";
	
	var arg = null;
	var blockArgs = this.block_.getArguments();
	for(var i=0; i < blockArgs.length; i += 1) {
		if(blockArgs[i].getName() == argName) {
			arg = blockArgs[i];
			break;
		}
	}
	switch(arg.getSocketType()) {
		case "internal":
			var inputBox = goog.dom.getElementsByTagNameAndClass("input", null, socketEl)[0];
			inputBox.style.display = "none";
			this.computeWidth();
			break;
		case "list":
			var selectBox = goog.dom.getElementsByTagNameAndClass("select", null, socketEl)[0];
			selectBox.style.display = "none";
			this.computeWidth();
			break;
		case "nested":
			break;
	}
	
	this.childViews_[argName] = childView;
	childView.setParent(this);
	
	this.computeHeight();
}

/**
 * @return {boolean}
 */
sb.BlockView.prototype.removeChild = function( childView ) {
	
	for( var argName in this.childViews_ ) {
		if(this.childViews_[argName] === childView) {
			this.childViews_[argName] = null;
		
			var childEl = childView.getElement();
			if(goog.dom.contains(this.element_, childEl)) {
				goog.dom.removeNode(childEl);
			}
			this.updateArgValue(argName, null);
			
			break;
		}
		
	}
	
	return (childEl !== undefined && childEl !== null);
}

/**
 * Set the afterView_ of this block view,
 * append its element to this view's element
 * and move it to the correct socket position.
 * 
 * @param {sb.BlockView} afterView
 */
sb.BlockView.prototype.connectAfterView = function( afterView ) {
	
	var afterEl = afterView.getElement();
	afterEl.style.position = "relative";
	goog.style.setPosition( afterEl, 0, 0 );
	
	goog.dom.appendChild(this.afterSocketEl_, afterEl);
	
	this.afterView_ = afterView;
	
	afterView.setParentEventTarget(this);
	this.computeHeight();
}

/**
 * @return {boolean}
 */
sb.BlockView.prototype.removeAfter = function() {
	if(this.afterView_ === null || this.afterView_ === undefined ) {
		return false;
	}
	
	var afterEl = this.afterView_.getElement();
	if(goog.dom.contains(this.element_, afterEl)) {
		goog.dom.removeNode(afterEl);
	}
	
	this.afterView_ = null;
	
	this.redraw();
	
	return true;
}

/**
 * @return {sb.BlockView}
 */
sb.BlockView.prototype.getAfterView = function() {
	return this.afterView_;
}

/**
 * @return {sb.BlockView}
 */
sb.BlockView.prototype.getBeforeView = function() {
	return this.beforeView_;
}

/**
 * @param {string} argName
 */
sb.BlockView.prototype.removeSocket = function( argName ) {
	goog.object.remove( this.sockets_, argName );
	goog.object.remove( this.socketPositions_, argName );
}

/**
 * @param {goog.math.Coordinate} position
 */
sb.BlockView.prototype.moveTo = function( position ) {
	goog.style.setPosition( this.element_, position.x, position.y );
	this.element_.style.position = "absolute";
}

sb.BlockView.prototype.redraw = function() {
	
	for( var i = 0; i < this.childViews_.length; i += 1 ) {
		var childBlockView = this.childViews_[i];
		childBlockView.redraw();
	}
	this.computeWidth();
	this.computeHeight();
	this.updateSocketPositions();
	
	this.drawShape();
}

/**
 * Returns the total width of a block and all of its connected blocks.
 * 
 * @return {number}
 */
sb.BlockView.prototype.getRecursiveWidth = function() {
	
	var totalWidth = goog.style.getSize(this.element_).width;
	var tempWidth;
	
	var blockArgs = this.block_.getArguments();
	if(blockArgs !== undefined) {
		
		for(var i = 0; i < blockArgs.length; i += 1) {
			var arg = blockArgs[i];
			var argName = arg.getName();
			var childView = this.childViews_[argName];
			
			if(childView !== null && childView !== undefined) {
				tempWidth = goog.style.getRelativePosition(
						childView.getElement(), this.element_ ).x;
				
				tempWidth += childView.getRecursiveWidth();
				
				if(tempWidth > totalWidth) {
					totalWidth = tempWidth;
				}
			}
		}
	}
	
	var connections = this.block_.getConnections(); 
	if( connections !== undefined && goog.array.contains(connections, sb.SocketType.AFTER) ) {
		
		if( this.afterView_ !== null && this.afterView_ !== undefined ) {
			tempWidth = this.afterView_.getRecursiveWidth();
			if(tempWidth > totalWidth) {
				totalWidth = tempWidth;
			}
		}
	}
	
	return totalWidth;
}

/**
 * @return {number}
 */
sb.BlockView.prototype.getRecursiveHeight = function() {
	
	var	totalHeight = goog.style.getSize(this.layoutParentEl_).height;
	
	return totalHeight;
}

/**
 * Removes the listeners from internal elements.
 */
sb.BlockView.prototype.clearListeners = function() {
	for(var i = 0; i < this.listeners_.length; i += 1) {
		goog.events.unlistenByKey( this.listeners_[i] );
	}
	this.listeners_ = [];
}

/**
 * @override
 */
sb.BlockView.prototype.disposeInternal = function() {
	sb.BlockView.superClass_.disposeInternal.call( this );
	
	this.clearListeners();
	
	this.block_ = null;
	
	this.layoutElement_ = null;
	this.layoutParentEl_ = null;
	this.afterSocketEl_ = null;
	
	this.sockets_ = {};
	this.socketPositions_ = {};
	this.afterView_ = null;
	this.beforeView_ = null;
	this.parentView_ = null;

	this.canvas_ = null;
	this.graphics_ = null;

}

/**
 * Rebuilds the DOM elements of the view.
 */
sb.BlockView.prototype.layout = function() {
	var i, j;
	
	this.clearListeners();
	
	// clear the existing elements
	goog.dom.removeChildren(this.layoutElement_)
	goog.dom.removeNode(this.afterSocketEl_);
	
	// create and layout label and socket elements
	var labelText = this.block_.getLabel();
	if(labelText !== undefined) {
		
		var labelLines = labelText.split('\n');
		if(labelLines[labelLines.length-1] == "") {
			labelLines.length -= 1;
		}
		
		for(i = 0; i < labelLines.length; i += 1) {
			
			var lineEl = document.createElement('div');
			goog.dom.classes.add( lineEl, "sbBlockLine" );
			this.lines_[i] = lineEl;
			
			labelLines[i] = goog.string.trim(labelLines[i]);
			var lineParts = labelLines[i].split(' ');
			
			j = 0;
			var part = lineParts[0];
			
			while(part !== undefined) {
				// if this part of the label is an argument
				if(part.charAt(0) === '@') {
					
					var argName = part.substr(1);
					var arg = this.block_.getArgument(argName);
					var argSocketType = arg.getSocketType();
					
					// create a socket
					var socketEl = document.createElement('div');
					switch( argSocketType ) {
					case sb.SocketType.INTERNAL: 
						goog.dom.classes.add( socketEl, "sbSocket", "sbInternal" );
					
						var inputBox = document.createElement('input');
						goog.style.setSize( inputBox, 40, 18 );
						goog.dom.classes.add( inputBox, sb.BlockView.styleNames.INTERNAL_INPUT );
						
						if(arg.getDataType() == sb.DataType.NUMBER) {
							sb.InputManager.addInputRestriction( inputBox, sb.InputManager.NUMBER_ONLY );
						}
						
						var val = arg.getValue();
						if(goog.isDefAndNotNull(val)) {
							if(goog.isString(val) || goog.isNumber(val)) {
								inputBox.value = val;
								
								sb.BlockView.autoSizeText( inputBox );
							} 
							else if(val.getType() === sb.Block.TYPE) {
								inputBox.style.display = "none";
							}
						}
						
						
						if(this.block_.isFactory) {
							inputBox.style.cursor = "default";
							inputBox.enabled = false;
							inputBox.tabIndex = "-1";
						} 
						else {
							this.listeners_.push( goog.events.listen( inputBox, goog.events.EventType.MOUSEDOWN, sb.Utils.stopEvent_) );
							this.listeners_.push( goog.events.listen( inputBox, goog.events.EventType.INPUT, this.onInputChange_, false, this) );
						}
						
						inputBox.onDragStart = function() {return false};
						
						goog.dom.appendChild( socketEl, inputBox );
						break;
						
					case  sb.SocketType.NESTED:
						goog.dom.classes.add( socketEl, "sbSocket", "sbNested" );
						
						// make sure any nested sockets are the last part on the line
						if(j != lineParts.length - 1) {
							// if not, split the rest of the line into a new line
							var remParts = goog.array.splice(lineParts, j + 1, lineParts.length - (j + 1));
							goog.array.splice(labelLines, i + 1, 1, remParts.join(" "));
						}
						break;
						
					case sb.SocketType.EDGE: 
						goog.dom.classes.add( socketEl, "sbSocket", sb.BlockView.styleNames.EDGE );
						
						// make sure any nested sockets are the last part on the line
						if(j != lineParts.length - 1) {
							// if not, split the rest of the line into a new line
							var remParts = goog.array.splice(lineParts, j + 1, lineParts.length - (j + 1));
							goog.array.splice(labelLines, i + 1, 1, remParts.join(" "));
						}
						
						goog.style.setStyle( lineEl, 'text-align', 'right');
						break;
						
					case sb.SocketType.LIST:
						goog.dom.classes.add( socketEl, "sbSocket", "sbInternal" );
					
						var selectEl = document.createElement('select');
						
						var options = (/**@type {sb.ListArgument}*/arg).getOptions();
						for(var l = 0; l < options.length; l += 1) {
							var optionEl = document.createElement('option');
							
							optionEl.innerHTML = goog.string.htmlEscape(options[l].label);
							optionEl.value = options[l].value;
							
							goog.dom.appendChild( selectEl, optionEl );
						}
						
						var val = arg.getValue();
						if(goog.isDefAndNotNull(val)) {
							selectEl.value = val;
						}
						
						this.listeners_.push( 
								goog.events.listen( selectEl, goog.events.EventType.MOUSEDOWN, sb.Utils.stopEvent_, false, this) );
						
						if(this.block_.isFactory) {
							selectEl.disabled = true;
						}
						else {
							this.listeners_.push( 
									goog.events.listen( selectEl, goog.events.EventType.CHANGE, this.onInputChange_, false, this) );
						}
						
						goog.dom.appendChild( socketEl, selectEl );
						break;
						
					// editable label
					case sb.SocketType.NONE:
						socketEl = document.createElement('input');
						goog.dom.classes.add( socketEl, 'sbEditableLabel');
						socketEl.value = arg.getValue();
						socketEl.type = 'text';
						
						var onEdLabelClick = function(event) {
							
							var el = event.currentTarget;
							goog.dom.classes.remove(el, 'sbUnselectable');
							el.style['-moz-user-select'] = 'text';
							el.focus();
							el.select();
							
						};
						this.listeners_.push( 
								goog.events.listen( socketEl, goog.events.EventType.DBLCLICK, onEdLabelClick ) );
						this.listeners_.push( 
								goog.events.listen( socketEl, goog.events.EventType.INPUT, this.onInputChange_, false, this) );
						sb.BlockView.autoSizeText( socketEl );
						
						break;
						
					default:
						break;
					}
					
					goog.dom.appendChild( lineEl, socketEl );
					
					this.sockets_[argName] = socketEl;
				}
				//plain text part of the label
				else {
					
					// combine non-socket parts back into a single string
					var nextPart = lineParts[j + 1];
					while(nextPart !== undefined && nextPart.charAt(0) !== '@') {
						lineParts[j] = lineParts[j] + " " + nextPart;
						lineParts.splice(j+1, 1);
						nextPart = lineParts[j + 1];
					}
					part = lineParts[j];
					
					if(part !== '') {
						// create a label
						var labelEl = document.createElement('div');
						goog.dom.classes.add( labelEl, 'sbLabel');
						
						goog.dom.setTextContent(labelEl, part);
						
						goog.dom.appendChild(lineEl, labelEl);
					}
				}
				
				j += 1;
				part = lineParts[j];
			}
			
			goog.dom.appendChild(this.layoutElement_, lineEl);
		}
	}
	
	// place child block views
	for(var argName in this.childViews_) {
		var childView = this.childViews_[argName];
		if(goog.isDefAndNotNull(childView)) {
			this.sockets_[argName].appendChild(childView.getElement());
			
			var arg = this.block_.getArgument( argName );
			var socketType = arg.getSocketType();
		}
	}
	
	// find the longest line element, and set the width of this block
	// (inline-block elements will only stay on the same line if the parent element is wide enough)
	
	this.computeWidth();
	
	
	// place after socket/block view
	var connections = this.block_.getConnections();
	if( connections !== undefined ) {
		
		if( goog.array.indexOf( connections, sb.SocketType.BEFORE ) !== -1
				|| this.block_.getReturnType() === sb.DataType.COMMAND ) {
			goog.dom.classes.add(this.layoutElement_, "sbBeforeConBlock");
		}
		
		if( goog.array.indexOf( connections, sb.SocketType.AFTER ) !== -1 ) {
			this.afterSocketEl_ = document.createElement('div');
			goog.dom.classes.add( this.afterSocketEl_, "sbAfterSocket" );
			goog.dom.appendChild(this.layoutParentEl_, this.afterSocketEl_);
			if(this.afterView_ !== null) {
				this.connectAfterView(this.afterView_);
				this.afterView_.redraw();
			}
		}
	}
	
	// populate this.socketPositions_
	this.updateSocketPositions();
	
	// subtract 1 to account for overlapping borders
	var lpHeight = goog.style.getSize(this.layoutParentEl_).height;
	lpHeight = lpHeight >= 1 ? lpHeight - 1 : 0;
	
	goog.style.setHeight(this.element_, lpHeight);
}

/**
 * Updates the text value of the input element.
 * 
 * @param {string} argName
 * @param {string|number|boolean|null} value
 */
sb.BlockView.prototype.updateArgValue = function(argName, value) {
	
	var socketEl = this.sockets_[argName];
	var inputEl = null;
	if(socketEl.tagName === 'INPUT') {
		inputEl = socketEl;
	}
	else {
		var socketChildren = socketEl.children;
		for(var i = 0; i < socketChildren.length; i += 1) {
			if(socketChildren[i].tagName === 'INPUT' || socketChildren[i].tagName === 'SELECT') {
				inputEl = socketChildren[i];
				break;
			}
		}
	}
	
	if(goog.isDefAndNotNull(inputEl)) {
		if(value === null) {
			value = '';
		}
		inputEl.value = value;
		inputEl.style.display = 'inline-block';
		if(inputEl.tagName === 'INPUT') {
			sb.BlockView.autoSizeText(inputEl);
		}
	}
	
}

sb.BlockView.prototype.updateArgOptions = function( argName ) {
	var socketEl = this.sockets_[argName];
	var selectEl = null;
	var i;
	
	var socketChildren = socketEl.children;
	for(i = 0; i < socketChildren.length; i += 1) {
		if(socketChildren[i].tagName === 'SELECT') {
			selectEl = socketChildren[i];
			break;
		}
	}
	
	var arg = this.block_.getArgument( argName );
	if(goog.isDefAndNotNull(selectEl)) {
		
		for(i = selectEl.options.length - 1; i >= 0; i -= 1) {
			selectEl.remove(i);
		}
		
		var options = (/**@type {sb.ListArgument}*/arg).getOptions();
		for(i = 0; i < options.length; i += 1) {
			var optionEl = document.createElement('option');
			
			optionEl.innerHTML = goog.string.htmlEscape(options[i].label);
			optionEl.value = options[i].value;
			
			goog.dom.appendChild( selectEl, optionEl );
		}
	}
}

sb.BlockView.prototype.updateSocketSize = function(argName) {
	
	var socketEl = this.sockets_[argName];
	var childView = this.childViews_[argName];
	if(goog.isDefAndNotNull(childView)) {
		goog.style.setHeight(socketEl, childView.getRecursiveHeight());
	}
}

sb.BlockView.prototype.computeHeight = function() {
	
	var height = goog.style.getSize(this.layoutParentEl_).height;
	goog.style.setHeight(this.element_, height);
}

sb.BlockView.prototype.computeWidth = function() {
	var maxLineWidth = 0;
	for(var i = 0; i < this.lines_.length; i += 1 ) {
		var lineWidth = 0;
		var trueLineWidth = 0;
		var lineEl = this.lines_[i];
		var hasNested = false;
		for(var j = 0; j < lineEl.children.length; j += 1) {
			var linePartEl = lineEl.children[j];
			
			var partWidth;
			// add 1 to width (IE seems to round widths down)
			partWidth = goog.style.getSize(linePartEl).width + 1;
			trueLineWidth += partWidth;
			lineWidth += partWidth;
		}
		if(lineWidth > maxLineWidth) {
			maxLineWidth = lineWidth;
		}
		if(hasNested) {
			goog.style.setWidth(lineEl, trueLineWidth);
		}
	}
	var blockWidth = maxLineWidth;
	var padBox = goog.style.getPaddingBox(this.layoutElement_);
	blockWidth += padBox.left;
	blockWidth += padBox.right;
	
	goog.style.setWidth(this.layoutElement_, blockWidth);
	goog.style.setWidth(this.element_, blockWidth);
	
	return blockWidth;
}

/**
 * @param {boolean} value
 */
sb.BlockView.prototype.setSelected = function(value) {
	this.selected_ = value;
	this.drawShape();
}

/**
 * @return {boolean}
 */
sb.BlockView.prototype.getSelected = function() {
	return this.selected_;
}

sb.BlockView.minWidth = 28;
sb.BlockView.minHeight = 18;

sb.BlockView.socketOffset = 10;
sb.BlockView.notchWidth = 9;
sb.BlockView.notchHeight = 11;
sb.BlockView.notchOffset = 3;

sb.BlockView.hNotchWidth = 12;
sb.BlockView.hNotchHeight = 5;
sb.BlockView.hNotchOffset = 8;

sb.BlockView.linePadding = 0.5;

sb.BlockView.labelPadding = 10;

sb.BlockView.lineHeight = 20;

sb.BlockView.styleNames = {};
sb.BlockView.styleNames.BLOCK = "sbBlock";
sb.BlockView.styleNames.BEFORE_CON_BLOCK = "sbBeforeConBlock";
sb.BlockView.styleNames.BLOCK_LINE = "sbBlockLine";
sb.BlockView.styleNames.LABEL = "sbLabel";
sb.BlockView.styleNames.ED_LABEL = "sbEditableLabel";
sb.BlockView.styleNames.ED_LABEL_HOVER = "sbEditableLabel:hover";
sb.BlockView.styleNames.ED_LABEL_FOCUS = "sbEditableLabel:focus";
sb.BlockView.styleNames.SOCKET = "sbSocket";
sb.BlockView.styleNames.INTERNAL = "sbInternal";
sb.BlockView.styleNames.INTERNAL_INPUT = "sbInternalInput";
sb.BlockView.styleNames.INTERNAL_SELECT = "sbInternal select";
sb.BlockView.styleNames.HILIGHT = "sbHilight";
sb.BlockView.styleNames.NESTED = "sbNested";
sb.BlockView.styleNames.EDGE = "sbEdge";
sb.BlockView.styleNames.AFTER_SOCKET = "sbAfterSocket";


sb.BlockView.installStyles = function() {
	var testBlockStyle = {border: "1px solid black"};
	var styleStr = "";
	
	var styles = {};
	styles[sb.BlockView.styleNames.BLOCK] = 
	{
		position: "relative",
		"vertical-align": "middle",
		display: "block",
		height: "auto",
		width: "auto",
		"min-width": sb.BlockView.minWidth + "px",
		"min-height": sb.BlockView.minHeight + "px",
		padding: "1px 0px 2px 10px",
		"text-align": "left"
	}

	styles[sb.BlockView.styleNames.BEFORE_CON_BLOCK] = {
		"padding-top": "6px"
	}

	styles[sb.BlockView.styleNames.BLOCK_LINE] = {
		padding: "1px 0px 1px 0px",
		margin: "0px 0px 0px 0px"
	}

	styles[sb.BlockView.styleNames.LABEL] = {
		"white-space": "nowrap",
		"vertical-align": "top",
		cursor: "default",
		display: "inline",
		margin: "2px 0px 2px 0px",
		padding: "1px 5px 0px 5px"
	}

	styles[sb.BlockView.styleNames.ED_LABEL] = {
		"vertical-align": "top",
		display: "inline-block",
		"background-color": "transparent",
		border: "none",
		width: "80px",
		margin: "-1px 0px 0px 0px",
		padding: "1px 1px 1px 1px",
		"font-size": "15px"
	}

	styles[sb.BlockView.styleNames.ED_LABEL_HOVER] = {
		border: "1px dashed #FFF",
		padding: "0px 0px 0px 0px"
	}
	
	styles[sb.BlockView.styleNames.ED_LABEL_FOCUS] = {
		"background-color": "#FFF",
		border: "1px dashed #999",
		padding: "0px 0px 0px 0px",
		"-moz-user-select": "text"
	}
	

	styles[sb.BlockView.styleNames.SOCKET] = {
		position: "relative",
		padding: "0px 8px 0px 8px",
		height: "auto"
	}

	styles[sb.BlockView.styleNames.INTERNAL] = {
		display: "inline-block",
		"vertical-align": "top",
		padding: "0px 2px 0px 0px",
		margin: "1px 0px 1px 0px"
	}

	styles[sb.BlockView.styleNames.INTERNAL_INPUT] = {
		"background-color": "transparent",
		border: "none",
		margin: "0px 0px 0px 11px",
		padding: "0px 1px 0px 2px",
		"min-width": "20px"
	}

	styles[sb.BlockView.styleNames.INTERNAL_SELECT] = {
		margin: "0px 0px 0px 6px"
	}

	styles[sb.BlockView.styleNames.HILIGHT] = {
		"box-shadow": "inset 0 0 2em gold"
	}

	styles[sb.BlockView.styleNames.NESTED] = {
		display: "inline-block",
		border: "0px",
		padding: "0px 0px 0px 0px",
		height: "auto",
		width: "26px",
		"min-height": sb.BlockView.minHeight + "px",
		margin: "1px 0px 2px 0px"
	}

	styles[sb.BlockView.styleNames.EDGE] = {
		display: "inline-block",
		border: "0px",
		padding: "0px 0px 0px 0px",
		height: "auto",
		width: (sb.BlockView.notchWidth + 1) + "px",
		"min-height": sb.BlockView.notchHeight + "px",
		margin: "-1px 0px 0px 0px",
		"vertical-align": "top"
	}

	styles[sb.BlockView.styleNames.AFTER_SOCKET] = {
		position: "relative",
		top: "-1px",
		left: "0px",
		"min-height": sb.BlockView.hNotchHeight + "px",
		width: "auto",
		height: "auto"
	}
	
	for(var className in styles) {
		styleStr += "." + className + " " + toStyleString(styles[className]) +'\n';
	}
	
	function toStyleString(obj) {
		var str = "{";
		for(var name in obj) {
			if(obj.hasOwnProperty(name)) {
				str += name + ": " + obj[name] + ";";
			}
		}
		str += "}";
		
		return str;
	}
	
	goog.style.installStyles(styleStr);
}

/**
 * Draws the borders and fill of the block.
 */
sb.BlockView.prototype.drawShape = function() {
	
	var notchWidth = sb.BlockView.notchWidth;
	var notchHeight = sb.BlockView.notchHeight;
	var notchOffset = sb.BlockView.notchOffset;
	
	var hnw = sb.BlockView.hNotchWidth;
	var hnh = sb.BlockView.hNotchHeight;
	var hno = sb.BlockView.hNotchOffset;

	var gw = goog.style.getSize(this.layoutElement_).width;
	var gh = goog.style.getSize(this.layoutElement_).height;
	if(this.hasAfterSocket()) {
		gh += notchHeight;
	}
	
	if(gh == 0 || gw == 0 || isNaN(gh) || isNaN(gw)) {
		return;
	}
	
	this.graphics_.clear();
	
	//this.graphics_.width = gw;
	//this.graphics_.height = gh;
	goog.dom.removeNode(this.canvas_);
	this.graphics_.setSize(gw, gh);

	goog.dom.insertSiblingBefore(this.canvas_, this.layoutParentEl_);
	
	// new drawing vars
	
	
	var blockColor = this.block_.getColor();
	if(goog.isDefAndNotNull(blockColor) && blockColor != "") {
		//TODO: move this - util class?
		// If the color is passed as a number, it may not have all 6 hex digits
		while(blockColor.length < 7) {
			blockColor = "#" + goog.string.padNumber(parseInt(blockColor.slice(1), 10), 8);
		}
	}
	else {
		blockColor = sb.BlockView.DEFAULT_COLOR;//<
	}
	blockColor = this.selected_ ? sb.Utils.lightenColor(blockColor, 0.8) : blockColor;
	
	var blockStrokeColor = "#333";
	var blockHilightColor = this.selected_ ? 
			sb.Utils.lightenColor(blockColor, 0.70) : sb.Utils.lightenColor(blockColor, 0.85);
	var blockShadowColor = this.selected_ ? 
			sb.Utils.darkenColor(blockColor, 1.5) : sb.Utils.darkenColor(blockColor, 1.2);
			
	var socketColor = "#FFF";
			
	/** corner radius */
	var cr = 2;
	
	var strokeWidth = 1.1;
	
	var blockStroke = new goog.graphics.Stroke(strokeWidth, blockStrokeColor);
	var blockFill = new goog.graphics.SolidFill(blockColor);
	
	/** line padding */
	var lp = strokeWidth / 2;
	
	/** width of highlight and shadow lines */
	var bevelWidth = 1;
	
	/** bevel line padding */
	var blp = strokeWidth / 2  + bevelWidth / 2;
	
	/** block border */
	var path = new goog.graphics.Path();
	
	/** hilight (top right bevel) */
	var hPath = new goog.graphics.Path();
	
	/** shadow (bottom left bevel) */
	var sPath = new goog.graphics.Path();
	
	
	var hStroke = new goog.graphics.Stroke(bevelWidth, blockHilightColor);
	var sStroke = new goog.graphics.Stroke(bevelWidth, blockShadowColor);
	
	var socketStroke = new goog.graphics.Stroke(strokeWidth, blockStrokeColor);
	var socketFill = new goog.graphics.SolidFill(socketColor);
	// end new drawing vars
	
	
	var blockPadBox = goog.style.getPaddingBox(this.layoutElement_);
			
	var currentHeight = 0;
	var currentWidth = 0;
	
	// clear the canvas & resize if necessary
	
	
	sb.SocketShapes.setBlockVars(path, sPath, hPath, lp, blp, notchWidth, notchHeight);
	
	// draw nub
	var returnNubWidth = notchWidth;
	var returnType = this.block_.getReturnType();
	var connections = this.block_.getConnections();
	
	var drawFunc = sb.SocketShapes.map[returnType];
	if(goog.isDefAndNotNull( drawFunc )) {
		
		currentWidth = notchWidth + lp;
		currentHeight = notchOffset + notchHeight + lp;
		path.moveTo(currentWidth, currentHeight);
		
		drawFunc(path, false, true);//<
		
		drawLeftLineTo(currentWidth, cr + lp);
		drawTopLeftCorner();
		//drawTopLineTo(notchWidth + lp, lp);
	}
	else {
		if(connections !== undefined && goog.array.indexOf(connections, sb.SocketType.BEFORE) !== -1) {
			// draw square notch on top of block
			path.moveTo(lp, hno + hnh + lp);
			drawLeftLineTo(lp, lp + cr);
			drawTopLeftCorner();
			drawTopLineTo(hno - cr, lp);
			drawTopRightCorner();
			drawRightLineTo(hno, lp + hnh - cr);
			drawBtmLeftCorner(true);
			drawTopLineTo(hno + hnw - cr, lp + hnh);
			drawBtmRightCorner(true);
			drawLeftLineTo(hno + hnw, lp + cr);
			drawTopLeftCorner();
		}
		else {
			// no notch/nub
			path.moveTo(lp, lp + notchOffset + notchHeight);
			drawLeftLineTo(lp, lp + cr);
			drawTopLeftCorner();
		}
		
		returnNubWidth = 0;
	}
			
	
	currentHeight = lp;
	currentWidth = gw - lp;
	
	drawTopLineTo(currentWidth - cr, currentHeight);
	// TOP RIGHT CORNER
	drawTopRightCorner();
	if(blockPadBox.top) {
		currentHeight += blockPadBox.top;
		drawRightLineTo(currentWidth, currentHeight);
	}
	
	// loop through each child of this.layoutElement_
	var i;
	for(i = 0; i < this.lines_.length; i += 1) {
		var lineEl = this.lines_[i];
		
		var linePartEls = lineEl.children;
		var lastEl = linePartEls[linePartEls.length - 1];
		
		if( !goog.isDefAndNotNull( lastEl ) ) {
			continue;
		}
		
		if( goog.dom.classes.has( lastEl, "sbNested" )) {
			// find the argument that this socket represents
			var arg;
			var argName;
			for(var socketName in this.sockets_) {
				if(this.sockets_[socketName] == lastEl) {
					argName = socketName;
					break;
				}
			}
			if(goog.isDefAndNotNull(argName)) {
				arg = this.block_.getArgument(argName);
			}
			
			var childPos = goog.style.getRelativePosition(lastEl, this.layoutElement_);
			
			currentHeight = childPos.y + lp;
			drawRightLineTo(currentWidth, currentHeight - cr);
			drawBtmRightCorner();
			
			var argNotchWidth = notchWidth;
			
			var dataType = arg.getDataType();
			var argValue = arg.getValue();
			if(dataType === sb.DataType.POLY && argValue instanceof sb.Block) {
				dataType = argValue.getReturnType();
			}
			drawFunc = sb.SocketShapes.map[dataType];
			
			if( goog.isDefAndNotNull(drawFunc) ) {
				
				currentWidth = childPos.x + lp + notchWidth;
				drawBtmLineTo(currentWidth + cr, currentHeight);
				drawTopLeftCorner(true);
				currentHeight += notchOffset;
				drawRightLineTo(currentWidth, currentHeight);
				
				drawFunc(path, true, true);
				
				currentHeight -= notchOffset;
			}
			else {
				// draw vertical square nub (connects command blocks)
				currentWidth = childPos.x - lp + hnw + hno;

				drawBtmLineTo(currentWidth + cr, currentHeight);
				drawTopLeftCorner(true);
				drawRightLineTo(currentWidth, currentHeight + hnh - cr);
				drawBtmRightCorner();
				drawBtmLineTo(currentWidth - hnw + 2 * lp + cr, currentHeight + hnh);
				drawBtmLeftCorner();
				drawLeftLineTo(currentWidth - hnw + 2 * lp, currentHeight + cr);
				drawTopRightCorner(true);
				
				currentWidth = childPos.x + lp;
				drawBtmLineTo(currentWidth + cr, currentHeight);
				drawTopLeftCorner(true);
				
				argNotchWidth = 0;
			}
			
			var argChildHeight;
			var argChild = this.childViews_[ arg.getName() ];
			if( goog.isDefAndNotNull(argChild) ) {
				argChildHeight = argChild.getRecursiveHeight();
			}
			else {
				argChildHeight = goog.style.getSize(lastEl).height;
			}
			
			currentHeight += argChildHeight - 2 * lp;
			drawRightLineTo(currentWidth, currentHeight - cr);
			drawBtmLeftCorner(true);
			currentWidth = gw - lp;
			drawTopLineTo(currentWidth - cr, currentHeight);
			drawTopRightCorner();
		}
		else if (goog.dom.classes.has(lastEl, 'sbEdge')) {
			var arg;
			var argName;
			for(var socketName in this.sockets_) {
				if(this.sockets_[socketName] == lastEl) {
					argName = socketName;
					break;
				}
			}
			if(goog.isDefAndNotNull(argName)) {
				arg = this.block_.getArgument(argName);
			}
			
			var childPos = goog.style.getRelativePosition(lastEl, this.layoutElement_);
			
			currentHeight = childPos.y + notchOffset + lp;
			drawRightLineTo(currentWidth, currentHeight);
			
			var argNotchWidth = notchWidth;
			
			var dataType = arg.getDataType();
			var argValue = arg.getValue();
			if(dataType === sb.DataType.POLY && argValue instanceof sb.Block) {
				dataType = argValue.getReturnType();
			}
			drawFunc = sb.SocketShapes.map[dataType];
			
			if( goog.isDefAndNotNull(drawFunc) ) {
				
				drawFunc(path, true, true);
				
				currentHeight -= notchOffset;
			}
		}
		
		
	}
	
	currentHeight = goog.style.getSize(this.layoutElement_).height - lp;
	drawRightLineTo(currentWidth, currentHeight - cr);
	// BTM RIGHT CORNER
	drawBtmRightCorner();
	
	// draw bottom socket
	if( this.hasAfterSocket() ) {
		
		this.socketPositions_[ sb.SocketType.AFTER ] = 
			new goog.math.Coordinate(0, currentHeight);
		
		drawBtmLineTo(hno + hnw + cr - lp, currentHeight);
		drawTopLeftCorner(true);
		
		currentHeight += hnh;
		drawRightLineTo(hno + hnw - lp, currentHeight - cr);
		drawBtmRightCorner();
		drawBtmLineTo(lp + hno + cr, currentHeight);
		drawBtmLeftCorner();
		
		drawLeftLineTo(lp + hno, currentHeight - hnh + cr);
		drawTopRightCorner(true);
		drawBtmLineTo(lp + returnNubWidth + cr, currentHeight - hnh);
	}
	else {
		drawBtmLineTo(lp + returnNubWidth + cr, currentHeight);
	}
	drawBtmLeftCorner();
	drawLeftLineTo(lp + returnNubWidth, notchOffset + notchHeight - lp);
	
	
	// internal sockets
	var socketPaths = [];
	for(var argName in this.sockets_) {
		var socketEl = this.sockets_[argName];
		arg = this.block_.getArgument(argName);
		if(goog.isDefAndNotNull(socketEl) && goog.dom.classes.has( socketEl, "sbInternal" )) {
			
			var socketPath = new goog.graphics.Path();
			
			// draw internal socket shape
			var inputEl;
			var socketChildren = socketEl.children;
			for(i = 0; i < socketChildren.length; i += 1) {
				if(socketChildren[i].tagName === "INPUT") {
					inputEl = socketChildren[i];
					break;
				}
			}
			// don't draw anything if the input element isn't visible
			if(!goog.isDefAndNotNull(inputEl) || goog.style.getStyle(inputEl, "display") === "none") {
				continue;
			}
			
			var inputSize = goog.style.getSize(inputEl);
			var inputMargin = goog.style.getMarginBox(inputEl);
			var inputPadding = goog.style.getPaddingBox(inputEl);
			var inputWidth = inputMargin.left + inputSize.width;
			
			var socketPos = this.socketPositions_[argName];
			
			currentWidth = socketPos.x + lp + inputPadding.left - 1;
			currentHeight = socketPos.y + lp;
			
			socketPath.moveTo(currentWidth + notchWidth, currentHeight);
			socketPath.lineTo(currentWidth + inputWidth - 1, currentHeight);
			socketPath.lineTo(currentWidth + inputWidth - 1, currentHeight + inputSize.height);
			socketPath.lineTo(currentWidth + notchWidth, currentHeight + inputSize.height);
			socketPath.lineTo(currentWidth + notchWidth, currentHeight + notchOffset + notchHeight);
			
			
			drawFunc = sb.SocketShapes.map[ arg.getDataType() ];
			if( !goog.isDefAndNotNull(drawFunc) ) {
				drawFunc = sb.SocketShapes.drawLine;
			}
			drawFunc(socketPath, false, false);
						
			socketPath.lineTo(currentWidth + notchWidth, currentHeight - lp);
			
			socketPaths.push(socketPath);
		}
	}
	

	this.graphics_.drawPath(path, null, blockFill);
	this.graphics_.drawPath(hPath, hStroke, null);
	this.graphics_.drawPath(sPath, sStroke, null);
	this.graphics_.drawPath(path, blockStroke, null);
	
	for(i = 0; i < socketPaths.length; i += 1) {
		var socketPath = socketPaths[i];
		this.graphics_.drawPath(socketPath, socketStroke, socketFill);
	}
	
	// drawing functions
	function drawTopLineTo(x, y) {
		var cp = path.getCurrentPoint();
		hPath.moveTo(cp[0], cp[1] + blp);
		path.lineTo(x, y);
		hPath.lineTo(x, y + blp);
	}
	
	function drawBtmLineTo(x, y) {
		var cp = path.getCurrentPoint();
		sPath.moveTo(cp[0], cp[1] - blp);
		path.lineTo(x, y);
		sPath.lineTo(x, y - blp);
	}
	
	function drawLeftLineTo(x, y) {
		var cp = path.getCurrentPoint();
		sPath.moveTo(cp[0] + blp, cp[1]);
		path.lineTo(x, y);
		sPath.lineTo(x + blp, y);
	}
	
	function drawRightLineTo(x, y) {
		var cp = path.getCurrentPoint();
		hPath.moveTo(cp[0] - blp, cp[1]);
		path.lineTo(x, y);
		hPath.lineTo(x - blp, y);
	}
	
	/**
	 * @param {boolean=} innerCorner
	 */
	function drawTopLeftCorner(innerCorner) {
		var cp = path.getCurrentPoint();
		if (innerCorner) {
			sPath.moveTo(cp[0], cp[1] - blp);
			path.arcTo(cr, cr, 270, -90);
			sPath.arcTo(cr + blp, cr + blp, 270, -45);
			
			cp = path.getCurrentPoint();
			hPath.moveTo(cp[0] - blp, cp[1]);
			hPath.arcTo(cr + blp, cr + blp, 180, 45);
		}
		else {
			sPath.moveTo(cp[0]+ blp, cp[1]);
			path.arcTo(cr, cr, 180, 90);
			sPath.arcTo(cr - blp, cr - blp, 180, 45);
			
			cp = path.getCurrentPoint();
			hPath.moveTo(cp[0], cp[1] + blp);
			hPath.arcTo(cr - blp, cr - blp, 270, -45);
		}
	}
	
	/**
	 * @param {boolean=} innerCorner
	 */
	function drawTopRightCorner(innerCorner) {
		var cp = path.getCurrentPoint();
		if (innerCorner) {
			sPath.moveTo(cp[0] + blp, cp[1]);
			path.arcTo(cr, cr, 0, -90);
			sPath.arcTo(cr + blp, cr + blp, 0, -90);
		}
		else {
			hPath.moveTo(cp[0], cp[1] + blp);
			path.arcTo(cr, cr, 270, 90);
			hPath.arcTo(cr - blp, cr - blp, 270, 90);
		}
	}
	
	/**
	 * @param {boolean=} innerCorner
	 */
	function drawBtmLeftCorner(innerCorner) {
		var cp = path.getCurrentPoint();
		
		if (innerCorner) {
			hPath.moveTo(cp[0] - blp, cp[1]);
			path.arcTo(cr, cr, 180, -90);
			hPath.arcTo(cr + blp, cr + blp, 180, -90);
		}
		else {
			sPath.moveTo(cp[0], cp[1] - blp);
			path.arcTo(cr, cr, 90, 90);
			sPath.arcTo(cr - blp, cr - blp, 90, 90);
		}
	}
	
	/**
	 * @param {boolean=} innerCorner
	 */
	function drawBtmRightCorner(innerCorner) {
		var cp = path.getCurrentPoint();
		
		if (innerCorner) {
			hPath.moveTo(cp[0], cp[1] + blp);
			path.arcTo(cr, cr, 90, -90);
			hPath.arcTo(cr + blp, cr + blp, 90, -45);
			
			cp = path.getCurrentPoint();
			sPath.moveTo(cp[0] + blp, cp[1]);
			sPath.arcTo(cr + blp, cr + blp, 0, 45);
		}
		else {
			hPath.moveTo(cp[0] - blp, cp[1]);
			
			path.arcTo(cr, cr, 0, 90);
			hPath.arcTo(cr - blp, cr - blp, 0, 45);
			
			cp = path.getCurrentPoint();
			sPath.moveTo(cp[0], cp[1] - blp);
			sPath.arcTo(cr - blp, cr - blp, 90, -45);
		}
	}
	// end drawing functions
	
}

/**
 * Highlights the specified socket.
 * 
 * @param {string} socketName
 * @param {boolean} value
 */
sb.BlockView.prototype.setSocketHilight = function( socketName, value ) {
	
	var socket = this.sockets_[socketName];
	if(socketName === sb.SocketType.AFTER) {
		socket = this.afterSocketEl_;
	}
	if(value) {
		goog.dom.classes.add( socket, "sbHilight" );
	}
	else {
		goog.dom.classes.remove( socket, "sbHilight" );
	}
}

/**
 * Returns a rectangle defining the size and position of the specified socket.
 * 
 * @param {string} socketName
 * 
 * @return {goog.math.Rect}
 */
sb.BlockView.prototype.getSocketRect = function( socketName ) {
	
	var socket, pos, size, rect;
	
	if(socketName === sb.SocketType.BEFORE) {
		pos = new goog.math.Coordinate(0, 0);
	}
	else {
		if(socketName === sb.SocketType.AFTER) {
			socket = this.afterSocketEl_;
		}
		else {
			socket = this.sockets_[socketName];
		}
		
		pos = goog.style.getRelativePosition( socket, this.element_ );
		if(goog.dom.classes.has(socket, 'sbEdge')) {
			pos.y += 3;
			//pos.x += 1;
		}
	}
	
	if(socketName === sb.SocketType.BEFORE ||socketName === sb.SocketType.AFTER || this.block_.getArgument(socketName).getDataType() === sb.DataType.COMMAND ) {
		size = new goog.math.Size(
				goog.style.getSize( this.element_ ).width - pos.x,
				sb.BlockView.hNotchHeight);
	}
	else {
		size = goog.style.getSize( socket );
	}
	rect = new goog.math.Rect( pos.x, pos.y, size.width + 1, size.height + 1 );
	
	return rect;
}

/**
 * @return {boolean}
 */
sb.BlockView.prototype.hasAfterSocket = function() {
	var connections = this.block_.getConnections();
	var hasAfter = (connections !== undefined && goog.array.indexOf(connections, sb.SocketType.AFTER) !== -1); 
	return hasAfter;
}

/**
 * @param {Event} event
 */
sb.BlockView.prototype.onInputChange_ = function(event) {
	
	var inputEl = /**@type {Element}*/event.target;
	
	if(inputEl.tagName !== 'SELECT') {
		sb.BlockView.autoSizeText(inputEl);
	}
	
	var blockArgs = this.block_.getArguments();
	for(var argName in this.sockets_) {
		var sock = this.sockets_[argName];
		if(goog.dom.contains(sock, inputEl)) {
			for(var i=0; i < blockArgs.length; i += 1) {
				if(blockArgs[i].getName() === argName) {
					
					this.updateSocketSize(argName);
					this.computeWidth();
					this.drawShape();
					
					var newEvent = new sb.BlockViewEvent(sb.BlockViewEvent.INPUT_CHANGED);
					newEvent.argName = argName;
					newEvent.value = inputEl.value;
					this.dispatchEvent(newEvent);
					
					break;
				}
			}
			break;
		}
	}
	
}

sb.BlockView.autoSizeText = function(element) {
	var widthDiv = document.createElement('div');
	widthDiv.style.width = "auto";
	widthDiv.style.display = "inline-block";
	widthDiv.style.visibility = "hidden";
	
	// TODO: use element font size (element would need to already be appended to block element)
	if(goog.dom.classes.has( element, "sbEditableLabel" )) {
		goog.style.setStyle(widthDiv, "font-size", "15px");
	}
	else {
		goog.style.setStyle(widthDiv, "font-size", "13px");
	}
	
	
	goog.style.setStyle(widthDiv, "font-family", "Arial");
	goog.style.setStyle(widthDiv, "padding-left", goog.style.getComputedStyle(element, "padding-left"));
	goog.style.setStyle(widthDiv, "padding-right", goog.style.getComputedStyle(element, "padding-right"));
	widthDiv.innerHTML = element.value;
	document.body.appendChild(widthDiv);
	goog.style.setWidth( element, goog.style.getSize(widthDiv).width + 4 );
	
	goog.dom.removeNode(widthDiv);
}
/**
 * @param {Element} blockEl The BlockView element to check.
 * @param {goog.math.Coordinate} point The coordinate relative to the block element.
 * 
 * @return {boolean} If the coordinate is within the drawn block.
 */
sb.BlockView.containsPoint = function( blockEl, point ) {
	
	var pointInBlock;
	
	
	var canvas = goog.dom.getElementsByTagNameAndClass( "canvas", null, blockEl )[0];
	if(goog.isDefAndNotNull(canvas)) {
		var ctx = canvas.getContext( '2d' );
		
		// rgba array
		var pixel = ctx.getImageData( point.x, point.y, 1, 1 ).data;
		
		// only consider the point to be part of the block if the pixel alpha value 
		// is greater than 50%
		pointInBlock = pixel[3] > 128;
	}
	else {
		pointInBlock = true;
		var socketEls = goog.dom.findNodes( blockEl, 
				function(el) {
					var match = goog.dom.classes.has(el, "sbNested");
					return match;
				});
		
		for(var i = 0; i < socketEls.length; i += 1) {
			var el = /**@type{Element}*/(socketEls[i]);
			var socketPos = goog.style.getRelativePosition( el, blockEl );
			var socketSize = goog.style.getSize( el );
			var socketRect = new goog.math.Rect( socketPos.x, socketPos.y, socketSize.width, socketSize.height);
			if(socketRect.contains(point)){
				pointInBlock = false;
			}
		}
	}
	
	return pointInBlock;
}

/**
 * @param {Element} element The BlockView element to clone.
 * 
 * @return {Element}
 */
sb.BlockView.cloneBlockElement = function( element ) {

	var cloneEl = /** @type {!Element} */ (element.cloneNode( true )); // doesn't work in mozilla? may need to encapsulate element creation
	
	var cloneID = element.id + "_clone";
	cloneEl.id = cloneID;
	
	var origCanvas = goog.dom.getElementsByTagNameAndClass( "canvas", null, /** @type {!Element} */(element) )[0];
	var cloneCanvas = goog.dom.getElementsByTagNameAndClass( "canvas", null, cloneEl )[0];
	//var cloneCanvas = origCanvas.cloneNode();
	//goog.dom.appendChild( cloneEl, cloneCanvas );
	if(goog.isDefAndNotNull(cloneCanvas)) {
		var cloneCtx = cloneCanvas.getContext("2d");
		cloneCtx.drawImage(origCanvas, 0, 0);
	}
	
	return cloneEl;
}