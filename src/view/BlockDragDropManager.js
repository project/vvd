goog.provide('sb.BlockDragDropManager');
goog.provide('sb.BlockDragEvent');

goog.require('sb.InputManager');
goog.require('sb.Logger');

goog.require('goog.dom');
goog.require('goog.events.BrowserEvent');
goog.require('goog.style');

// TODO: should BDDMgr use ViewManager to get BlockID/PageID/ArgID?  
// (instead of having ScriptBlocks/Controller do the work).
// possibly a second controller to handle interaction between BDDMgr and ViewMgr?

/**
 * @constructor
 * @extends {goog.events.EventTarget}
 */
sb.BlockDragDropManager = function( ) {
	
	this.workspaceEl_ = null;
	
	/**
	 * @type {?Element}
	 * @private
	 */
	this.dragEl_ = null;
	
	/**
	 * The position of the dragged element relative to the mouse.
	 * 
	 * @type {!goog.math.Coordinate}
	 * @private
	 */
	this.dragOffset_ = new goog.math.Coordinate(0, 0);
	
	this.dragging_ = false;
	this.factoryDrag_ = false;
	
	this.inputMgr_ = sb.InputManager.getInstance();
}
goog.inherits(sb.BlockDragDropManager, goog.events.EventTarget);

sb.BlockDragDropManager.DRAG_TOLERANCE_SQ = 5 * 5;

/**
 * @return {boolean}
 */
sb.BlockDragDropManager.prototype.isDragging = function() {
	
	return this.dragging_;
}

/**
 * @param {Element} workspaceElement
 */ 
sb.BlockDragDropManager.prototype.setWorkspaceElement = function( workspaceElement ) {
	this.workspaceEl_ = workspaceElement;
}

/**
 * @return {?Element}
 */
sb.BlockDragDropManager.prototype.getCurrentDragElement = function() {
	return this.dragEl_;
}

/**
 * @param {Element} element
 * @param {boolean=} isFactory
 */
sb.BlockDragDropManager.prototype.addBlockListeners = function( element, isFactory ) {
	
	// for blocks in a drawer
	var handler;
	if(isFactory) {
		handler = this.onFactoryMouseDown_;
	}
	else {
		handler = this.onMouseDown_;
	}
	
	this.inputMgr_.convertTouchEvents( element );
	
	if( !goog.events.hasListener( element, goog.events.EventType.MOUSEDOWN ) ) {
		goog.events.listen( element, goog.events.EventType.MOUSEDOWN, handler, false, this);
	}
	
}

/**
 * @param {Element} element
 * @param {boolean=} isFactory
 */
sb.BlockDragDropManager.prototype.removeBlockListeners = function( element, isFactory ) {
	
	// for blocks in a drawer
	var handler;
	if(isFactory) {
		handler = this.onFactoryMouseDown_;
	}
	else {
		handler = this.onMouseDown_;
	}
	
	goog.events.unlisten( element, goog.events.EventType.MOUSEDOWN, handler, false, this);
	
}

/**
 * @param {Element} element
 */
sb.BlockDragDropManager.prototype.addPageListeners = function( element ) {
	
	// TODO: add listeners for drag-selecting a group of blocks
	
}

/**
 * @param {goog.events.BrowserEvent} event
 */
sb.BlockDragDropManager.prototype.onMouseDown_ = function( event ) {
	
	if( this.dragging_ || !(event.currentTarget instanceof Element)) {
		return;
	}
	
	this.dragEl_ = event.currentTarget;
	
	var blockPt = goog.style.getRelativePosition( event, this.dragEl_ );
	if( !sb.BlockView.containsPoint( this.dragEl_, blockPt ) ) {
		return;
	}
	
	this.dragOffset_ =  goog.math.Coordinate.difference(
			goog.style.getClientPosition(event),
			goog.style.getClientPosition(this.dragEl_)
	);
	
	
	goog.events.listen( this.workspaceEl_, goog.events.EventType.MOUSEMOVE, this.onMouseMove_, false, this );
	goog.events.listen( this.workspaceEl_, goog.events.EventType.MOUSEUP, this.onMouseUp_, true, this );
	
	event.preventDefault();
	event.stopPropagation();
	
}

/**
 * Creates a clone of the block to be dragged onto a page.
 * 
 * @param {goog.events.BrowserEvent} event
 */
sb.BlockDragDropManager.prototype.onFactoryMouseDown_ = function( event ) {
	
	if( this.dragging_ || !(event.currentTarget instanceof Element) ) {
		return;
	}
	
	this.factoryDrag_ = true;
	this.factoryBlock_ = sb.ObjectManager.getInstance().getObject(event.currentTarget.id);
	
	
	var origEl = /** @type {Element} */event.currentTarget;
	this.dragOffset_ = goog.math.Coordinate.difference(
			goog.style.getClientPosition(event),
			goog.style.getClientPosition(origEl)
	);
	
	goog.events.listen( this.workspaceEl_, goog.events.EventType.MOUSEMOVE, this.onMouseMove_, false, this );
	goog.events.listen( this.workspaceEl_, goog.events.EventType.MOUSEUP, this.onMouseUp_, true, this );
	
	event.preventDefault();
	event.stopPropagation();
}

/**
 * @param {goog.events.BrowserEvent} event
 */
sb.BlockDragDropManager.prototype.onMouseMove_ = function( event ) {
	
	var dX = this.dragOffset_.x - event.offsetX;
	var dY = this.dragOffset_.y - event.offsetY;
	
	var sqDist = dX * dX + dY * dY;
	
	// if the distance the mouse has moved > drag tolerance
	if( sqDist > sb.BlockDragDropManager.DRAG_TOLERANCE_SQ ) {
		
		this.dragging_ = true;
		
		var workspacePos = null;
		var blockView = null;
		
		if(this.factoryDrag_) {
			var vm = sb.ViewManager.getInstance();
			
			var origEl = vm.getBlockView(this.factoryBlock_.getID()).getElement();
			
			var newBlock = this.factoryBlock_.clone( true, true );
			blockView = vm.getBlockView((/**@type {sb.Block} */newBlock).getID());
			this.dragEl_ = blockView.getElement();
			
			this.dragEl_.style.margin = "0px";
			workspacePos = goog.style.getRelativePosition(origEl, this.workspaceEl_);
		}
		else {
			workspacePos = goog.style.getRelativePosition(this.dragEl_, this.workspaceEl_);
			blockView = sb.ViewManager.getInstance().getBlockView(this.dragEl_.id);
		}

		this.dragEl_.style.position = "absolute";
		goog.style.setPosition( this.dragEl_, workspacePos );
		goog.dom.appendChild( this.workspaceEl_, this.dragEl_ );
		
		
		goog.events.unlisten( this.workspaceEl_, goog.events.EventType.MOUSEMOVE, this.onMouseMove_, false, this );
		
		// update the position of the element when the mouse moves
		goog.events.listen( this.workspaceEl_, goog.events.EventType.MOUSEMOVE, this.onMouseMoveDrag_, false, this );
		
		var dragEvent = new sb.BlockDragEvent( sb.BlockDragEvent.DRAG_START, this.dragEl_.id );
		this.dispatchEvent(dragEvent);
	}
	
	event.preventDefault();
};

/**
 * @param {goog.events.BrowserEvent} event
 */
sb.BlockDragDropManager.prototype.onMouseMoveDrag_ = function( event ) {
	
	var tarEl = /** @type {!Element} */(event.target);
	
	var evtTargetWSOffset = goog.style.getRelativePosition( tarEl, this.workspaceEl_ );
	var evtTargetBlockOffset = goog.style.getRelativePosition( tarEl, this.dragEl_ );
	
	
	var wsPos = goog.style.getClientPosition(this.workspaceEl_);
	var wsMousePos = /** @type {!goog.math.Coordinate} */(goog.math.Coordinate.difference( 
			new goog.math.Coordinate(event.clientX, event.clientY), goog.style.getClientPosition(this.workspaceEl_)));
		
	// move the dragged block element to the mouse position
	var newDragPos = goog.math.Coordinate.sum(wsMousePos, evtTargetBlockOffset);
	goog.style.setPosition( this.dragEl_, goog.math.Coordinate.difference(wsMousePos, this.dragOffset_) );
	
	var dragEvent = new sb.BlockDragEvent( sb.BlockDragEvent.DRAG_MOVE, this.dragEl_.id );
	dragEvent.position = goog.style.getPosition( this.dragEl_ );
	
	this.dispatchEvent( dragEvent );
	
	event.preventDefault();
}

/**
 * @param {goog.events.BrowserEvent} event
 */
sb.BlockDragDropManager.prototype.onMouseUp_ = function( event ) {
	
	goog.events.unlisten( this.workspaceEl_, goog.events.EventType.MOUSEUP, this.onMouseUp_, true, this );
	goog.events.unlisten( this.workspaceEl_, goog.events.EventType.MOUSEMOVE, this.onMouseMove_, false, this );
	
	goog.events.unlisten( this.workspaceEl_, goog.events.EventType.MOUSEMOVE, this.onMouseMoveDrag_, false, this );
	
	event.preventDefault();
	
	if(this.dragging_) {
		var dragEvent = new sb.BlockDragEvent( sb.BlockDragEvent.DRAG_END, this.dragEl_.id );
		dragEvent.position = goog.style.getRelativePosition( this.dragEl_, this.workspaceEl_ );
	}
	

	this.factoryDrag_ = false;
	
	this.dragEl_ = null;
	this.dragging_ = false;
	
	if(goog.isDefAndNotNull( dragEvent )) {
		this.dispatchEvent( dragEvent );
	}
}

/**
 * @param {string} type
 * @param {string} blockID
 * 
 * @constructor
 */
sb.BlockDragEvent = function( type, blockID ) {
	
	this.type = type;
	this.blockID = blockID;
	
	/** @type {goog.math.Coordinate} */
	this.position;
}

sb.BlockDragEvent.DRAG_START = "dragStart";
sb.BlockDragEvent.DRAG_MOVE = "dragMove";
sb.BlockDragEvent.DRAG_END = "dragEnd";

