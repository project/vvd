goog.provide('sb.SocketShapes');

sb.SocketShapes = {};

/** @typedef { function(goog.graphics.Path, boolean, boolean) } */
sb.SocketShapes.drawSocketFunction;

/**
 * @type {Object.<sb.SocketShapes.drawSocketFunction>}
 */
sb.SocketShapes.map;

sb.SocketShapes.initDefaultMap = function() {
	sb.SocketShapes.map = {};
	
	sb.SocketShapes.map[sb.DataType.NUMBER] = sb.SocketShapes.drawTriangle;
	sb.SocketShapes.map[sb.DataType.STRING] = sb.SocketShapes.drawRectangle;
	sb.SocketShapes.map[sb.DataType.BOOLEAN] = sb.SocketShapes.drawCircle;
	sb.SocketShapes.map[sb.DataType.POLY] = sb.SocketShapes.drawStar;
}

/**
 * @param {string} dataType
 * 
 * @return {sb.SocketShapes.drawSocketFunction}
 */
sb.SocketShapes.getShapeFunction = function( dataType ) {
	return sb.SocketShapes.map[dataType];
}

/**
 * @param {goog.graphics.Path} path 	The path to draw
 * @param {boolean} down				If the socket should be drawn top to bottom. 
 * @param {boolean} bevel				If the bevel lines should be drawn.
 */
sb.SocketShapes.drawLine = function(path, down, bevel) {
	var bv = sb.SocketShapes.blockVars
	var cp = path.getCurrentPoint();
	var x = cp[0], y = cp[1];
	
	if(down) {
		path.lineTo(x, y - bv.notchHeight);
	}
	else {
		path.lineTo(x, y + bv.notchHeight);
	}
}

/**
 * @param {goog.graphics.Path} path 	The path to draw
 * @param {boolean} down				If the socket should be drawn top to bottom. 
 * @param {boolean} bevel				If the bevel lines should be drawn.
 */
sb.SocketShapes.drawTriangle = function(path, down, bevel) {
	var bv = sb.SocketShapes.blockVars
	var cp = path.getCurrentPoint();
	var x = cp[0], y = cp[1];
	if(down) {
		path.lineTo(x - bv.notchWidth + bv.lp, y + bv.notchHeight / 2);
		path.lineTo(x, y + bv.notchHeight);
		if(bevel) {
			bv.hp.moveTo(x - bv.notchWidth - bv.blp, y + bv.notchHeight / 2 + bv.blp);
			bv.hp.lineTo(x - bv.blp, y + bv.notchHeight + bv.blp);
		}
	}
	else {
		path.lineTo(x - bv.notchWidth + bv.lp, y - bv.notchHeight / 2);
		path.lineTo(x, y - bv.notchHeight);
		if(bevel) {
			bv.sp.moveTo(x + bv.blp, y);
			bv.sp.lineTo(x + bv.blp, y - bv.lp);
			bv.sp.lineTo(x - bv.notchWidth + 2 * bv.blp, y - bv.notchHeight / 2);
		}
	}
}

/**
 * @param {goog.graphics.Path} path		The path for the border of the block.
 * @param {goog.graphics.Path} sPath	The path for the bevel shadow.
 * @param {goog.graphics.Path} hPath		The path for the bevel highlight.
 * @param {number} lp					The line padding.
 * @param {number} blp					The bevel line padding.
 * @param {number} notchWidth			
 * @param {number} notchHeight
 */
sb.SocketShapes.setBlockVars = function(path, sPath, hPath, lp, blp, notchWidth, notchHeight) {
	sb.SocketShapes.blockVars = {p:path, sp:sPath, hp:hPath, lp:lp, blp:blp, notchWidth: notchWidth, notchHeight:notchHeight};
}

/**
 * @param {goog.graphics.Path} path 	The path to draw
 * @param {boolean} down				If the socket should be drawn top to bottom. 
 * @param {boolean} bevel				If the bevel lines should be drawn.
 */
sb.SocketShapes.drawCircle = function(path, down, bevel) {
	var bv = sb.SocketShapes.blockVars
	var cp = path.getCurrentPoint();
	var rad = bv.notchHeight/2;
	if(down) {
		path.arcTo(rad, rad, 270, -180);//(x, y - bv.notchHeight * 0.5, bv.notchHeight * 0.5, Math.PI * 0.5, Math.PI * 1.5, false);
		if(bevel) {
			bv.hp.moveTo(cp[0] - rad - bv.blp, cp[1] + rad + bv.lp);
			bv.hp.arcTo(rad + bv.blp, rad + bv.blp, 180, -80);
		}
	}
	else {
		path.arcTo(rad, rad, 90, 180);
		if(bevel) {
			bv.sp.moveTo(cp[0] + bv.blp, cp[1]);
			bv.sp.lineTo(cp[0], cp[1] - bv.blp)
			bv.sp.arcTo(rad - bv.blp, rad - bv.blp, 90, 90);
		}
	}
}

/**
 * @param {goog.graphics.Path} path 	The path to draw
 * @param {boolean} down				If the socket should be drawn top to bottom. 
 * @param {boolean} bevel				If the bevel lines should be drawn.
 */
sb.SocketShapes.drawRectangle = function(path, down, bevel) {
	var bv = sb.SocketShapes.blockVars
	var cp = path.getCurrentPoint();
	var x = cp[0], y = cp[1];
	if(down) {
		path.lineTo(x - bv.notchWidth, y);
		path.lineTo(x - bv.notchWidth, y + bv.notchHeight);
		path.lineTo(x, y + bv.notchHeight);
		
		if(bevel) {
			bv.sp.moveTo(x - bv.blp, y - bv.blp);
			bv.sp.lineTo(x - bv.notchWidth - bv.blp, y - bv.blp);
			
			bv.hp.moveTo(x - bv.notchWidth - bv.blp, y);
			bv.hp.lineTo(x - bv.notchWidth - bv.blp, y + bv.notchHeight + bv.blp);
			bv.hp.lineTo(x - bv.blp, y + bv.notchHeight + bv.blp);
		}
	}
	else {
		path.lineTo(x - bv.notchWidth, y);
		path.lineTo(x - bv.notchWidth, y - bv.notchHeight);
		path.lineTo(x, y - bv.notchHeight);
		
		if(bevel) {
			bv.sp.moveTo(x + bv.blp, y + bv.blp);
			bv.sp.lineTo(x + bv.blp, y - bv.blp);
			bv.sp.lineTo(x - bv.notchWidth + bv.blp, y - bv.blp);
			bv.sp.lineTo(x - bv.notchWidth + bv.blp, y - bv.notchHeight + bv.blp);
			
			bv.hp.moveTo(x - bv.notchWidth + bv.blp, y - bv.notchHeight + bv.blp);
			bv.hp.lineTo(x, y - bv.notchHeight + bv.blp);
		}
		
	}
}

/**
 * @param {goog.graphics.Path} path 	The path to draw
 * @param {boolean} down				If the socket should be drawn top to bottom. 
 * @param {boolean} bevel				If the bevel lines should be drawn.
 */
sb.SocketShapes.drawStar = function(path, down, bevel) {
	
	var bv = sb.SocketShapes.blockVars
	var cp = path.getCurrentPoint();
	var x = cp[0], y = cp[1];
	
	if(down) {
		path.lineTo(x - bv.notchWidth * 0.5, y - bv.notchHeight * 0.125);
		path.lineTo(x - bv.notchWidth * 0.2, y + bv.notchHeight * 0.25);
		path.lineTo(x - bv.notchWidth * 0.9, y + bv.notchHeight * 0.5);
		path.lineTo(x - bv.notchWidth * 0.2, y + bv.notchHeight * 0.75);
		path.lineTo(x - bv.notchWidth * 0.5, y + bv.notchHeight * 1.125);
		path.lineTo(x, y + bv.notchHeight);
	}
	else {
		path.lineTo(x - bv.notchWidth * 0.5, y + bv.notchHeight * 0.125);
		path.lineTo(x - bv.notchWidth * 0.2, y - bv.notchHeight * 0.25);
		path.lineTo(x - bv.notchWidth * 0.9, y - bv.notchHeight * 0.5);
		path.lineTo(x - bv.notchWidth * 0.2, y - bv.notchHeight * 0.75);
		path.lineTo(x - bv.notchWidth * 0.5, y - bv.notchHeight * 1.125);
		path.lineTo(x, y - bv.notchHeight);
	}
}

/**
 * @param {goog.graphics.Path} path 	The path to draw
 * @param {boolean} down				If the socket should be drawn top to bottom. 
 * @param {boolean} bevel				If the bevel lines should be drawn.
 */
sb.SocketShapes.drawTrap = function(path, down, bevel) {
	var bv = sb.SocketShapes.blockVars;
	var cp = path.getCurrentPoint();
	var x = cp[0], y = cp[1];
	
	if(down) {
		path.lineTo(x - bv.notchWidth * 0.6, y + bv.notchHeight * 0.25);
		path.lineTo(x - bv.notchWidth * 0.6, y + bv.notchHeight * 0.75);
		path.lineTo(x, y + bv.notchHeight);
	}
	else {
		path.lineTo(x - bv.notchWidth * 0.6, y - bv.notchHeight * 0.25);
		path.lineTo(x - bv.notchWidth * 0.6, y - bv.notchHeight * 0.75);
		path.lineTo(x, y - bv.notchHeight);
	}
}