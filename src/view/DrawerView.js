goog.provide('sb.DrawerView');


goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.style');
goog.require('goog.object');

/**
 * @param {sb.Drawer} drawer
 * @param {?Element=} element
 * @param {string=} name
 * 
 * @constructor
 * @extends {sb.PageView}
 */
sb.DrawerView = function( drawer, element, name ) {
	
	sb.PageView.call(this, drawer, element, name);
	
	this.drawer_ = drawer;
	
	if(element === undefined) {
		element = null;
	}
	
	this.padding_ = 15;
	
	
	this.element_.className = "sbDrawer";
}
goog.inherits(sb.DrawerView, sb.PageView);

// TODO: layout - blocks in a drawer should not overlap
/**
 * @inheritDoc
 */
sb.DrawerView.prototype.addBlockView = function( blockView ) {
	
	if( goog.isNull( this.element_ ) ) {
		return;
	}
	
	var blockEl = blockView.getElement();
	goog.dom.append( this.element_, blockEl );
	
	blockEl.style.position = "relative";
	blockEl.style.margin = this.padding_ + "px 0px " + this.padding_ + "px 0px";
	
	blockView.setParent(this);
	
}
