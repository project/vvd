goog.provide('sb.InputManager');

goog.require('goog.events');

/**
 * @extends {goog.events.EventTarget}
 * @constructor
 */
sb.InputManager = function() {
	
	/**
	 * @type {boolean}
	 * @private
	 */
	this.supportsTouch_;
	
	try {  
		document.createEvent("TouchEvent");  
		this.supportsTouch_ = true;  
	} catch (e) {  
		this.supportsTouch_ = false;  
	}
	
}
goog.inherits(sb.InputManager, goog.events.EventTarget);

goog.addSingletonGetter(sb.InputManager);

sb.InputManager.prototype.convertTouchEvents = function(element) {
	if(!this.supportsTouch_) {
		return;
	}
	
	element.addEventListener( goog.events.EventType.TOUCHSTART, sb.InputManager.onTouchEvent_);
	element.addEventListener( goog.events.EventType.TOUCHMOVE, sb.InputManager.onTouchEvent_);
	element.addEventListener( goog.events.EventType.TOUCHEND, sb.InputManager.onTouchEvent_);
}

sb.InputManager.onTouchEvent_ = function(event) {
	var touches = event.changedTouches;
	var first;
	if(goog.isDefAndNotNull(touches)) {
		first = touches[0];
	}
    var type = "";
	
	switch(event.type)
	{
	    case goog.events.EventType.TOUCHSTART: type = goog.events.EventType.MOUSEDOWN; break;
	    case goog.events.EventType.TOUCHMOVE:  type = goog.events.EventType.MOUSEMOVE; break;        
	    case goog.events.EventType.TOUCHEND:   type = goog.events.EventType.MOUSEUP; break;
	    default: return;
	}
	
	var simulatedEvent = document.createEvent("MouseEvent");
	simulatedEvent.initMouseEvent(type, true, true, window, 1, 
	                          first.screenX, first.screenY, 
	                          first.clientX, first.clientY, false, 
	                          false, false, false, 0/*left*/, null);
	                                       
	first.target.dispatchEvent(simulatedEvent);
	event.preventDefault();
}

/**
 *  Based on code from qodo.co.uk
 *  by Stewart Orr
 *  http://creativecommons.org/licenses/by/3.0/
 *  
 *  Restrict the characters that can be typed into an input element.
 *  
 *  @param {Element} inputElement
 *  @param {RegExp} restrictionType
 **/
sb.InputManager.addInputRestriction = function(inputElement, restrictionType) {
	inputElement.onkeypress = function(event){return sb.InputManager.onKeyRestrict_(this, event, restrictionType)};
}

sb.InputManager.DIGITS_ONLY = /[1234567890]/g;
sb.InputManager.NUMBER_ONLY = /[0-9\.]/g;
sb.InputManager.ALPHA_ONLY = /[A-Z]/g;


/**
 * @param {Element} input
 * @param {Event} e
 * @param {RegExp} restrictionType
 */
sb.InputManager.onKeyRestrict_ = function(input, e, restrictionType) {
	
	if (!e) {
		e = window.event;
	}
	var code;
	if (e.keyCode) {
		code = e.keyCode;
	}
	else if (e.which) {
		code = e.which;
	}
	
	var character = String.fromCharCode(code);
	// if they pressed esc... remove focus from field...
	if (code==27) { input.blur(); return false; }
	
	// ignore if they are press other keys
	// strange because code: 39 is the down key AND ' key...
	// and DEL also equals .
	if (!e.ctrlKey && code!=9 && code!=8 && code!=36 && code!=37 && code!=38 && (code!=39 || (code==39 && character=="'")) && code!=40) {
		if (character.match(restrictionType)) {
			return true;
		} 
		else {
			return false;
		}
	}
}
/** End code from quodo.co.uk */

/**
 * @param {Element} input
 * @param {Event} e
 * @param {string} restrictionType
 */
sb.InputManager.onBlurConvert_ = function(input, e, restrictionType) {
	switch(input.value.toLowerCase()) {
		
		case "":
			break;
			
		case "0":
		case "f":
		case "false":
			input.value = "false";
			break;
			
		default:
			input.value = "true";
			break;
	}
}
