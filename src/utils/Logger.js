goog.provide('sb.Logger');

goog.require('goog.debug.Logger');
goog.require('goog.debug.Console');

sb.Logger = {};

sb.Logger.init = function() {
	sb.Logger.logger_ = goog.debug.Logger.getLogger('sb.Logger');
	sb.Logger.console_ = new goog.debug.Console();
	sb.Logger.console_.setCapturing(true);
}

/**
 * @param {string} msg
 * @param {?goog.debug.Logger.Level=} level
 */
sb.Logger.log = function(msg, level) {
	if( !goog.isDefAndNotNull(level) ) {
		level = goog.debug.Logger.Level.INFO;
	}
	sb.Logger.logger_.log(level, msg);
}

/**
 * @return {goog.debug.Console}
 */
sb.Logger.getConsole = function() {
	return sb.Logger.console_;
}

goog.exportSymbol('sb.Logger.getConsole', sb.Logger.getConsole);
goog.exportSymbol('sb.Logger.log', sb.Logger.log);