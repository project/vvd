goog.provide('sb.Utils');

goog.require('goog.events');

sb.Utils = {};

/**
 * Stops the propagation of the event.
 * 
 * @param {Event} event 
 */
sb.Utils.stopEvent_ = function(event) {
	event.stopPropagation();
}

/**
 * @param {string} color
 * @param {number=} factor
 * 
 * @return {string}
 */
sb.Utils.darkenColor = function(color, factor)
{
	var rgb = sb.Utils.getRGB(goog.string.toNumber("0x" + color.substr(1)));
	
	if(!goog.isDefAndNotNull(factor)) {
		factor = 1.2;
	}
	
	var darkR, darkG, darkB;
	darkR = Math.floor(rgb[0] * (1 / factor));
	darkG = Math.floor(rgb[1] * (1 / factor));
	darkB = Math.floor(rgb[2] * (1 / factor));
	
	var darkColor = darkR * 0x10000 + darkG * 0x100 + darkB;
	
	var darkColorStr = "#" + darkColor.toString(16);
	return darkColorStr;
}

/**
 * @param {string} color
 * @param {number=} factor
 * 
 * @return {string}
 */
sb.Utils.lightenColor = function(color, factor)
{
	var rgb = sb.Utils.getRGB(goog.string.toNumber("0x" + color.substr(1)));
	
	if(!goog.isDefAndNotNull(factor)) {
		factor = 0.85;
	}
	
	var lightR, lightG, lightB;
	lightR = Math.min( Math.floor(rgb[0] * (1 / factor)), 0xFF );
	lightG = Math.min( Math.floor(rgb[1] * (1 / factor)), 0xFF );
	lightB = Math.min( Math.floor(rgb[2] * (1 / factor)), 0xFF );
	
	var lightColor = lightR * 0x10000 + lightG * 0x100 + lightB;
	
	var lightColorStr = "#" + lightColor.toString(16);
	return lightColorStr;
}


/**
 * @param {number} color
 * 
 * @return {Array.<number>}
 */
sb.Utils.getRGB = function(color)
{
	var rg = Math.floor(color / 0x100);
	var b = color - rg * 0x100;
	var r = Math.floor(rg / 0x100);
	var g = rg - r * 0x100;
	
	return [r, g, b];
}

/**
 * Based on code from http://diveintohtml5.org/detect.html#canvas
 * by Mark Pilgrim
 * http://creativecommons.org/licenses/by/3.0/
 * 
 * @return {boolean}
 */
sb.Utils.supportsCanvas = function() {
	return !!document.createElement('canvas').getContext;
}