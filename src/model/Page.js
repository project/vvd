/**
 *	@fileoverview Page is a collection of blocks.
 *	@author clgreenhill@gmail.com (Colin Greenhill)
 */
goog.provide('sb.Page');

goog.require('sb.AbstractBlockParent');

goog.require('goog.array');

/**
 * Creates a new Page.
 * 
 * @param {string=} name
 * 
 * @constructor
 * @extends {sb.AbstractBlockParent}
 */
sb.Page = function( name ) {

	/**
	 * @type {string}
	 * @private
	 */
	if(goog.isDefAndNotNull(name)) {
		this.name_ = name;
	}
	
	sb.AbstractBlockParent.call(this, sb.Page.TYPE);
	
};
goog.inherits(sb.Page, sb.AbstractBlockParent);

sb.Page.TYPE = "page";

/**
 * @return {string}
 */
sb.Page.prototype.getName = function() {
	return this.name_
}

/**
 * @return {Array.<sb.Block>}
 */
sb.Page.prototype.getAllBlocks = function() {
	
	var allBlocks = [];
	
	for(var i = 0; i < this.blocks_.length; i++) {
		var block = this.blocks_[i];
		allBlocks.push(block);
		allBlocks = allBlocks.concat(this.getSubBlocks_(block));
	}
	
	return allBlocks;
};

// Move to sb.Block?
/**
 * @param {sb.Block} block
 * 
 * @return {Array.<sb.Block>}
 */
sb.Page.prototype.getSubBlocks_ = function( block ) {
	
	var subBlocks = [];
	
	
	subBlocks = block.getChildBlocks();
	
	if(goog.isDefAndNotNull(block.getAfter())) {
		subBlocks.push( block.getAfter() );
	}
	var subSubBlocks = [];
	for(var i = 0; i < subBlocks.length; i += 1) {
		
		var subBlock = subBlocks[i];
		subSubBlocks.push(this.getSubBlocks_(subBlock));
	}
	subSubBlocks = goog.array.flatten(subSubBlocks);
	subBlocks = goog.array.concat(subBlocks, subSubBlocks);
	
	return subBlocks;
}

goog.exportSymbol('sb.Page', sb.Page);
goog.exportSymbol('sb.Page.prototype.addBlock', sb.Page.prototype.addBlock);