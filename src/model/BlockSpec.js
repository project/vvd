goog.provide('sb.BlockSpec');

goog.require('sb.SBObject');
goog.require('sb.Argument');
goog.require('sb.DataType');
goog.require('sb.SocketType');

/**
 * BlockSpec defines the properties of a Block.
 * 
 * @constructor
 */
sb.BlockSpec = function() {
	
	this.name = "";
	this.label = "";
	this.initLabel = "";
	this.connections = [];
	this.shape = "";
	this.arguments = [];
	this.langSpecProperties = {};
	this.returnType = "";
	this.color = "";
	
};


sb.BlockSpec.extend = function( spec, newSpec ) {
		var prop;
		
		if(!goog.isDefAndNotNull(newSpec)) {
			newSpec = {};
		}
		
		for (prop in spec) {
			if (newSpec[prop] === undefined) {
				if(goog.isArray(spec[prop])) {
					newSpec[prop] = goog.array.clone(spec[prop]);
				}
				else {
					newSpec[prop] = spec[prop];
				}
			}
		}
		return newSpec;
	}
						
	
sb.BlockSpec.DEFAULT = {
		label: "Block",
		color: "#66FF66"
	};

sb.BlockSpec.HAT = sb.BlockSpec.extend(sb.BlockSpec.DEFAULT, {
		connections: [sb.SocketType.AFTER]
	});

sb.BlockSpec.BEFORE_AFTER = sb.BlockSpec.extend(sb.BlockSpec.DEFAULT, {
		label: "Block",
		color: 0x66FF66,
		arguments: [],
		connections: [sb.SocketType.AFTER, sb.SocketType.BEFORE]
	});

sb.BlockSpec.RETURN = sb.BlockSpec.extend(sb.BlockSpec.DEFAULT, {
	label: "Return @returnVal",
	color: "#66FF66",
	arguments: [
			new sb.Argument("returnVal", "block", "nested")            
	],
	connections: [sb.SocketType.BEFORE],
	returnType: "%v"
});

sb.BlockSpec.IF_THEN = sb.BlockSpec.extend(sb.BlockSpec.BEFORE_AFTER, { 
		label: "If @condition\nthen @command",
		arguments: [
			new sb.Argument("condition", "boolean", "nested"),
			new sb.Argument("command", "command", "nested")
		]
	});

sb.BlockSpec.IF_THEN_ELSE = sb.BlockSpec.extend(sb.BlockSpec.BEFORE_AFTER, { 
	label: "If @condition\nthen @command\nelse @elseCommand",
	arguments: [
		new sb.Argument("condition", "boolean", "nested"),
		new sb.Argument("command", "block", "nested"),
		new sb.Argument("elseCommand", "block", "nested")
	]
});

											  
sb.BlockSpec.WHILE = sb.BlockSpec.extend(sb.BlockSpec.BEFORE_AFTER, { 
		label: "While @condition\ndo @command",
		arguments: [
			new sb.Argument("condition", "boolean", "nested"),
			new sb.Argument("command", "command", "nested")
		]
});

sb.BlockSpec.REPEAT = sb.BlockSpec.extend(sb.BlockSpec.BEFORE_AFTER, { 
		label: "Repeat @numTimes times",
		arguments: [
		    new sb.Argument("numTimes", "number", "nested", 10)
		]
});

sb.BlockSpec.PROCEDURE = sb.BlockSpec.extend(sb.BlockSpec.HAT, { 
		label: "Procedure",
		color: "#99FF66"
});
