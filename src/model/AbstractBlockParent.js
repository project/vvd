goog.provide('sb.AbstractBlockParent');

goog.require('sb.SBObject');
goog.require('sb.Block');
goog.require('sb.BlockEvent');

goog.require('goog.array');
goog.require('goog.string');

/**
 * @param {string} objectType The type of the subclass.
 * 
 * @constructor
 * @extends {sb.SBObject}
 * @implements {sb.IBlockParent}
 */
sb.AbstractBlockParent = function( objectType ) {
	
	sb.SBObject.call(this, objectType);
	/**
	 * @type {Array.<sb.Block>}
	 * @private
	 */
	this.blocks_ = [];
	
	/**
	 * @type {sb.SBObject}
	 * @private
	 */
	this.parent_ = null;
	
};
goog.inherits(sb.AbstractBlockParent, sb.SBObject);



/**
 * @inheritDoc
 */
sb.AbstractBlockParent.prototype.addBlock = function(block) {
	if(goog.array.indexOf(this.blocks_, block) !== -1) {
		return false;
	}
	this.blocks_.push(block);
	block.setParent(this);
	this.log(this.getID() + ": added child: " + block.getID())
	
	var event = new sb.BlockParentEvent( sb.BlockParentEvent.BLOCK_ADDED );
	event.children = [block];
	this.dispatchEvent( event );
	
	return true;
};

/**
 * @inheritDoc
 */
sb.AbstractBlockParent.prototype.getChildBlocks = function() {
	
	return goog.array.clone( this.blocks_ );
};

/**
 * @inheritDoc
 */
sb.AbstractBlockParent.prototype.removeBlock = function( childBlock ) {
	for( var i = 0; i < this.blocks_.length; i += 1 ) {
		if(this.blocks_[i] === childBlock) {
			childBlock.setParent(null);
			goog.array.splice( this.blocks_, i, 1);
			this.log( this.getID() + ": removed child: " + childBlock.getID() );
			
			var event = new sb.BlockParentEvent(  sb.BlockParentEvent.BLOCK_REMOVED );
			event.children = [childBlock];
			this.dispatchEvent( event );
			
			return true;
		}
	}
	this.log( this.getID() + ": remove child FAILED: " + childBlock.getID() );
	return false;
};

/**
 * @inheritDoc
 */
sb.AbstractBlockParent.prototype.removeAllBlocks = function() {
	
	var allBlocks = goog.array.clone( this.blocks_ );
	
	for( var i = 0; i < this.blocks_.length; i += 1 ) {
		this.blocks_[i].setParent(null);
	}
	
	this.blocks_ = [];
	
	var event = new sb.BlockEvent(  sb.BlockParentEvent.ALL_BLOCKS_REMOVED );
	this.dispatchEvent( event );
};

/**
 * @param {sb.SBObject} parent
 */
sb.AbstractBlockParent.prototype.setParent = function( parent ) {
	this.parent_ = parent;
}

/**
 * @return {sb.SBObject}
 */
sb.AbstractBlockParent.prototype.getParent = function() {
	return this.parent_;
}