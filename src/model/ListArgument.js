goog.provide('sb.ListArgument');

goog.require('sb.Argument');

/**
 * @param {string} name
 * @param {string} dataType
 * @param {string} socketType
 * @param {string|number|boolean|?sb.Block=} value
 * 
 * @constructor
 * @extends {sb.Argument}
 */
sb.ListArgument = function( name, dataType, socketType, value) {

	sb.Argument.call( this, name, dataType, socketType, value );
	
	/**
	 * @type {Array}
	 * @private
	 */
	this.options_ = [];

};
goog.inherits(sb.ListArgument, sb.Argument);

/** @typedef {{label: string, value: *}} */
sb.ListArgument.Option;

/**
 * Adds a single option to the list.
 * 
 * @param {sb.ListArgument.Option|string|number|boolean} option
 */
sb.ListArgument.prototype.addOption = function( option ) {
	
	option = sb.ListArgument.toOption( option );
	
	this.options_.push( option );
	
	var event = new sb.ArgumentEvent( sb.ArgumentEvent.OPTIONS_CHANGED );
	this.dispatchEvent( event );
}

/**
 * Adds an array of options the list.
 * 
 * @param {Array.<sb.ListArgument.Option|string|number|boolean>} options
 */
sb.ListArgument.prototype.addOptions = function( options ) {
	
	for(var i = 0; i < options.length; i += 1) {
		options[i] = sb.ListArgument.toOption( options[i] );
	}
	
	goog.array.extend(this.options_, options);
	
	var event = new sb.ArgumentEvent( sb.ArgumentEvent.OPTIONS_CHANGED );
	this.dispatchEvent( event );
}

/**
 * Returns a copy of the list of options.
 * 
 * @return {Array.<sb.ListArgument.Option>}
 */
sb.ListArgument.prototype.getOptions = function() {
	return goog.array.clone(this.options_);
}

/** 
 * Removes an option from the list.
 * 
 * @param {sb.ListArgument.Option|string|number|boolean} option
 */
sb.ListArgument.prototype.removeOption = function( option ) {
	
	for(var i = 0; i < this.options_.length; i += 1) {
		
		if(typeof(option) == "string" || typeof(option) == "number" || typeof(option) == "boolean" ) {
			
			if(this.options_[i].value === option) {
				
				this.options_.splice(i, 1);
				break;
			}
		}
		else if( "value" in option && "label" in option ) {
			
			if(this.options_[i].value === option.value && this.options_[i].label === option.label) {
				
				this.options_.splice(i, 1);
				break;
			}
		}
		else if( "value" in option ) {
			if(this.options_[i].value === option.value) {
				
				this.options_.splice(i, 1);
				break;
			}
		}
	}
	
	var event = new goog.events.Event( "optionsChanged" );
	this.dispatchEvent( event );
}

/** 
 * Removes all options.
 */
sb.ListArgument.prototype.removeAllOptions = function() {
	
	this.options_ = [];
	
	var event = new goog.events.Event( "optionsChanged" );
	this.dispatchEvent( event );
}

/**
 * Converts the given value into an object that has "label" and "value" properties.
 * 
 * @param {sb.ListArgument.Option|string|number|boolean} value
 * 
 * @return {sb.ListArgument.Option}
 */
sb.ListArgument.toOption = function( value ) {
	
	var option = {label: "undefined", value: undefined};
	
	if( typeof(value) == "string" || typeof(value) == "number" || typeof(value) == "boolean" ) {
		option = {label: value.toString(), value: value};
	}
	else if( "label" in value && typeof(value.label) == "string" && "value" in value ) {
		option = /**@type {sb.ListArgument.Option}*/(value);
	}
	else if( "value" in value ) {
		option = {label: value.value.toString(), value: value.value};
	}
		
	return option;
}