goog.provide('sb.BlockParentEvent');

goog.require('goog.events.Event');
/**
 * @inheritDoc
 * 
 * @extends {goog.events.Event}
 * @constructor
 */
sb.BlockParentEvent = function( type ){
	this.type = type;
}
goog.inherits(sb.BlockParentEvent, goog.events.Event);

/** @type {Array.<sb.Block>} */
sb.BlockParentEvent.prototype.children;

/** @define {string} */
sb.BlockParentEvent.BLOCK_ADDED = "blockAdded";

/** @define {string} */
sb.BlockParentEvent.BLOCK_REMOVED = "blockRemoved";

/** @define {string} */
sb.BlockParentEvent.ALL_BLOCKS_REMOVED = "allBlocksRemoved";