/**
 * @fileoverview Workspace contains pages, drawers, and eventually maybe widgets.
 * 
 * @author Colin Greenhill
 */

goog.provide('sb.Workspace');

goog.require('sb.Block');
goog.require('sb.Page');
goog.require('sb.Drawer');
goog.require('sb.SBObject');

goog.require('sb.WorkspaceEvent');

goog.require('goog.array');
goog.require('goog.dom');

/**
 * @constructor
 * @extends {sb.SBObject}
 */
sb.Workspace = function() {
	
	sb.SBObject.call(this, sb.Workspace.TYPE);
	this.pages_ = [];
	this.drawers_ = [];
	
	this.curPage_ = null;
	this.curDrawer_ = null;
	
	this.selectedBlocks_ = [];
	
	this.dirtyFlag_ = false;
}
goog.inherits(sb.Workspace, sb.SBObject);

sb.Workspace.TYPE = "workspace";

/**
 *	@return {Array.<sb.Drawer>}
 */
sb.Workspace.prototype.getDrawers = function() {
	return goog.array.clone(this.drawers_);
}

/**
 *	@return	{Array.<sb.Page>}
 */
sb.Workspace.prototype.getPages = function() {
	return goog.array.clone(this.pages_);
}


/**
 * @return {sb.Page}
 */
sb.Workspace.prototype.getCurrentPage = function() {
	
	return this.curPage_;
}

/**
 * @return {sb.Drawer}
 */
sb.Workspace.prototype.getCurrentDrawer = function() {
	
	return this.curDrawer_;
}


// TODO: removePage/removeDrawer methods

/**
 *	@param {sb.Drawer} drawer
 */
sb.Workspace.prototype.addDrawer = function(drawer) {
	this.drawers_.push(drawer);

	drawer.setParentEventTarget( this );
	
	var event = new sb.WorkspaceEvent( sb.WorkspaceEvent.DRAWER_ADDED );
	event.drawer = drawer;
	this.dispatchEvent( event );
	
	// if this is the only drawer, make it the current drawer
	if(this.drawers_.length == 1) {
		this.setCurrentDrawer( drawer );
	}
}

/**
 *	@param {sb.Page} page
 */
sb.Workspace.prototype.addPage = function(page) {
	this.pages_.push(page);
	
	page.setParentEventTarget( this );
	
	var event = new sb.WorkspaceEvent( sb.WorkspaceEvent.PAGE_ADDED );
	event.page = page;
	this.dispatchEvent( event );
	
	// if this is the only page, make it the current page
	if(this.pages_.length == 1) {
		this.setCurrentPage( page );
	}
}

/**
 * @param {sb.Page} page
 */
sb.Workspace.prototype.setCurrentPage = function(page) {
	
	this.curPage_ = page;
	
	var event = new sb.WorkspaceEvent( sb.WorkspaceEvent.PAGE_CHANGED );
	event.page = page;
	this.dispatchEvent( event );
}

/**
 * @param {sb.Drawer} drawer
 */
sb.Workspace.prototype.setCurrentDrawer = function(drawer) {
	
	this.curDrawer_ = drawer;

	var event = new sb.WorkspaceEvent( sb.WorkspaceEvent.DRAWER_CHANGED );
	event.drawer = drawer;
	this.dispatchEvent( event );
}

sb.Workspace.prototype.nextDrawer = function() {
	
	var i, curIndex;
	for(i = 0; i < this.drawers_.length; i += 1) {
		if(this.drawers_[i] === this.curDrawer_) {
			curIndex = i;
			break;
		}
	}
	
	if( goog.isDefAndNotNull(curIndex) ) {
		curIndex += 1;
		if(curIndex >= this.drawers_.length) {
			curIndex = 0;
		}
		
		this.curDrawer_ = this.drawers_[curIndex];
		
		var event = new sb.WorkspaceEvent( sb.WorkspaceEvent.DRAWER_CHANGED );
		event.drawer = this.curDrawer_;
		this.dispatchEvent( event );
	}
}

sb.Workspace.prototype.prevDrawer = function() {
	
	var i, curIndex;
	for(i = 0; i < this.drawers_.length; i += 1) {
		if(this.drawers_[i] === this.curDrawer_) {
			curIndex = i;
			break;
		}
	}
	
	if( goog.isDefAndNotNull(curIndex) ) {
		curIndex -= 1;
		if(curIndex < 0) {
			curIndex = this.drawers_.length - 1;
		}
		
		this.curDrawer_ = this.drawers_[curIndex];
		
		var event = new sb.WorkspaceEvent( sb.WorkspaceEvent.DRAWER_CHANGED );
		event.drawer = this.curDrawer_;
		this.dispatchEvent( event );
	}
}

/**
 * @param {?Array.<sb.Block>} newBlocks
 */
sb.Workspace.prototype.setSelection = function( newBlocks ) {
	
	if( !goog.isDefAndNotNull( newBlocks ) ) {
		this.selectedBlocks_ = [];
	}
	else {
		this.selectedBlocks_ = goog.array.clone( newBlocks );
	}
	
	var event = new sb.WorkspaceEvent( sb.WorkspaceEvent.SELECTION_CHANGED );
	this.dispatchEvent( event );
}

/**
 * @return {Array.<sb.Block>}
 */
sb.Workspace.prototype.getSelection = function() {
	return goog.array.clone( this.selectedBlocks_ );
}

/**
 * @param {Array.<sb.Block>} newBlocks
 */
sb.Workspace.prototype.addToSelection = function( newBlocks ) {
	goog.array.extend( this.selectedBlocks_, newBlocks );
	goog.array.removeDuplicates( this.selectedBlocks_ );
	var event = new sb.WorkspaceEvent( sb.WorkspaceEvent.SELECTION_CHANGED );
	this.dispatchEvent( event );
}

/**
 * @param {Array.<sb.Block>} removeBlocks
 */
sb.Workspace.prototype.removeFromSelection = function(removeBlocks) {
	
	for(var i = 0; i < removeBlocks.length; i += 1) {
		var block = removeBlocks[i];
		goog.array.remove( this.selectedBlocks_, block );
	}
	
	var event = new sb.WorkspaceEvent( sb.WorkspaceEvent.SELECTION_CHANGED );
	this.dispatchEvent( event );
}

sb.Workspace.prototype.clearSelection = function() {
	
	this.selectedBlocks_ = [];
	
	var event = new sb.WorkspaceEvent( sb.WorkspaceEvent.SELECTION_CHANGED );
	this.dispatchEvent( event );
}



/**
 * @return {boolean} If the workspace has changed since the dirty flag was cleared.
 */
sb.Workspace.prototype.getDirty = function() {
	return this.dirtyFlag_;
}

/**
 * Sets the dirty flag to false. This should be called after saving the workspace data.
 */
sb.Workspace.prototype.clearDirty = function() {
	this.dirtyFlag_ = false;
}

/**
 * Sets the dirty flag to true. This is called within ScriptBlocks 
 * when a change is made in the workspace.
 */
sb.Workspace.prototype.setDirty = function() {
	this.dirtyFlag_ = true;
}

goog.exportSymbol('sb.Workspace', sb.Workspace);
goog.exportSymbol('sb.Workspace.prototype.addDrawer', sb.Workspace.prototype.addDrawer);
goog.exportSymbol('sb.Workspace.prototype.addPage', sb.Workspace.prototype.addPage);