/**
 *	@fileoverview sb.Block is the model for all programming blocks.
 *	@author clgreenhill@gmail.com (Colin Greenhill)
 */
goog.provide('sb.Block');

goog.require('goog.events.Event');
goog.require('goog.events.EventTarget');

goog.require('goog.array');

goog.require('sb.Argument');
goog.require('sb.ListArgument');
goog.require('sb.SBObject');
goog.require('sb.IBlockParent');
goog.require('sb.BlockSpec');
goog.require('sb.BlockEvent');
goog.require('sb.DataType');

/**
 * Creates a new Block.
 * 
 * @param {sb.BlockSpec} spec The definition that this block uses as a base.
 * 
 * @constructor
 * @extends {sb.SBObject}
 * @implements {sb.IBlockParent}
 */
sb.Block = function( spec ) {
	

	/**
	 * @private
	 * @type {?sb.IBlockParent}
	 */
	this.parent_ = null;
	
	/**
	 * @private
	 * @type {?sb.Block}
	 */
	this.beforeBlock_ = null;
	
	/**
	 * @private
	 * @type {?sb.Block}
	 */
	this.afterBlock_ = null;
	
	/**
	 * @private 
	 * @type {string}
	 */
	this.name_ = spec.name;
	
	/**
	 * @private 
	 * @type {string}
	 */
	this.label_ = spec.label;
	
	/**
	 * @private
	 * @type {Array.<sb.Argument>}
	 */
	this.arguments_ = [];
	
	this.isFactory = false;
	
	/** 
	 * @private
	 * @type {sb.BlockSpec}
	 */
	this.spec_ = spec;
	
	var i;
	if(spec.arguments !== undefined) {
		for(i = 0; i < spec.arguments.length; i += 1) {
			
			var specArg = spec.arguments[i];
			
			var arg;
			var argClass = sb.Argument;
			if(specArg instanceof sb.Argument) {
				
				if(specArg instanceof sb.ListArgument) {
					argClass = sb.ListArgument;
				}
				arg = new argClass(specArg.getName(), specArg.getDataType(), specArg.getSocketType());
				arg.setValue( specArg.getValue() );
				
				if(specArg instanceof sb.ListArgument) {
					/**@type {sb.ListArgument} */(arg).addOptions(specArg.getOptions());
				}
			} 
			else {
				if(specArg.socketType === sb.SocketType.LIST) {
					argClass = sb.ListArgument;
				}
				arg = new argClass(specArg.name, specArg.dataType, specArg.socketType);
				arg.setValue( specArg.value );
				if( goog.isDefAndNotNull(specArg.options) ) {
					arg.addOptions(specArg.options);
				}
			}
			
			arg.setParentEventTarget(this);
			this.arguments_[i] = arg;
		}
	}
	
	/**
	 * @private
	 * @type {Array.<string>}
	 */
	this.connections_ = [];
	
	if(spec.connections !== undefined) {
		this.connections_ = goog.array.clone(spec.connections);
	}
	
	/**
	 * @private
	 * @type {string}
	 */
	this.returnType_ = spec.returnType;
	
	
	/**
	 * @private
	 * @type {goog.math.Coordinate}
	 */
	this.position_ = null;
	
	if( goog.isDefAndNotNull(spec.position) ) {
		this.position_ = new goog.math.Coordinate( spec.position.x, spec.position.y );
	}
	
	/**
	 * @private
	 * @type {string}
	 */
	this.color_ = spec.color;
	
	sb.SBObject.call( this, sb.Block.TYPE );
	for(i = 0; i < this.arguments_.length; i += 1) {
		arg = this.arguments_[i]
		this.registerDisposable( arg );
	}
	
};
goog.inherits(sb.Block, sb.SBObject);

/** @define {string} */
sb.Block.TYPE = "block";

/**
 * @param {string} id
 */
sb.Block.getByID = function( id ) {
	var block = /** @type {sb.Block} */sb.ObjectManager.getInstance().getObject( id, sb.Block.TYPE );
	return block;
}

/**
 * @override
 */
sb.Block.prototype.disposeInternal = function() {
	
	sb.Block.superClass_.disposeInternal.call(this);
	
	var i;
	
	this.parent_ = null;
	this.beforeBlock_ = null;
	this.afterBlock_ = null;
	
	for(i = 0; i < this.arguments_.length; i += 1) {
		var arg = this.arguments_[i];
		arg.setValue(null);
		sb.ScriptBlocks.objMgr_.removeFromMap( arg );
	}
	
	this.arguments_ = [];
	this.spec_ = null;
	this.connections_ = [];
	this.position_ = null;
}


/**
 * @param {string} argName
 * @return {?sb.Argument} 
 */
sb.Block.prototype.getArgument = function( argName ) {
	var arg = null;
	var i, tempArg;
	
	for(i=0; i < this.arguments_.length; i+= 1) {
		tempArg = this.arguments_[i];
		if( goog.isDefAndNotNull(tempArg) && tempArg.getName() === argName ) {
			arg = tempArg;
		}
	}

	return arg;
}

/**
 * Returns an array of all of this block's arguments.
 * 
 * @return {Array.<sb.Argument>}
 */
sb.Block.prototype.getArguments = function() {
	
	return goog.array.clone(this.arguments_);
}


/**
 * Returns an associative array of argument values mapped to
 * the argument names.
 * 
 * @return {Object}
 */
sb.Block.prototype.getArgValues = function() {
	
	var argValues = {};
	
	for(var i = 0; i < this.arguments_.length; i += 1) {
		var arg = this.arguments_[i];
		argValues[arg.getName()] = arg.getValue();
	}
	
	return argValues;
}

/**
 * @return {string}
 */
sb.Block.prototype.getName = function() {
	return this.name_;
}

/**
 * @return {string}
 */
sb.Block.prototype.getLabel = function() {
	return this.label_;
}

/**
 * Returns an array of this block's connections.
 * 
 * @return {Array.<string>}
 */
sb.Block.prototype.getConnections = function() {
	return goog.array.clone(this.connections_);
}

/**
 * @return {string}
 */
sb.Block.prototype.getReturnType = function() {
	return this.returnType_;
}

/**
 * @return {goog.math.Coordinate}
 */
sb.Block.prototype.getPosition = function() {
	return this.position_;
}

/**
 * @return {string}
 */
sb.Block.prototype.getColor = function() {
	return this.color_;
}

/**
 * Returns the base BlockSpec that was used to create this Block.
 * 
 * @return {sb.BlockSpec}
 */
sb.Block.prototype.getBaseSpec = function() {
	
	return this.spec_;
}

/**
 * Returns the current definition of this block.
 * 
 * @param {boolean} includeSimpleValues	If the spec should include all non-block values of arguments.
 * 
 * @return {sb.BlockSpec}
 */
sb.Block.prototype.getSpec = function( includeSimpleValues ) {
	var spec = new sb.BlockSpec();
	spec.name = this.name_;
	spec.label = this.label_;
	spec.connections = goog.array.clone(this.connections_);
	spec.returnType = this.returnType_;
	spec.color = this.color_;
	
	spec.arguments = [];
	
	var i;
	for( i = 0; i < this.arguments_.length; i += 1 ) {
		var arg = this.arguments_[i];
		var argClass = sb.Argument;
		if( arg instanceof sb.ListArgument ) {
			argClass = sb.ListArgument;
		}
		var argCopy = new argClass(
			arg.getName(),
			arg.getDataType(),
			arg.getSocketType()
		);
		
		if(includeSimpleValues) {
			var argValue = arg.getValue();
			if( !(argValue instanceof sb.Block) ) {
				argCopy.setValue( argValue );
			}
		}
	
		if( arg instanceof sb.ListArgument ) {
			var options = arg.getOptions();
			if( goog.isDefAndNotNull( options ) && options.length > 0 ) {
				argCopy.addOptions(options);
			}
		}
		
		spec.arguments.push(argCopy);
	}
	
	return spec;
}


sb.Block.prototype.getJSONSpec = function() {
	var jsonArgs = [];
	for(var i = 0; i < this.arguments_.length; i += 1) {
		var arg = this.arguments_[i];
		var jsonArg = {
			name: arg.getName(),
			dataType: arg.getDataType(),
			socketType: arg.getSocketType()
		};
		
		if( arg.getSocketType() === sb.SocketType.LIST ) {
			var options = arg.getOptions();
			if( goog.isDefAndNotNull( options ) && options.length > 0 ) {
				jsonArg.options = goog.array.clone(options);
			}
		}
		
		jsonArgs.push(jsonArg);
	}
	
	var spec  = {};
	spec.name = this.name_;
	spec.label = this.label_;
	spec.arguments = jsonArgs;
	spec.connections = goog.array.clone(this.connections_);
	spec.color = this.color_;
	spec.returnType = this.returnType_;
	
	return spec;
}

/**
 * Returns all child blocks of this block.
 * 
 * @return {Array.<sb.Block>}
 * @override
 */
sb.Block.prototype.getChildBlocks = function() {
	var childBlocks = [];
	var arg, childBlock, i;
	for( i=0; i < this.arguments_.length; i += 1) {
		arg = this.arguments_[i];
		childBlock = arg.getValue();
		if(childBlock !== null && childBlock instanceof sb.Block) {
			childBlocks.push( childBlock );
		}
	}
	
	return childBlocks;
}

/**
 * @param {string} newLabel
 */
sb.Block.prototype.setLabel = function( newLabel ) {
	
	if( this.label_ != newLabel ) {
		this.label_ = newLabel;
		
		var event = new sb.BlockEvent( sb.BlockEvent.LABEL_CHANGED );
		this.dispatchEvent( event );
	}
}

/**
 * Set the position of the block 
 * 
 * @param {?goog.math.Coordinate|number} arg1	X position or coordinate
 * @param {number=}	opt_arg2					Y position.
 */
sb.Block.prototype.setPosition = function( arg1, opt_arg2 ) {
	
	if (arg1 instanceof goog.math.Coordinate) {
		this.position_ = arg1.clone();
	} 
	else if( goog.isDefAndNotNull( arg1 ) && goog.isDefAndNotNull( opt_arg2 ) ) {
		this.position_ = new goog.math.Coordinate( arg1, opt_arg2 );
	}
	else {
		this.position_ = null;
	}
	
	if( goog.isDefAndNotNull( arg1 ) ) {
		var event = new sb.BlockEvent( sb.BlockEvent.MOVED );
		this.dispatchEvent( event );
	}
}

/**
 * Connects the block to the first argument that matches the returnType and
 * does not already have a block as its value.
 * 
 * @inheritDoc
 */
sb.Block.prototype.addBlock = function( block ) {
	for(var i = 0; i < this.arguments_.length; i += 1) {
		var arg = this.arguments_[i];
		if(!(arg.getValue() instanceof sb.Block) && arg.getDataType() === block.getReturnType()) {
			this.connectChildBlock(block, arg.getName());
			return true;
		}
	}
	
	return false;
}

/**
 * Set the parent for this block.
 * 
 * @param {?sb.IBlockParent} parent
 */
sb.Block.prototype.setParent = function( parent ) {
	this.parent_ = parent;
	
	if(parent === null) {
		var event = new sb.SBObjectEvent( sb.SBObjectEvent.NULL_PARENT );
		this.dispatchEvent( event );
	}
	else {
		this.setParentEventTarget( /**@type {goog.events.EventTarget}*/(parent) );
	}
}

/**
 * Returns the parent for this block.
 * 
 * @return {?sb.IBlockParent}
 */
sb.Block.prototype.getParent = function() {
	return this.parent_;
}

/**
 * Returns an array of blocks connected to this one by before/after connections.
 * 
 * @return {Array.<sb.IBlockParent>}
 */
sb.Block.prototype.getStack = function() {
	var stack = [];
	stack.push(this);
	
	var beforeBlock = this.getBefore();
	while( goog.isDefAndNotNull(beforeBlock) ) {
		goog.array.insertAt(stack, beforeBlock, 0);
		beforeBlock = beforeBlock.getBefore();
	}
	
	var afterBlock = this.getAfter();
	while( goog.isDefAndNotNull(afterBlock) ) {
		stack.push(afterBlock);
		afterBlock = afterBlock.getAfter();
	}
	
	return stack;
}

/**
 * Creates new argument and adds it to this block.
 * 
 * @param {string} name			The name of the argument to create.
 * @param {string} type			The type of argument to create.
 * @param {string} socketType 	The type of socket the argument will be displayed as
 * @param {boolean=} newLine 	Whether the socket for this argument should be placed on a new line
 * @param {number=} index		The index for the new argument.
 * 
 * @return {?sb.Argument} The new argument.
 */
sb.Block.prototype.addArgument = function(name, type, socketType, newLine, index) {
	
	if( goog.isDefAndNotNull( this.getArgument(name) ) ) {
		return null;
	}
	
	var arg = new sb.Argument( name, type, socketType );
	
	var argLabel;
	if(newLine) {
		argLabel = "\n @" + name;
	}
	else {
		argLabel = " @" + name;
	}
	
	if(index == undefined ) {
		this.label_ += argLabel;
		this.arguments_.push(arg);
	}
	else
	{
		// position if index is specified
		var prevArg = this.arguments_[index];
		if(goog.isDefAndNotNull(prevArg)) {
			var strPos = this.label_.indexOf("@" + prevArg.getName());
			this.label_ = this.label_.substring(0, strPos) + argLabel + " " + this.label_.substr(strPos);
		}
		else {
			this.label_ = this.label_ += "\n @" + name;
		}
		
		goog.array.splice( this.arguments_, index, 0, arg);
	}
	
	this.registerDisposable( arg );
	arg.setParentEventTarget( this );
	
	var event = new sb.BlockEvent( sb.BlockEvent.ARGUMENT_ADDED );
	event.argument = arg;
	
	this.dispatchEvent( event );
	
	return arg;
}

sb.Block.prototype.removeArgument = function( argName ) {
	
	var arg = this.getArgument( argName );
	
	var argVal = arg.getValue();
	if( argVal instanceof sb.Block ) {
		this.removeBlock( /**@type {sb.Block}*/(argVal) );
	}
	
	goog.array.remove( this.arguments_, arg );
	
	this.label_ = goog.string.remove( this.label_, "@" + argName);
	
	var event = new sb.BlockEvent( sb.BlockEvent.ARGUMENT_REMOVED );
	event.argument = arg;
	
	this.dispatchEvent( event );
}

/**
 * @param {sb.Block} childBlock
 * @param {string} argName
 * @return {boolean} If the block was successfully added as a child.
 */
sb.Block.prototype.connectChildBlock = function( childBlock, argName ) {
	
	var childConnected = false;
	
	var arg = this.getArgument(argName);
	if(arg === null) {
		return false;
	}
	
	var argVal = arg.getValue();
	if(argVal !== undefined && argVal !== null) {
		if(argVal instanceof sb.Block) {
			return false;
		}
	}
	
	var oldParent = childBlock.getParent();
	if( goog.isDefAndNotNull( oldParent ) ) {
		oldParent.removeBlock(childBlock);
	}
	
	// suppress the argument value change event, since adding a child block has its own event
	arg.setValue(childBlock, true);
	childBlock.setParent(this);
	
	childBlock.setPosition( null );
	
	childConnected = true;
	
	this.log(this.id_ + ": added child: " + childBlock.getID());
	
	var event = new sb.BlockEvent( sb.BlockEvent.CHILD_CONNECTED );
	event.childBlock = childBlock;
	event.argName = arg.getName();
	this.dispatchEvent( event );
	
	return childConnected;
}

/**
 * @param {sb.Block} childBlock The block to remove.
 * 
 * @return {boolean} If the child block was removed from this block.
 */
sb.Block.prototype.removeBlock = function( childBlock ) {
	
	var childRemoved = false,
	argName = null;
		
	var arg;
	for( var i = 0; i < this.arguments_.length; i += 1 ) {
		arg = this.arguments_[i];
		if( goog.isDefAndNotNull(arg) && arg.getValue() === childBlock ) {
			
			arg.setValue(null, true);
			childRemoved = true;
			childBlock.setParent(null);
			
			break;
		}
	}
	
	if(childRemoved) {
		this.log(this.id_ + ': removed child: ' + childBlock.getID());
	} 
	else {
		this.log(this.id_ + ': remove child: ' + childBlock.getID() + " - FAILED");
	}
	
	var event = new sb.BlockEvent( sb.BlockEvent.CHILD_DISCONNECTED );
	event.childBlock = childBlock;
	this.dispatchEvent( event );
	
	return childRemoved;
}

sb.Block.prototype.removeAllBlocks = function() {
	
	var event = new sb.BlockEvent( sb.BlockEvent.CHILD_DISCONNECTED );
	this.dispatchEvent( event );
}

/**
 * Connects a sibling block after this block. The method has no effect if
 * this block already has an afterBlock.
 * 
 * @param {sb.Block} newAfterBlock
 * 
 * @return {boolean} If the after block was connected.
 */ 
sb.Block.prototype.connectAfterBlock = sb.Block.prototype.connectAfter = function( newAfterBlock ) {
	
	var oldBeforeBlock = newAfterBlock.getBefore();
	if( goog.isDefAndNotNull( oldBeforeBlock ) ) {
		oldBeforeBlock.disconnectAfterBlock();
	}
	
	var oldParent = newAfterBlock.getParent();
	if( goog.isDefAndNotNull(oldParent) ) {
		oldParent.removeBlock( newAfterBlock );
	}
	
	if(this.afterBlock_ !== null) {
		return false;
	}
	
	this.afterBlock_ = newAfterBlock;
	newAfterBlock.setBeforeBlock(this);

	newAfterBlock.setPosition( null );
	
	this.log( this.getID() + ": added after block: " + this.afterBlock_.getID() );
	
	var event = new sb.BlockEvent( sb.BlockEvent.AFTER_CONNECTED );
	event.afterBlock = newAfterBlock;
	this.dispatchEvent( event );
	
	return true;
}

/**
 * Returns the sibling block after this block.
 * 
 * @return {?sb.Block}
 */
sb.Block.prototype.getAfterBlock = sb.Block.prototype.getAfter = function() {
	return this.afterBlock_;
}

sb.Block.prototype.disconnectAfterBlock = sb.Block.prototype.disconnectAfter = function() {
	
	if(this.afterBlock_ === null) {
		return;
	}
	
	this.log( this.getID() + ": removed after block: " + this.afterBlock_.getID() );
	
	var event = new sb.BlockEvent( sb.BlockEvent.AFTER_DISCONNECTED );
	event.afterBlock = this.afterBlock_;
	
	this.afterBlock_.setBeforeBlock( null );
	this.afterBlock_ = null;

	
	this.dispatchEvent( event );
}

/**
 * @param {function( sb.Block, sb.Block, string ):boolean} checkFunction
 * @param {string} argName
 */
sb.Block.prototype.setConnectionCheck = function( checkFunction, argName ) {
	
	if(!goog.isDefAndNotNull(this.connectionChecks_)) {
		this.connectionChecks_ = {};
	}
	
	this.connectionChecks_[argName] = checkFunction;
}

/**
 * @param {string} argName
 * 
 * @return {?function( sb.Block, sb.Block, string ):boolean}
 */
sb.Block.prototype.getConnectionCheck = function( argName ) {
	var func = null;
	
	if(goog.isDefAndNotNull(this.connectionChecks_)) {
		if(goog.isDef(this.connectionChecks_[argName])) {
			func = this.connectionChecks_[argName];
		}
	}
	
	return func;
}

/**
 * @param {?sb.Block} block
 */
sb.Block.prototype.setBeforeBlock = sb.Block.prototype.setBefore = function( block ) {
	this.beforeBlock_ = block;
	this.setParentEventTarget( block );
	this.parent_ = null;
}

/**
 * Returns the sibling block before this block.
 * 
 * @return {?sb.Block}
 */
sb.Block.prototype.getBeforeBlock = sb.Block.prototype.getBefore = function() {
	return this.beforeBlock_;
}

/**
 * Changes the color of the block.
 * 
 * @param {string} color
 */
sb.Block.prototype.setColor = function( color ) {
	
	this.color_ = color;
	
	var event = new sb.BlockEvent( sb.BlockEvent.COLOR_CHANGED );
	this.dispatchEvent( event );
}

/**
 * @param {boolean=} cloneChildren
 * @param {boolean=} cloneAfterStack
 */
sb.Block.prototype.clone = function ( cloneChildren, cloneAfterStack ) {
	
	/** @type {function(new:sb.Block, (sb.BlockSpec))}*/
	var blockCtor;
	if( goog.isDefAndNotNull(this.__proto__) ){
		blockCtor = /** @type {function(new:sb.Block, (sb.BlockSpec))}*/this.__proto__.constructor;
	}
	else {
		blockCtor = /** @type {function(new:sb.Block, (sb.BlockSpec))}*/this.constructor;
	}
	var cloneBlock = (/** @type {sb.Block} */(new blockCtor( this.getSpec(true) )));

	if(cloneChildren) {
		var i;
		for(i = 0; i < this.arguments_.length; i += 1) {
			var arg = this.arguments_[i];
			var argVal = arg.getValue();
			if(argVal instanceof sb.Block) {
				var cloneChild = argVal.clone( true, true );
				cloneBlock.connectChildBlock( cloneChild, arg.getName() );
			}
		}
	}

	if( cloneAfterStack && goog.isDefAndNotNull( this.afterBlock_ ) ) {
		var cloneAfter = this.afterBlock_.clone( true, true );
		cloneBlock.connectAfter( cloneAfter );
	}

	return cloneBlock;
}


goog.exportSymbol('sb.Block', sb.Block);

goog.exportSymbol('sb.Block.prototype.setColor', sb.Block.prototype.setColor);

goog.exportSymbol('sb.Block.prototype.connectChildBlock', sb.Block.prototype.connectChildBlock);
