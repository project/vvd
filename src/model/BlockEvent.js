goog.provide('sb.BlockEvent');

goog.require('goog.events.Event');

/**
 * An event relating to blocks.
 * 
 * @param {string} type The type of event
 * @extends {goog.events.Event}
 * @constructor 
 */
sb.BlockEvent = function( type ) {
	this.type = type;
}
goog.inherits(sb.BlockEvent, goog.events.Event);

/** @type {sb.Block} */
sb.BlockEvent.prototype.childBlock;

/** @type {sb.Block} */
sb.BlockEvent.prototype.afterBlock;


/** @define {string} */
sb.BlockEvent.MOVED = "blockMoved"; 

/** @define {string} */
sb.BlockEvent.LABEL_CHANGED = "blockLabelChanged";

/** @define {string} */
sb.BlockEvent.COLOR_CHANGED = "blockColorChanged";

/** @define {string} */
sb.BlockEvent.CHILD_CONNECTED = "blockChildConnected"; 

/** @define {string} */
sb.BlockEvent.CHILD_DISCONNECTED = "blockChildDisconnected";

/** @define {string} */
sb.BlockEvent.AFTER_CONNECTED = "blockAfterConnected";

/** @define {string} */
sb.BlockEvent.AFTER_DISCONNECTED = "blockAfterDisconnected";

/** @define {string} */
sb.BlockEvent.ARGUMENT_ADDED = "blockArgumentAdded";

/** @define {string} */
sb.BlockEvent.ARGUMENT_REMOVED = "blockArgumentRemoved";