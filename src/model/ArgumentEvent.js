goog.provide('sb.ArgumentEvent');

goog.require('goog.events.Event');

/**
 * An event relating to blocks.
 * 
 * @param {string} type The type of event
 * @extends {goog.events.Event}
 * @constructor 
 */
sb.ArgumentEvent = function( type ) {
	this.type = type;
}
goog.inherits(sb.ArgumentEvent, goog.events.Event);

/** @define {string} */
sb.ArgumentEvent.VALUE_CHANGED = "valueChanged";

/** @define {string} */
sb.ArgumentEvent.OPTIONS_CHANGED = "optionsChanged";