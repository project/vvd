goog.provide('sb.WorkspaceEvent');

goog.require('goog.events.Event');

/**
 * An event relating to blocks.
 * 
 * @param {string} type The type of event
 * @extends {goog.events.Event}
 * @constructor 
 */
sb.WorkspaceEvent = function( type ) {
	this.type = type;
}
goog.inherits(sb.WorkspaceEvent, goog.events.Event);

/** @type {sb.Page} */
sb.WorkspaceEvent.prototype.page;

/** @type {sb.Drawer} */
sb.WorkspaceEvent.prototype.drawer;

/** @define {string} */
sb.WorkspaceEvent.PAGE_ADDED = "pageAdded";

/** @define {string} */
sb.WorkspaceEvent.DRAWER_ADDED = "drawerAdded";

/** @define {string} */
sb.WorkspaceEvent.PAGE_CHANGED = "pageChanged";

/** @define {string} */
sb.WorkspaceEvent.DRAWER_CHANGED = "drawerChanged";

/** @define {string} */
sb.WorkspaceEvent.SELECTION_CHANGED = "selectionChanged";