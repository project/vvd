/**
 * @fileoverview Drawer contains disconnected blocks in a specific order.
 * 
 * @author Colin Greenhill
 */

goog.provide('sb.Drawer');

goog.require('sb.AbstractBlockParent');

/**
 * @param {string=} name
 * 
 * @constructor
 * @extends {sb.AbstractBlockParent}
 */
sb.Drawer = function( name ) {
	/**
	 * @private
	 * @type {string}
	 */
	this.name_ = name ? name : "";
	
	sb.AbstractBlockParent.call(this, sb.Drawer.TYPE);
		
}
goog.inherits(sb.Drawer, sb.AbstractBlockParent);

sb.Drawer.TYPE = "drawer";

/**
 * @param {string} name
 */
sb.Drawer.prototype.setName = function(name) {
	this.name_ = name;
}

/**
 * @return {string}
 */
sb.Drawer.prototype.getName = function() {
	return this.name_;
}

/**
 *	@param {sb.Block} block
 *	@param {number=} index
 *	@override
 */
/*
sb.Drawer.prototype.addBlock = function(block, index) {
	if(index !== undefined) {
		this.blocks_[index] = block;
	}
	else {
		this.blocks_.push(block);
	}
};
*/

goog.exportSymbol('sb.Drawer', sb.Drawer);
goog.exportSymbol('sb.Drawer.prototype.addBlock', sb.Drawer.prototype.addBlock);