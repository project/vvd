goog.provide('sb.SocketType');

/** @enum {string} */
sb.SocketType = {

	NESTED: "nested",
	EDGE: "edge",
	INTERNAL: "internal",
	LIST: "list",
	AFTER: "after",
	BEFORE: "before",
	NONE: "none"
}