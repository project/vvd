goog.provide('sb.SBObjectEvent');

goog.require('goog.events.Event');

/**
 * @param {string} type Event type.
 * @param {Object=} opt_target Reference to the object that is the target of this event.
 * 
 * @constructor
 * @extends {goog.events.Event}
 */
sb.SBObjectEvent = function( type, opt_target ) {
	
	goog.events.Event.call( this, type, opt_target );
	
	/**@type {sb.SBObject}*/
	this.object = null;
	this.data = null;
}
goog.inherits( sb.SBObjectEvent, goog.events.Event);


/** @define {string} */
sb.SBObjectEvent.CREATED = "objectCreated";
/** @define {string} */
sb.SBObjectEvent.DELETED = "objectDeleted";
/** @define {string} */
sb.SBObjectEvent.NULL_PARENT = "objectNullParent";