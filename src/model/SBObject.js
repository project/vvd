goog.provide('sb.SBObject');

goog.require('sb.ObjectManager');
goog.require('sb.SBObjectEvent');

goog.require('goog.events.EventTarget');
goog.require('goog.debug.Logger');

/**
 * @param {string} objectType The type of the subclass.
 * @constructor
 * @extends {goog.events.EventTarget}
 */
sb.SBObject = function( objectType ) {
	
	goog.events.EventTarget.call( this );
	
	sb.SBObject.counter_ += 1;
	
	/**
	 * @type {string}
	 * @private
	 */
	this.id_ = sb.ObjectManager.getInstance().createUniqueID(objectType);
	
	/**
	 * @type {string}
	 * @private
	 */
	this.type_ = objectType;
	
	sb.ObjectManager.getInstance().registerObject( this );
};
goog.inherits(sb.SBObject, goog.events.EventTarget);


/**
 * @type {number}
 * @private
 */
sb.SBObject.counter_ = 0;


/**
 * @type {goog.debug.Logger}
 * @protected
 */
sb.SBObject.prototype.logger = goog.debug.Logger.getLogger('sb.SBObject');

sb.SBObject.prototype.destroy = function() {
	sb.ObjectManager.getInstance().deleteObject( this );
}

/**
 * @return {string}
 */
sb.SBObject.prototype.getID = function() {
	
	return this.id_;
};

/**
 * @return {string}
 */
sb.SBObject.prototype.getType = function() {
	return this.type_;
}

/**
 * @param {string} msg
 * @param {goog.debug.Logger.Level=} level
 */
sb.SBObject.prototype.log = function( msg, level ) {
	if(level === undefined) {
		level = goog.debug.Logger.Level.INFO
	}
	
	this.logger.log(level, msg);
}
