goog.provide('sb.ObjectManager');

goog.require('sb.SBObjectEvent');
goog.require('goog.events.EventTarget');

/**
 * @constructor
 * @extends {goog.events.EventTarget}
 */
sb.ObjectManager = function() {
	this.map_ = {};
	this.typeCounts_ = {};
}
goog.inherits(sb.ObjectManager, goog.events.EventTarget);
goog.addSingletonGetter(sb.ObjectManager);

/**
 *	@param {string} type
 *	
 *	@return {string}
 */
sb.ObjectManager.prototype.createUniqueID = function( type ) {
	
	if(this.typeCounts_[type] === undefined) {
		this.typeCounts_[type] = 0;
		this.map_[type] = {};
	}
	
	this.typeCounts_[type] += 1;
	
	var id = type + this.typeCounts_[type];
	return id;
};

/**
 *	@param {sb.SBObject} obj
 */
sb.ObjectManager.prototype.mapObject = function(obj) {
	
	this.map_[obj.getType()][obj.getID()] = obj;
};

/**
 *	@param {string} id
 *	@param {string=} type
 *
 *	@return {sb.SBObject}
 */
sb.ObjectManager.prototype.getObject = function(id, type) {
	
	var obj = null;
	if(type === undefined) {
		for(type in this.map_) {
			for(var key in this.map_[type]) {
				if( key === id ) {
					obj = this.map_[type][key];
					break;
				}                
			}
		}
	}
	else {
		if(this.map_[type] !== undefined) {
			obj = this.map_[type][id];
		}
	}
	
	return obj;
};

/**
 * @param {string} type
 * 
 * @return {Array.<sb.SBObject>}
 */
sb.ObjectManager.prototype.getObjects = function( type ) {
	var typeArray = []; 
		
	for(var objID in this.map_[type]) {
		var obj = this.map_[type][objID];
		if( obj !== null && obj !== undefined) {
			typeArray.push(obj);
		}
	}
	
	return typeArray;
}

/**
 * @param {sb.SBObject} obj
 */
sb.ObjectManager.prototype.removeFromMap = function( obj ) {
	goog.object.remove(this.map_[obj.getType()], obj.getID());
}

/**
 * @param {sb.SBObject} obj
 */
sb.ObjectManager.prototype.registerObject = function( obj ) {
	
	this.mapObject( obj );
	
	if( !this.suppressEvents_ ) {
		var event = new sb.SBObjectEvent( sb.SBObjectEvent.CREATED );
		event.object = obj;
		
		this.dispatchEvent(event);
	}
}

/**
 * @param {sb.SBObject} obj
 */
sb.ObjectManager.prototype.deleteObject = function( obj ) {
	
	var event = new sb.SBObjectEvent( sb.SBObjectEvent.DELETED );
	event.objectID = obj.getID();
	event.objectType = obj.getType();
	
	this.removeFromMap( obj );
	
	if( !this.suppressEvents_ ) {
		this.dispatchEvent(event);
	}
}

sb.ObjectManager.prototype.suppressEvents = function() {
	this.suppressEvents_ = true;
}

sb.ObjectManager.prototype.resumeEvents = function() {
	this.suppressEvents_ = false;
}

goog.exportSymbol('sb.ObjectManager.getInstance', sb.ObjectManager.getInstance);
