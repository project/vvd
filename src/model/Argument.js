goog.provide('sb.Argument');

goog.require('sb.SBObject');
goog.require('sb.ArgumentEvent');

/**
 * @param {string} name
 * @param {string} dataType
 * @param {string} socketType
 * @param {string|number|boolean|?sb.Block=} value
 * 
 * @constructor
 * @extends {sb.SBObject}
 */
sb.Argument = function( name, dataType, socketType, value) {
	
	sb.SBObject.call( this, sb.Argument.TYPE );
	
	/**
	 * @type {string}
	 * @private
	 */
	this.name_ = name;
	
	/**
	 * @type {string}
	 * @private
	 */
	this.dataType_ = dataType;
	
	/**
	 * @type {string}
	 * @private
	 */
	this.socketType_ = socketType;
	
	/**
	 * @type {string|number|boolean|?sb.Block}
	 * @private
	 */
	this.value_ = value ? value : null;
	
};
goog.inherits(sb.Argument, sb.SBObject);

sb.Argument.TYPE = "argument";

/**
 * @return {string}
 */
sb.Argument.prototype.getName = function() {
	return this.name_;
}

/**
 * @return {string}
 */
sb.Argument.prototype.getDataType = function() {
	return this.dataType_;
}

/**
 * @return {string}
 */
sb.Argument.prototype.getSocketType = function() {
	return this.socketType_;
}


/**
 * @return {?string|number|boolean|sb.Block}
 */
sb.Argument.prototype.getValue = function() {
	return this.value_;
}

/**
 * @param {string|number|boolean|?sb.Block} value
 * @param {boolean=} suppressEvent
 */
sb.Argument.prototype.setValue = function( value, suppressEvent ) {
	
	if(this.dataType_ == sb.DataType.BOOLEAN) {
		if( goog.isDefAndNotNull(value) && !(value instanceof sb.Block) ) {
			
			switch(value.toLowerCase()) {
					
				case "":
					break;
					
				case "0":
				case "f":
				case "false":
					value = "false";
					break;
					
				default:
					value = "true";
					break;
			}
			
			suppressEvent = false;
		}
	}
	
	this.value_ = value;
	
	if(!suppressEvent) {
		var event = new sb.ArgumentEvent( sb.ArgumentEvent.VALUE_CHANGED );
		event.value = value;
		this.dispatchEvent( event );
	}
}

/**
 * @return {sb.Argument}
 */
sb.Argument.prototype.clone = function() {
	
	var cloneValue = this.value_;
	if(cloneValue instanceof sb.Block) {
		cloneValue = cloneValue.clone();
	}
	
	var clonedArg = new sb.Argument(this.name_, this.dataType_, this.socketType_);
	clonedArg.setValue( cloneValue );
	
	return clonedArg;
};

