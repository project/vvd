goog.provide('sb.DataType');

/** @enum {string} */
sb.DataType = {

	BLOCK: "block",
	NUMBER: "number",
	BOOLEAN: "boolean",
	STRING: "string",
	COMMAND: "command",
	POLY: "poly"
}