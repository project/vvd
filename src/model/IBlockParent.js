goog.provide('sb.IBlockParent');

/**
 * @interface
 */
sb.IBlockParent = function() {};

/**
 * @return {string}
 */
sb.IBlockParent.prototype.getID = function(){};

/**
 * Add a block to this block parent.
 * 
 * @param {sb.Block} block The block to add.
 * 
 * @return {boolean} If the block was successfully added.
 */
sb.IBlockParent.prototype.addBlock = function( block ){};

/**
 * Removes a block to from this block parent.
 *  
 * @param {sb.Block} childBlock The block to remove.
 * 
 * @return {boolean} If the block was removed.
 */
sb.IBlockParent.prototype.removeBlock = function( childBlock ){};

/**
 * Removes all Blocks from this block parent.
 */
sb.IBlockParent.prototype.removeAllBlocks = function(){};

/**
 * Returns an array of the blocks that are children of this block parent.
 * 
 * @return {Array.<sb.Block>}
 */
sb.IBlockParent.prototype.getChildBlocks = function(){};