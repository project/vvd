goog.provide('sb.ScriptBlocksXML');

goog.require('sb.ScriptBlocks');
goog.require('sb.Block');
goog.require('sb.BlockSpec');
goog.require('sb.Drawer');
goog.require('sb.Page');
goog.require('sb.Workspace');

goog.require('goog.string');

/**
 * ScriptBlocksXML.js - a convenience class with static methods for initializing, 
 * loading, and saving ScriptBlocks workspaces from and to XML.
 * 
 * @author djwendel
 */


var lang_def_str = '<?xml version="1.0" encoding="UTF-16"?><!DOCTYPE BlockLangDef SYSTEM "sb_lang_def.dtd"><!-- This is a test document for loading ScriptBlocks from an xml file.--><BlockLangDef><BlockGenuses><BlockGenus name="number" kind="data" initlabel="1" editable-label="yes" is-label-value="yes" color="255 216 67"><description><text>Reports the number shown.</text></description><BlockConnectors><BlockConnector connector-kind="plug" connector-type="number" position-type="mirror"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="eval-num"></LangSpecProperty><LangSpecProperty key="is-monitorable" value="yes"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="string" kind="data" initlabel="abc" editable-label="yes" is-label-value="yes" color="255 216 67"><description><text>Reports the text shown.</text></description><BlockConnectors><BlockConnector connector-kind="plug" connector-type="string" position-type="mirror"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="eval-num"></LangSpecProperty><LangSpecProperty key="is-monitorable" value="yes"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="true" kind="data" initlabel="true" color="255 216 67"><description><text>Reports the boolean true.</text></description><BlockConnectors><BlockConnector connector-kind="plug" connector-type="boolean" position-type="mirror"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="true"></LangSpecProperty><LangSpecProperty key="is-monitorable" value="yes"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="false" kind="data" initlabel="false" color="255 216 67"><description><text>Reports the boolean false.</text></description><BlockConnectors><BlockConnector connector-kind="plug" connector-type="boolean" position-type="mirror"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="false"></LangSpecProperty><LangSpecProperty key="is-monitorable" value="yes"></LangSpecProperty></LangSpecProperties></BlockGenus><!-- MOVEMENT --><BlockGenus name="fd" kind="command" initlabel="forward" color="200 235 254"><description><text>Agents moves <arg n="1"/> steps forward.</text><arg-description n="1" name="steps">Number of steps to move.</arg-description></description><BlockConnectors><BlockConnector label="steps" connector-kind="socket" connector-type="number"><DefaultArg genus-name="number" label="1"></DefaultArg></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="fd"></LangSpecProperty><LangSpecProperty key="stack-type" value="breed"></LangSpecProperty></LangSpecProperties></BlockGenus>    <BlockGenus name="bk" kind="command" initlabel="back" color="200 235 254"><description><text>Agents moves <arg n="1"/> steps backward.</text><arg-description n="1" name="steps">Number of steps to move.</arg-description></description><BlockConnectors><BlockConnector label="steps" connector-kind="socket" connector-type="number"><DefaultArg genus-name="number" label="1"></DefaultArg></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="bk"></LangSpecProperty><LangSpecProperty key="stack-type" value="breed"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="lt" kind="command" initlabel="left" color="200 235 254"><description><text>Makes agents turn <arg n="1"/> degrees to the left.</text><arg-description n="1" name="degs">Number of degrees to turn.</arg-description></description><BlockConnectors><BlockConnector label="degs" connector-kind="socket" connector-type="number"><DefaultArg genus-name="number" label="90"></DefaultArg></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="lt"></LangSpecProperty><LangSpecProperty key="stack-type" value="breed"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="rt" kind="command" initlabel="right" color="200 235 254"><description><text>Makes agents turn <arg n="1"/> degrees to the right.</text><arg-description n="1" name="degs">Number of degrees to turn.</arg-description></description><BlockConnectors><BlockConnector label="degs" connector-kind="socket" connector-type="number"><DefaultArg genus-name="number" label="90"></DefaultArg></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="rt"></LangSpecProperty><LangSpecProperty key="stack-type" value="breed"></LangSpecProperty></LangSpecProperties></BlockGenus><!-- LOGIC --><BlockGenus name="if" kind="command" initlabel="if" color="205 232 213"><description><text>If <arg n="1"/> is true, the agent will run the blocks attached to <arg n="2"/></text><arg-description n="1" name="test">The condition being tested.</arg-description><arg-description n="2" name="then">The set of instructions that is done when the condition is true.</arg-description></description><BlockConnectors><BlockConnector label="test" connector-kind="socket" connector-type="boolean"></BlockConnector><BlockConnector label="then" connector-kind="socket" connector-type="command"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="if"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="ifelse" kind="command" initlabel="ifelse" color="205 232 213"><description><text>Does the first set of commands if the condition specified is true, otherwise it will do the second set of commands.</text><arg-description n="1" name="test">The condition being tested.</arg-description><arg-description n="2" name="then">The set of instructions that is done when the condition is true.</arg-description><arg-description n="3" name="else">The set of instructions that is done when the condition is false.</arg-description></description><BlockConnectors><BlockConnector label="test" connector-kind="socket" connector-type="boolean"></BlockConnector><BlockConnector label="then" connector-kind="socket" connector-type="command"></BlockConnector><BlockConnector label="else" connector-kind="socket" connector-type="command"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="ifelse"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="repeat" kind="command" initlabel="repeat" color="205 232 213"><description><text>Repeats some commands <arg n="1"/> times.</text><arg-description n="1" name="times">The number of times to repeat the command.</arg-description><arg-description n="2" name="do">The commands to do.</arg-description></description><BlockConnectors><BlockConnector label="times" connector-kind="socket" connector-type="number"></BlockConnector><BlockConnector label="do" connector-kind="socket" connector-type="command"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="repeat"></LangSpecProperty></LangSpecProperties></BlockGenus><!-- String Functions or Commands --><BlockGenus name="say" kind="command" initlabel="say" color="200 235 254"><description><text>Causes this agent to "say" the supplied text by popping up a speech bubble in SpaceLand.</text></description><BlockConnectors><BlockConnector connector-kind="socket" connector-type="string"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="say"></LangSpecProperty><LangSpecProperty key="stack-type" value="breed"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="set-status" kind="command" initlabel="set status" color="200 235 254"><description><text>Displays the text attached to this block in the bottom panel of Spaceland.  </text></description><BlockConnectors><BlockConnector connector-kind="socket" connector-type="string"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="set-status"></LangSpecProperty></LangSpecProperties></BlockGenus><!-- MATH --><BlockGenus name="pi" kind="function" initlabel="pi" color="255 216 67"><description><text>Reports the value of PI (approximately 3.14).</text></description><BlockConnectors><BlockConnector connector-kind="plug" connector-type="number" position-type="mirror"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="pi"></LangSpecProperty><LangSpecProperty key="is-monitorable" value="yes"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="sum" kind="function" initlabel="+" color="224 209 204"><description><text>Reports the sum of two numbers (<arg n="1"/> + <arg n="2"/>).</text><arg-description n="1" doc-name="left number"></arg-description><arg-description n="2" doc-name="right number"></arg-description></description><BlockConnectors><BlockConnector label="" connector-kind="plug" connector-type="number" position-type="mirror"></BlockConnector><BlockConnector label="" connector-kind="socket" connector-type="number" position-type="bottom"></BlockConnector><BlockConnector label="" connector-kind="socket" connector-type="number" position-type="bottom"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="sum"></LangSpecProperty><LangSpecProperty key="is-monitorable" value="yes"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="difference" kind="function" initlabel="-" color="224 209 204"><description><text>Reports the difference of two numbers (<arg n="1"/> - <arg n="2"/>).</text><arg-description n="1" doc-name="left number"></arg-description><arg-description n="2" doc-name="right number"></arg-description></description><BlockConnectors><BlockConnector label="" connector-kind="plug" connector-type="number" position-type="mirror"></BlockConnector><BlockConnector label="" connector-kind="socket" connector-type="number" position-type="bottom"></BlockConnector><BlockConnector label="" connector-kind="socket" connector-type="number" position-type="bottom"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="difference"></LangSpecProperty><LangSpecProperty key="is-monitorable" value="yes"></LangSpecProperty></LangSpecProperties></BlockGenus><!-- TRAITS --><BlockGenus name="color" kind="function" initlabel="color" color="209 199 86"><description><text>Reports the color of this agent.</text></description><BlockConnectors><BlockConnector connector-kind="plug" connector-type="number" position-type="mirror"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="agent-color"></LangSpecProperty><LangSpecProperty key="stack-type" value="breed"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="xcor" kind="function" initlabel="xcor" color="209 199 86"><description><text>Reports the x coordinate of this agent.</text></description><BlockConnectors><BlockConnector connector-kind="plug" connector-type="number" position-type="mirror"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="xcor"></LangSpecProperty><LangSpecProperty key="stack-type" value="breed"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="ycor" kind="function" initlabel="ycor" color="209 199 86"><description><text>Reports the y coordinate of this agent.</text></description><BlockConnectors><BlockConnector connector-kind="plug" connector-type="number" position-type="mirror"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="vm-cmd-name" value="ycor"></LangSpecProperty><LangSpecProperty key="stack-type" value="breed"></LangSpecProperty></LangSpecProperties></BlockGenus><!-- SETUP AND RUNTIME BLOCKS --><!-- NOTE: The genus names "forever", "runonce", and "runforsometime" is duplicated in RunBlockManager.java, if you change this name, make sure to change it there too.  --><BlockGenus name="forever" kind="command" initlabel="forever" editable-label="yes" label-unique="yes" is-starter="yes" is-terminator="yes" color="252 164 75"><description><text>When switched on, agents do commands over and over. <note>There are different commands for each breed.</note></text><arg-description n="1" name="agents" doc-name="breed">The breed of the agents performing the commands attached here.</arg-description></description><BlockConnectors><BlockConnector label="" connector-kind="socket" connector-type="command"></BlockConnector></BlockConnectors><Images><Image block-location="southwest" image-editable="yes" width="15" height="15"><FileLocation>support/forever_switch_on.png</FileLocation></Image></Images><LangSpecProperties><LangSpecProperty key="runtime-type" value="forever"></LangSpecProperty><LangSpecProperty key="is-owned-by-breed" value="yes"></LangSpecProperty><LangSpecProperty key="has-runtime-equiv" value="yes"></LangSpecProperty><LangSpecProperty key="stack-type" value="breed-forever"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="runforsometime" kind="command" initlabel="run" editable-label="yes" label-unique="yes" is-starter="yes" is-terminator="yes" color="252 164 75"><description><text>When switched on, agents do commands over and over for <arg n="1"/> seconds. <note>There are different commands for each breed.</note></text><arg-description n="1" name="seconds">The number of seconds for which the commands are run when switched on.</arg-description><arg-description n="2" name="agents" doc-name="breed">The breed of the agents performing the commands attached here.</arg-description></description><BlockConnectors>    <BlockConnector label="seconds" connector-kind="socket" connector-type="number">    </BlockConnector><BlockConnector connector-kind="socket" connector-type="command"></BlockConnector></BlockConnectors><Images><Image block-location="southwest" image-editable="yes" width="15" height="15"><FileLocation>support/forever_switch_on.png</FileLocation></Image></Images><LangSpecProperties><LangSpecProperty key="runtime-type" value="runforsometime"></LangSpecProperty><LangSpecProperty key="is-owned-by-breed" value="yes"></LangSpecProperty><LangSpecProperty key="has-runtime-equiv" value="yes"></LangSpecProperty><LangSpecProperty key="stack-type" value="breed-forever"></LangSpecProperty></LangSpecProperties></BlockGenus><BlockGenus name="setup" kind="command" initlabel="setup" editable-label="yes" label-unique="yes" is-starter="yes" is-terminator="yes" color="252 164 75"><description><text>This block serves as a wrapper around blocks that set up a model.</text></description><BlockConnectors><BlockConnector connector-kind="socket" connector-type="command"></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="is-setup" value="yes"></LangSpecProperty><LangSpecProperty key="has-runtime-equiv" value="yes"></LangSpecProperty><LangSpecProperty key="stack-type" value="setup"></LangSpecProperty></LangSpecProperties></BlockGenus><!-- BREED SPECIFIC BLOCKS --><BlockGenus name="create-agents" kind="command" initlabel="" label-prefix="create " color="252 164 75"><description><text>Creates <arg n="1"/> number of agents of the given breed.</text><arg-description n="1" name="num">The number of agents to create.</arg-description></description><BlockConnectors><BlockConnector label="num" connector-kind="socket" connector-type="number"><DefaultArg genus-name="number" label="10"></DefaultArg></BlockConnector></BlockConnectors><LangSpecProperties><LangSpecProperty key="is-owned-by-breed" value="yes"></LangSpecProperty><LangSpecProperty key="vm-cmd-name" value="crt-breed"></LangSpecProperty><LangSpecProperty key="is-setup-block" value="yes"></LangSpecProperty><LangSpecProperty key="include-breed" value="yes"></LangSpecProperty><LangSpecProperty key="include-breed-shape" value="yes"></LangSpecProperty><LangSpecProperty key="stack-type" value="setup"></LangSpecProperty></LangSpecProperties></BlockGenus></BlockGenuses><BlockDrawerSets><BlockDrawerSet name="factory" type="stack" location="southwest" window-per-drawer="no" drawer-draggable="no" ><BlockDrawer name="Setup and Run" type="factory" is-open="yes" button-color="173 0 0"><BlockGenusMember>setup</BlockGenusMember><BlockGenusMember>forever</BlockGenusMember><BlockGenusMember>runforsometime</BlockGenusMember></BlockDrawer><BlockDrawer name="Movement" type="factory" button-color="247 0 0"><BlockGenusMember>fd</BlockGenusMember><BlockGenusMember>bk</BlockGenusMember><BlockGenusMember>lt</BlockGenusMember><BlockGenusMember>rt</BlockGenusMember></BlockDrawer><BlockDrawer name="Logic" type="factory" button-color="255 173 0"><BlockGenusMember>if</BlockGenusMember><BlockGenusMember>ifelse</BlockGenusMember><BlockGenusMember>true</BlockGenusMember><BlockGenusMember>false</BlockGenusMember></BlockDrawer><BlockDrawer name="Traits" type="factory" button-color="0 255 254"><BlockGenusMember>color</BlockGenusMember><BlockGenusMember>setx</BlockGenusMember><BlockGenusMember>xcor</BlockGenusMember><BlockGenusMember>sety</BlockGenusMember><BlockGenusMember>ycor</BlockGenusMember></BlockDrawer><BlockDrawer name="Text" type="factory" button-color="200 235 254"><BlockGenusMember>string</BlockGenusMember><BlockGenusMember>say</BlockGenusMember><BlockGenusMember>set-status</BlockGenusMember></BlockDrawer><BlockDrawer name="Math" type="factory" button-color="102 129 255"><BlockGenusMember>number</BlockGenusMember><BlockGenusMember>pi</BlockGenusMember><BlockGenusMember>sum</BlockGenusMember><BlockGenusMember>difference</BlockGenusMember></BlockDrawer></BlockDrawerSet></BlockDrawerSets></BlockLangDef>';
var project_str = '<?xml version="1.0" encoding="UTF-16"?><SLCODEBLOCKS><PageBlocks><Block id="823" genus-name="setup" ><Location><X>53</X><Y>229</Y></Location><Sockets num-sockets="1" ><BlockConnector connector-kind="socket" connector-type="command" init-type="command" label="" position-type="single" con-block-id="827" ></BlockConnector></Sockets></Block><Block id="827" genus-name="create-agents" ><Label>Turtles</Label><Location><X>104</X><Y>237</Y></Location><BeforeBlockId>823</BeforeBlockId><Sockets num-sockets="1" ><BlockConnector connector-kind="socket" connector-type="number" init-type="number" label="num" position-type="single" con-block-id="829" ></BlockConnector></Sockets><LangSpecProperties><LangSpecProperty key="breed-name" value="Turtles" ></LangSpecProperty></LangSpecProperties></Block><Block id="829" genus-name="number" ><Label>100</Label><Location><X>236</X><Y>240</Y></Location><Plug><BlockConnector connector-kind="plug" connector-type="number" init-type="number" label="" position-type="mirror" con-block-id="827" ></BlockConnector></Plug></Block><Block id="811" genus-name="forever" ><Location><X>52</X><Y>55</Y></Location><Sockets num-sockets="2" ><BlockConnector connector-kind="socket" connector-type="command" init-type="command" label="Turtles" position-type="single" con-block-id="815" ></BlockConnector><BlockConnector connector-kind="socket" connector-type="command" init-type="command" label="Fish" position-type="single" con-block-id="831" ></BlockConnector></Sockets><LangSpecProperties><LangSpecProperty key="breed-name" value="Turtles" ></LangSpecProperty></LangSpecProperties></Block><Block id="831" genus-name="bk" ><Location><X>152</X><Y>134</Y></Location><BeforeBlockId>811</BeforeBlockId><AfterBlockId>835</AfterBlockId><Sockets num-sockets="1" ><BlockConnector connector-kind="socket" connector-type="number" init-type="number" label="steps" position-type="single" con-block-id="833" ></BlockConnector></Sockets></Block><Block id="835" genus-name="rt" ><Location><X>152</X><Y>164</Y></Location><BeforeBlockId>831</BeforeBlockId><Sockets num-sockets="1" ><BlockConnector connector-kind="socket" connector-type="number" init-type="number" label="degs" position-type="single" con-block-id="837" ></BlockConnector></Sockets></Block><Block id="837" genus-name="number" ><Label>7</Label><Location><X>238</X><Y>167</Y></Location><Plug><BlockConnector connector-kind="plug" connector-type="number" init-type="number" label="" position-type="mirror" con-block-id="835" ></BlockConnector></Plug></Block><Block id="833" genus-name="number" ><Label>2</Label><Location><X>242</X><Y>137</Y></Location><Plug><BlockConnector connector-kind="plug" connector-type="number" init-type="number" label="" position-type="mirror" con-block-id="831" ></BlockConnector></Plug></Block><Block id="815" genus-name="fd" ><Location><X>153</X><Y>63</Y></Location><BeforeBlockId>811</BeforeBlockId><AfterBlockId>819</AfterBlockId><Sockets num-sockets="1" ><BlockConnector connector-kind="socket" connector-type="number" init-type="number" label="steps" position-type="single" con-block-id="817" ></BlockConnector></Sockets></Block><Block id="819" genus-name="lt" ><Location><X>153</X><Y>93</Y></Location><BeforeBlockId>815</BeforeBlockId><Sockets num-sockets="1" ><BlockConnector connector-kind="socket" connector-type="number" init-type="number" label="degs" position-type="single" con-block-id="821" ></BlockConnector></Sockets></Block><Block id="821" genus-name="number" ><Label>90</Label><Location><X>231</X><Y>96</Y></Location><Plug><BlockConnector connector-kind="plug" connector-type="number" init-type="number" label="" position-type="mirror" con-block-id="819" ></BlockConnector></Plug></Block><Block id="817" genus-name="number" ><Location><X>261</X><Y>66</Y></Location><Plug><BlockConnector connector-kind="plug" connector-type="number" init-type="number" label="" position-type="mirror" con-block-id="815" ></BlockConnector></Plug></Block></PageBlocks></SLCODEBLOCKS>';

var drawers = [];

sb.ScriptBlocksXML.genusMap = {}; //maps genus name to BlockSpec for that genus

sb.ScriptBlocksXML.loadXML = function(){
	sb.ScriptBlocksXML.initWorkspaceFromXML(lang_def_str);
	sb.ScriptBlocksXML.loadBlockStructureFromXML(project_str);
	sb.ScriptBlocksDemo.addDrawerBlocks();
}

sb.ScriptBlocksXML.saveXML = function(){
	project_str = sb.ScriptBlocksXML.getBlockStructureAsXML();
	sb.Logger.log(project_str);
}

sb.ScriptBlocksXML.parseColor = function(colorStr){
  var rgb = colorStr.split(' ');
	return "#"+(parseInt(rgb[0], 10)*0x010000 + parseInt(rgb[1], 10)*0x000100 + parseInt(rgb[2], 10)).toString(16);
}

sb.ScriptBlocksXML.parseGenusElements = function(genusElements){
	var spec = {};
	var specLabel = '';
	var connectorElts;
	var specArgs;
	var argLabel;
	var socketType;
	var lspElts;
	var specLSPs;
	var specReturnType;
	
	// loop through each BlockGenus specification Element
	for (var i=0; i<genusElements.length; i++) {
		specLabel = genusElements[i].getAttribute("initlabel")+'\n';
		specReturnType = genusElements[i].getAttribute("kind");
		
		// parse the arguments (sockets) for the genus
		connectorElts = genusElements[i].getElementsByTagName("BlockConnector");
		specArgs = [];
		for (var j=0; j<connectorElts.length; j++) {
			if (connectorElts[j].getAttribute("connector-kind") != "socket") {
				specReturnType = connectorElts[j].getAttribute("connector-type");
				continue; // only use sockets, not plugs, to create arguments spec
			}
			if (connectorElts[j].hasAttribute("label") && connectorElts[j].getAttribute("label") != '') {
				argLabel = connectorElts[j].getAttribute("label");
				specLabel += argLabel + ' ';
			} else {
				argLabel = 'arg'+j;
			}
			specLabel += '@'+argLabel + ' ';
			socketType = "nested";
			if(genusElements[i].hasAttribute("is-label-value") && genusElements[i].getAttribute("is-label-value") == "yes") {
				socketType = "internal";
			}
			specArgs.push( new sb.Argument(argLabel, connectorElts[j].getAttribute("connector-type"), socketType) );
		}
		
		// parse LangSpecProperties
		lspElts = genusElements[i].getElementsByTagName("LangSpecProperty");
		specLSPs = {};
		for (var k=0; k<lspElts.length; k++) {
			specLSPs[lspElts[k].getAttribute("key")] = lspElts[k].getAttribute("value");
		}
		
		specLabel = goog.string.trim(specLabel);
		
		// create the BlockSpec for this type of block
		spec = new sb.BlockSpec();
		spec = sb.BlockSpec.extend(spec, {
			name: genusElements[i].getAttribute("name"),
			label: specLabel,
			initLabel: genusElements[i].getAttribute("initlabel"),
			returnType: specReturnType,
			arguments: specArgs,
			langSpecProperties: specLSPs,
			color: sb.ScriptBlocksXML.parseColor(genusElements[i].getAttribute("color"))
		});
		
		// determine before/after connectors, and add to spec
		if (genusElements[i].getAttribute("kind") == "command"){
			spec = sb.BlockSpec.extend(spec, {
				connections: [sb.SocketType.AFTER, sb.SocketType.BEFORE]
			});
		} else {
			spec = sb.BlockSpec.extend(spec, {
				connections: [sb.SocketType.BEFORE]
			});
		}

		// save spec to the genus spec map
		sb.ScriptBlocksXML.genusMap[genusElements[i].getAttribute("name")] = spec;
		
		// TODO: FOR TESTING ONLY
		//var block = new sb.Block( spec );
		//sb.ScriptBlocksDemo.testPage.addBlock( block );
	}	
}


sb.ScriptBlocksXML.initDrawersFromElements = function(drawerElements){
	var drawer;
	var blockElts;
	var block;
	
	for (var i=0; i<drawerElements.length; i++) {
		var drawerName = drawerElements[i].getAttribute("name");
		drawer = new sb.Drawer(drawerName);
		sb.ScriptBlocksDemo.workspace.addDrawer(drawer);
		blockElts = drawerElements[i].getElementsByTagName("BlockGenusMember");
		for (var j=0; j<blockElts.length; j++) {
			if (sb.ScriptBlocksXML.genusMap[blockElts[j].textContent] == undefined) {
				sb.Logger.log("No genus definition for "+blockElts[j].textContent);
				continue;
			}
			block = new sb.Block(sb.ScriptBlocksXML.genusMap[blockElts[j].textContent]);
			drawer.addBlock(block);
			drawers.push(drawer);
		}
	}
}

/**
 * Adds a generated block to a given drawer. NOTE: These blocks are not fully implemented,
 * only enough to allow for the ActionScript to read them in.
 * 
 * @param {string}	label 		The text on the block
 * @param {boolean}	isCommand 	Whether or not the block is a command block
 * @param {number}		params 		Number of sockets the block has
 * @param {string}	drawer 		The name of the drawer the block belongs in
 */
sb.ScriptBlocksXML.generateNewBlock = function(label, isCommand, params, drawer){
	var spec = new sb.BlockSpec();
	var specArgs = [];
	var specLabel = label + ' ';
	
	for (var j = 0; j < params; j++) {
		specArgs.push( new sb.Argument("arg" + j, "number", "nested") );
		specLabel += '@arg' + j + '\n';
	}
	
	// remove trailing space
	if(specLabel[specLabel.length-1] === ' ' || specLabel[specLabel.length-1] === '\n') {
		specLabel = specLabel.substring(0, specLabel.length-1);
	}
	
	// create the BlockSpec for this type of block
	spec = sb.BlockSpec.extend(spec, {
		label: specLabel,
		returnType: "number",
		arguments: specArgs
	});
	
	// determine before/after connectors, and add to spec
	if (isCommand){
		spec = sb.BlockSpec.extend(spec, {
			connections: [sb.SocketType.AFTER, sb.SocketType.BEFORE]
		});
	} else {
		spec = sb.BlockSpec.extend(spec, {
			connections: [sb.SocketType.BEFORE]
		});
	}

	// save spec to the genus spec map
	sb.ScriptBlocksXML.genusMap[spec.name] = spec;
	var addDrawer;
	for (var i=0; i < drawers.length; i++) {
		if (drawers[i].name_ == drawer) {
			addDrawer = drawers[i];
		}
	}
	addDrawer.addBlock(new sb.Block(spec));
}

/**
 * loads language definition XML which includes block specs and drawer specs (for now assuming only one page)
 * @param {string} xml
 */
sb.ScriptBlocksXML.initWorkspaceFromXML = function(xml){
	var parser = new DOMParser();
	var xmlDoc = parser.parseFromString(xml,"text/xml");
	sb.ScriptBlocksXML.parseGenusElements(xmlDoc.getElementsByTagName("BlockGenus"));
	sb.ScriptBlocksXML.initDrawersFromElements(xmlDoc.getElementsByTagName("BlockDrawer"));
}


/**
 * loads a "project" - a set of blocks which are potentially interconnected, from saved XML
 * @param {string} xml
 */
sb.ScriptBlocksXML.loadBlockStructureFromXML = function(xml){
	var parser = new DOMParser();
	var xmlDoc = parser.parseFromString(xml,"text/xml");
	var blockElts = xmlDoc.getElementsByTagName("Block");
	var spec;
	var blocksByID = {};
	
	for (var i=0; i<blockElts.length; i++) {
		spec = new sb.BlockSpec();
		spec = sb.BlockSpec.extend(spec, sb.ScriptBlocksXML.genusMap[blockElts[i].getAttribute("genus-name")]);
		
		var connectorElts = blockElts[i].getElementsByTagName("BlockConnector");

		var specLabel = spec.initLabel+'\n';
		var specInitLabel = spec.initLabel;
		if (blockElts[i].getElementsByTagName("Label").length > 0) {
			specInitLabel = blockElts[i].getElementsByTagName("Label")[0].textContent;
			specLabel = specInitLabel+' ';
		}
		
		var specReturnType = spec.returnType;
		var blockID = blockElts[i].getAttribute("id");
		
		// parse the arguments (sockets) for this instance of the block
		var specArgs = [];
		var socketType = "nested";
		for (var j=0; j<connectorElts.length; j++) {
			if (connectorElts[j].getAttribute("connector-kind") != "socket") {
				specReturnType = connectorElts[j].getAttribute("connector-type");
				continue; // only use sockets, not plugs, to create arguments spec
			}
			var argLabel;
			if (connectorElts[j].hasAttribute("label") && connectorElts[j].getAttribute("label") != '') {
				argLabel = connectorElts[j].getAttribute("label");
				specLabel += argLabel + ' ';
			} else {
				argLabel = 'arg'+j;
			}
			if(blockElts[i].hasAttribute("is-label-value") && blockElts[i].getAttribute("is-label-value") == "yes") {
                socketType = "internal";
            }
			specLabel += '@'+argLabel+' ';
			specArgs.push( new sb.Argument(argLabel, connectorElts[j].getAttribute("connector-type"), socketType) );
		}
		if (specArgs.length == 0) { //if no arg override happened, use default args from the genus specs
			specArgs = spec.arguments;
		}
		
		// remove trailing space
		if(specLabel[specLabel.length-1] === ' ' || specLabel[specLabel.length-1] === '\n') {
			specLabel = specLabel.substring(0, specLabel.length-1);
		}
		
		// parse LangSpecProperties
		var lspElts = blockElts[i].getElementsByTagName("LangSpecProperty");
		var specLSPs = spec.langSpecProperties;
		for (var k=0; k<lspElts.length; k++) {
			specLSPs[lspElts[k].getAttribute("key")] = lspElts[k].getAttribute("value");
		}
		
		// update the BlockSpec for this instance of the block
		spec = sb.BlockSpec.extend(spec, {
			label: specLabel,
			initLabel: specInitLabel,
			returnType: specReturnType,
			arguments: specArgs,
			langSpecProperties: specLSPs,
			color: blockElts[i].hasAttribute("color") ? sb.ScriptBlocksXML.parseColor(blockElts[i].getAttribute("color")) : spec.color
		});
		
		// ID-block mapping (for connection next)
		var block = new sb.Block( spec );		
		blocksByID[blockID] = block;
	}	
	
	// now loop through and make all of the required connections
	for (i = 0; i < blockElts.length; i++) {
		
		blockID = blockElts[i].getAttribute("id");
		connectorElts = blockElts[i].getElementsByTagName("BlockConnector");
				
		// look through the sockets and make connections if found
		for (var j = 0; j < connectorElts.length; j++) {
			if (connectorElts[j].getAttribute("connector-kind") != "socket") {
				continue; // only connect sockets blocks
			}
			var conBlockID = connectorElts[j].getAttribute("con-block-id");
			var argName;
			if (conBlockID != '' && conBlockID != undefined) {
				if (connectorElts[j].hasAttribute("label") && connectorElts[j].getAttribute("label") != '') {
					argName = connectorElts[j].getAttribute("label");
				} else {
					argName = 'arg'+j;
				}				
				// connect the child block (ID = conBlockID) to this block in this socket
				blocksByID[blockID].connectChildBlock(blocksByID[conBlockID], argName);
			}
		}
		
		// now connect the block below it, if one exists
		if (blockElts[i].getElementsByTagName("AfterBlockId").length > 0) {
			blocksByID[blockID].connectAfterBlock(blocksByID[blockElts[i].getElementsByTagName("AfterBlockId")[0].textContent]);
		}
	}
	
	// add the top-level blocks to the page
	for (blockID in blocksByID) {
		var topBlock = blocksByID[blockID];
		
		if(topBlock.getParent() === null && topBlock.getBefore() === null) {
			sb.ScriptBlocksDemo.testPage.addBlock( topBlock );
		}
	}
}

/**
 * returns an XML string representation of the block structure on the page.
 * @return {string} 
 */
sb.ScriptBlocksXML.getBlockStructureAsXML = function(){
	var pages = sb.ScriptBlocks.getWorkspace().getPages();
	var xmlstr = '<?xml version="1.0" encoding="UTF-16"?><SLCODEBLOCKS><PageBlocks>';
	var idcounter = 1; //non-zero ID's just to be safe
	var color;
	
	// first, assign every Block an ID to make linking much easier
	for (var i = 0; i < pages.length; i++) {
		var pageBlocks = pages[i].getAllBlocks();
		for (var j = 0; j < pageBlocks.length; j++) {
			pageBlocks[j].id = idcounter;
			idcounter++;
		}
	}

	// now go through again and generate XML for each block
	for (i=0; i<pages.length; i++) {
		pageBlocks = pages[i].getAllBlocks();
		for (j=0; j<pageBlocks.length; j++) {
      color = pageBlocks[j].getColor();
			xmlstr += '<Block ';
			xmlstr += 'id="' + pageBlocks[j].id +'" ';
			xmlstr += 'genus-name="' + pageBlocks[j].getSpec().name +'" ';
			xmlstr += 'color="' + (color & 0xFF0000) / 0x10000 +' '+ (color & 0xFF00) / 0x100 +' '+ (color & 0xFF) + '">';
			xmlstr += '<Label>' + pageBlocks[j].getSpec().initLabel + '</Label>';
			if (pageBlocks[j].getSpec().returnType != "command") {
				xmlstr += '<Plug><BlockConnector connector-kind="plug" connector-type="' + pageBlocks[j].getSpec().returnType + '" ';
				xmlstr += 'init-type="' + pageBlocks[j].getSpec().returnType + '" label="" position-type="mirror" ';
				if (pageBlocks[j].getParent() instanceof sb.Block) {
					xmlstr += 'con-block-id="' + pageBlocks[j].getParent().id + '" ';
				}
				xmlstr += '></BlockConnector></Plug>';
			}
			//if (pageBlocks[j].getArguments().length > 0) { // TODO:NO WAY TO TEST FOR # of ARGS??
			xmlstr += '<Sockets num-sockets="' + 1 /*TODO:WRONG!*/ + '" >'
			for (var k=0; k < pageBlocks[j].getArguments().length; k += 1) {
				var arg = pageBlocks[j].getArguments()[k];
				xmlstr += '<BlockConnector connector-kind="socket" ';
				xmlstr += 'connector-type="' + arg.getDataType() + '" ';
				xmlstr += 'init-type="' + arg.getDataType() + '" ';
				xmlstr += 'label="' + arg.getName() + '" ';
				if (arg.getValue() instanceof sb.Block) {
					xmlstr += 'con-block-id="' + arg.getValue().id + '" ';
				}
				xmlstr += '></BlockConnector>';
			}
			xmlstr += '</Sockets>';
			//}
			if (pageBlocks[j].getAfterBlock() instanceof sb.Block) {
				xmlstr += '<AfterBlockId>' + pageBlocks[j].getAfterBlock().id + '</AfterBlockId>';
			}
			xmlstr += '</Block>';
		}
	}
	xmlstr += '</PageBlocks></SLCODEBLOCKS>';
	return xmlstr;
}



