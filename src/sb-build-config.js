{
	"id": "ScriptBlocks_closure",

	"inputs": "ScriptBlocks.js",

	"paths": [
		"model/ObjectManager.js",
		"model/SBObject.js",
	    "model/SBObjectEvent.js",
		"model/Argument.js",
	    "model/ArgumentEvent.js",
		"model/ListArgument.js",
		"model/Block.js",
		"model/BlockEvent.js",
		"model/BlockSpec.js",
		"model/DataType.js",
		"model/SocketType.js",
		"model/IBlockParent.js",
		"model/AbstractBlockParent.js",
		"model/BlockParentEvent.js",
		"model/Page.js",
		"model/Drawer.js",
		"model/Workspace.js",
		"model/WorkspaceEvent.js",
		"./model",
		
		"view/ViewManager.js",
		"view/BlockView.js",
		"view/SocketShapes.js",
		"view/PageView.js",
		"view/DrawerView.js",
		"view/BlockDragDropManager.js",
		"./view",
		
		"utils/InputManager.js",
		"utils/Utils.js",
		"utils/Logger.js",
		
		"ScriptBlocks.js"
	],
	
	"jsdoc-html-output-path": "../bin/docs",
	"mode": "SIMPLE",
	"level": "VERBOSE",
	"checks": {
	    // acceptable values are "ERROR", "WARNING", and "OFF" 
	    "deprecated": "ERROR",
	    "checkTypes": "ERROR",
	    "nonStandardJsDocs": "WARNING"
	  }

}