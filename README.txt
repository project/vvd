== Introduction ==

The vvd.module (Visual VoIP Drupal) provides a UI for creating VoIP Drupal call scripts.

In particular, this module provides:

 - a new Content Type VVD Workspace
 

== Installation ==

1. Extract vvd.module to your sites/all/modules directory

2. Enable the "Visual VoIP Drupal" and "Visual VoIP Drupal core" module in admin/build/modules

3. Go to node/add/vvd and start creating VoIP Scripts!

== Usage ==

1. Go to node/add/vvd
2. Move with the mouse to left side menu (near the "Script handling"), this will collapse the blocks menu. Blocks menu is divided in several categories:
  Script handling, Call control, Input and output, Expressions, Flow control, Voice and Sound. Each category contains appropriate script blocks. There are
  two types of script blocks: blue and brown. Blue blocks are commands and can be connected with other blocks, brown blocks are variables which can be either 
  string (represented as rectangle) or boolean (represented as cylinder) datatype. Brown blocks can be insterted as arguments inside blue blocks with matching 
  datatypes.
3. To create a VoIP Script you must add "Script" block in Workspace. Then you add appropriate command blocks inside that "Script" block.
!NOTE: Orphaned blocks are not considered part of any script.
4. In one Workspace you can create multiple VoIP Scripts by adding multiple "Script" blocks.  
5. When you save the node it will create VoIP Script from your Script blocks, the argument "name" will be used as your unique Script name so be sure to 
   fill it uniquely for each "Script" block otherwise you might get validation error. 
  

---
The Visual VoIP Drupal module has been originally developed by Tamer Zoubi under the sponsorship of the MIT Center for Future Civic Media (http://civic.mit.edu).